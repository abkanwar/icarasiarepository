////
//  Utility.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/8/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class Utility : NSObject , NSURLSessionDelegate {
    
    class var sharedInstance: Utility {
        struct Static {
            static let instance: Utility = Utility()
        }
        return Static.instance
    }
    
    
    func loginStatus() {
        
        let userDefault = NSUserDefaults()
        if userDefault.integerForKey("userLoginStatus") == 0{
            userDefault.setInteger(1, forKey: "userLoginStatus")
        }
        else{
            userDefault.setInteger(0, forKey: "userLoginStatus")
        }
    }
    
    func isUserLoggedIn() ->  NSInteger{
    
        let userDefault = NSUserDefaults()
        return userDefault.integerForKey("userLoginStatus")
    }
    
    func saveToken( token : String){
        
        let userDefault = NSUserDefaults()
        userDefault.setValue(token, forKey:("token"))
    }
    
    func token() -> String{
        
        let userDefault = NSUserDefaults()
        return userDefault.valueForKey("token") as! String
    }
    
    func saveUserDetails( dictionaryDetails : NSMutableDictionary){
        
        let userDefault = NSUserDefaults()
        return userDefault.setObject(dictionaryDetails, forKey: "userDetails")
    }
    
    func fetchUserDetails() -> NSMutableDictionary{
        
        let userDefault = NSUserDefaults()
        return userDefault.objectForKey("userDetails") as! NSMutableDictionary
    }
    
    func saveUserDetailsOfiCarAsia( dictionaryDetails : NSMutableDictionary){
        
        let userDefault = NSUserDefaults()
        userDefault.setObject(dictionaryDetails, forKey: "iCarAsiaUserDetails")
    }
    
    func fetchUserDetailsiCarAsia() -> NSMutableDictionary{
        
        let userDefault = NSUserDefaults()
        return userDefault.objectForKey("iCarAsiaUserDetails") as! NSMutableDictionary
    }
    
    func isDBCreated() -> Bool{
        
        let userDefault = NSUserDefaults()
        return userDefault.boolForKey("isDBCreated")
    }
    
    func statusDB(){
        
        let userDefault = NSUserDefaults()
        userDefault.setBool(true, forKey: "isDBCreated")
    }
    
    func navigationBarCrossButton( title : String ) -> UIButton {
        
        let buttonBack = UIButton()
        buttonBack.setTitle(title, forState: UIControlState.Normal)
        buttonBack.titleLabel?.font = UIFont.boldSystemFontOfSize(15)
        buttonBack.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        buttonBack.sizeToFit()
        return  buttonBack
    }
    
    func hexStringToUIColor (hex : String) -> UIColor {
        
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    func timeStringByDate ( date : NSDate ) -> String {
        
        
        var stringTime = String()
        
        let unitFlags: NSCalendarUnit   = [.Year, .Month, .Day]
        var comps: NSDateComponents     = NSCalendar.currentCalendar().components(unitFlags, fromDate:date)
        let dateMessage : NSDate        = NSCalendar.currentCalendar().dateFromComponents(comps)!
        
        comps                           = NSCalendar.currentCalendar().components(unitFlags, fromDate:NSDate())
        let dateCurrent : NSDate        = NSCalendar.currentCalendar().dateFromComponents(comps)!
        
        
        if dateMessage.compare(dateCurrent)      == NSComparisonResult.OrderedDescending {
           
            /* Message date later than curent date  */
            
        }
        else if dateMessage.compare(dateCurrent) == NSComparisonResult.OrderedAscending {
            
            /* Message date earlier than curent date */
            
            let formatter: NSDateFormatter          = NSDateFormatter()
            formatter.dateFormat                    = "dd, MMM yy"
            stringTime                              = formatter.stringFromDate(date)
        }
        else {
           
            /* Date are same */
            
            let formatter: NSDateFormatter          = NSDateFormatter()
            formatter.dateFormat                    = "HH:mm"
            stringTime                              = formatter.stringFromDate(date)
        }
        
        return stringTime
    }
    
    func saveArchivedChatIDs ( arrayChatIDs : NSMutableArray){
        
        let userDefault = NSUserDefaults()
        return userDefault.setObject(arrayChatIDs, forKey: "archivedChats")
    }
    
    func fetchArchivedChatIDs () -> NSMutableArray{
        
        let userDefault = NSUserDefaults()
        if (userDefault.objectForKey("archivedChats") != nil){
            return (userDefault.objectForKey("archivedChats")?.mutableCopy())! as! NSMutableArray
        }else{
            return NSMutableArray ()
        }
    }
    
    func showAlertView ( title : String , message : String , controller : UIViewController){
        
        let alert = UIAlertController(title: title, message:message , preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
        controller.presentViewController(alert, animated: true){}
    }
    
}

/*class Utility: NSObject {
 
    
    func loginStatus() {
        let userDefault = NSUserDefaults()
        userDefault.setInteger(1, forKey: "userLoginStatus")
    }
    
 }*/


