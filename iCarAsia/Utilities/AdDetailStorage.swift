//
//  AdDetailStorage.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/24/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class AdDetailStorage: NSObject {
    
    var plateNumber     = String()
    var refrenceNumber  = String()
    
    var year            = NSInteger()
    var brand           = String()
    var model           = String()
    
    var location        = String()
    var area            = String()
    var city            = String()
    
    var milage          = CGFloat()
    var price           = CGFloat()
    var title           = String()
    
    var bodyColor       = String()
    
    var descriptionAd   = String()
    
    var assets          = [DKAsset]()
    
    var vehicleInfo     : VehicleInfo!

    
    class var sharedInstance: AdDetailStorage {
        struct Static {
            static let instance: AdDetailStorage = AdDetailStorage()
        }
        return Static.instance
    }
}
