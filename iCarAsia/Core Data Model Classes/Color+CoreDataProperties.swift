//
//  Color+CoreDataProperties.swift
//  iCarAsia
//
//  Created by Raman Kant on 9/6/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Color {

    @NSManaged var color_name: String?
    @NSManaged var css_class: String?
    @NSManaged var hex_value: String?

}
