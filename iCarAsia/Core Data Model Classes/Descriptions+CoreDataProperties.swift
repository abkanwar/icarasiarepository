//
//  Descriptions+CoreDataProperties.swift
//  iCarAsia
//
//  Created by Raman Kant on 9/6/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Descriptions {

    @NSManaged var dscription: String?
    @NSManaged var id: NSNumber?
    @NSManaged var name: String?

}
