//
//  VehicleInfo+CoreDataProperties.swift
//  iCarAsia
//
//  Created by Raman Kant on 9/6/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension VehicleInfo {

    @NSManaged var engine_cc: NSNumber?
    @NSManaged var fuel_type: String?
    @NSManaged var icar_vcode: String?
    @NSManaged var make: String?
    @NSManaged var model: String?
    @NSManaged var series: String?
    @NSManaged var status: String?
    @NSManaged var vehicle_name: String?
    @NSManaged var year: NSNumber?
    @NSManaged var group: String?
    @NSManaged var month: NSNumber?
    @NSManaged var variant: String?
    @NSManaged var body: String?
    @NSManaged var clas: String?
    @NSManaged var lifestyle: String?
    @NSManaged var lifecycle: String?
    @NSManaged var doors: NSNumber?
    @NSManaged var transmission: String?
    @NSManaged var type: String?
    @NSManaged var provider: String?
    @NSManaged var nickname: String?
    @NSManaged var driven_wheel: String?
    @NSManaged var model_alias: String?

}
