//
//  AppDelegate.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/6/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit
import CoreData

import Fabric
import Crashlytics

// QuickBlox Set Up Keys //
let kQBApplicationID:UInt   = 43695
let kQBAuthKey              = "qQWHQgyT65baaYw"
let kQBAuthSecret           = "kVkdTMAqMXrPa9t"
let kQBAccountKey           = "uciKEipDoyupaqsJS6XR"


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?
    var tabBar = UITabBarController()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        print("DB Path : \(applicationDocumentsDirectory)")
        
        /* Enable IQKeyboard Manage for Keyboard */
        IQKeyboardManager.sharedManager().enable = true
        
        // Set TextField/TextView Cursor Color //
        UITextField.appearance().tintColor = UIColor.redColor()
        UITextView.appearance().tintColor  = UIColor.redColor()
        
        // Change Navigation Bar Appearance //
        self.navigationBarAppearance()
        //Fabric.with([Crashlytics.self])
        
        application.applicationIconBadgeNumber = 0
        
        /* Set QuickBlox credentials (You must create application in admin.quickblox.com). */
        QBSettings.setApplicationID(kQBApplicationID)
        QBSettings.setAuthKey(kQBAuthKey)
        QBSettings.setAuthSecret(kQBAuthSecret)
        QBSettings.setAccountKey(kQBAccountKey)
        
        // Enabling carbons for chat //
        QBSettings.setCarbonsEnabled(true)
        
        // Enables Quickblox REST API calls debug console output.//
        QBSettings.setLogLevel(QBLogLevel.Nothing)
        
        // Enables detailed XMPP logging in console output.//
        QBSettings.enableXMPPLogging()
        QBSettings.setChatDNSLookupCacheEnabled(true);
        
        // app was launched from push notification, handling it//
        let remoteNotification: NSDictionary! = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary
        if (remoteNotification != nil) {
            ServicesManager.instance().notificationService.pushDialogID = remoteNotification["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String
        }
        
        //let storyboard                  = UIStoryboard(name: "Main", bundle: nil)
        //let loadingVC                   = storyboard.instantiateViewControllerWithIdentifier("LoadingVC")
        //self.window?.rootViewController = loadingVC
        
        if Utility.sharedInstance.isUserLoggedIn() == 1{
            
            //self.changeRootToMainTabBar()
            //return true
            
            let storyboard                  = UIStoryboard(name: "Main", bundle: nil)
            let loadingVC                   = storyboard.instantiateViewControllerWithIdentifier("LoadingVC")
            self.window?.rootViewController = loadingVC
            
            // Logging to Quickblox REST API and chat. //
            let loginUser           = QBUUser()
            let dictUserDetails     = Utility.sharedInstance.fetchUserDetails()
            loginUser.login         = dictUserDetails.valueForKey("userName") as? String
            loginUser.password      = (dictUserDetails.valueForKey("password") as? String)! + String("ab")
            
            ServicesManager.instance().logInWithUser(loginUser, completion:{ (success:Bool , errorMessage: String?) -> Void in
                
                guard success else {
                    return
                }
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    self.changeRootToMainTabBar()
                }
            })
        }
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool{
        
        if stringInstance.sharedString .isEqualToString("google"){
            return GIDSignIn.sharedInstance().handleURL(url,sourceApplication: sourceApplication,annotation: annotation)
        }
        return FBSDKApplicationDelegate.sharedInstance().application( application,openURL: url,sourceApplication: sourceApplication,annotation: annotation)
    }
    
    func clearCoreDataStore() {
        
        let entities         = self.managedObjectModel.entities
        for entity in entities {
            
            let fetchRequest = NSFetchRequest(entityName: entity.name!)
            if #available(iOS 9.0, *) {
                let delete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                do {
                    try self.persistentStoreCoordinator.executeRequest(delete, withContext: managedObjectContext)
                } catch let error as NSError {
                    print("Error occured while deleting: \(error)")
                }
            } else {
                let fetchRequest                    = NSFetchRequest()
                fetchRequest.entity                   = NSEntityDescription.entityForName(entity.name!, inManagedObjectContext: managedObjectContext)
                fetchRequest.includesPropertyValues = false
                
                do {
                    let entity: NSArray = try managedObjectContext.executeFetchRequest(fetchRequest)
                    for car in entity {
                        managedObjectContext.delete(car)
                    }
                    try managedObjectContext.save()
                } catch let error as NSError {
                    print("Error occured while fetching or saving: \(error)")
                }
            }
        }
    }
    
    func navigationBarAppearance()  {
        
        UINavigationBar.appearance().barTintColor           = UIColor(red: 202.0/255.0, green: 24.0/255.0, blue: 29.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor              = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes    = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        UIToolbar.appearance().tintColor                    = UIColor.redColor();
        
    }
    
    func tabBarAppearance (tabBar : UITabBarController){
        
        UITabBar.appearance().tintColor     = UIColor.whiteColor()
        UITabBar.appearance().barTintColor  = UIColor(red: 40.0/255.0, green: 50.0/255.0, blue: 56.0/255.0, alpha: 1.0)
        
        // Add this code to change StateNormal text Color //
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
        // then if StateSelected should be different, you should add this code //
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Selected)
        
        let appearance = UITabBarItem.appearance()
        appearance.setTitleTextAttributes([NSFontAttributeName:UIFont(name: "Helvetica", size: 12)!], forState: .Normal)
    }
    
    func changeRootToMainTabBar() {
        
        let storyboard                  = UIStoryboard(name: "Main", bundle: nil)
        tabBar                          = (storyboard.instantiateViewControllerWithIdentifier("TabBarController") as? UITabBarController)!
        tabBar.selectedIndex            = 1
        self.tabBarAppearance(tabBar)
        self.window?.rootViewController = tabBar
    }
    
    func changeRootToLogin() {
        
        let storyboard                  = UIStoryboard(name: "Main", bundle: nil)
        let loginVC                     = storyboard.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC
        self.window?.rootViewController = loginVC
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "Raman-Kant.iCarAsia" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("iCarAsia", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    /*lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
     // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
     // Create the coordinator and store
     let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
     let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
     print("DB Path = \(url)")
     var failureReason = "There was an error creating or loading the application's saved data."
     do {
     try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
     } catch {
     // Report any error we got.
     var dict = [String: AnyObject]()
     dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
     dict[NSLocalizedFailureReasonErrorKey] = failureReason
     
     dict[NSUnderlyingErrorKey] = error as NSError
     let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
     // Replace this with code to handle the error appropriately.
     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
     NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
     abort()
     }
     
     return coordinator
     }()*/
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url         = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        
        // Load  Existing Data Base //
        if !NSFileManager.defaultManager().fileExistsAtPath(url.path!)
        {
            let sourceSqliteURLs    = [NSBundle.mainBundle().URLForResource("SingleViewCoreData", withExtension: "sqlite")!, NSBundle.mainBundle().URLForResource("SingleViewCoreData", withExtension: "sqlite-wal")!, NSBundle.mainBundle().URLForResource("SingleViewCoreData", withExtension: "sqlite-shm")!]
            let destSqliteURLs      = [self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite"), self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite-wal"), self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite-shm")]
            
            for index in 0 ..< sourceSqliteURLs.count {
                do {
                    try NSFileManager.defaultManager().copyItemAtURL(sourceSqliteURLs[index], toURL: destSqliteURLs[index])
                } catch {
                    print(error)
                }
            }
        }
        
        print("DB Path = \(url)")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got. //
            var dict                                = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey]         = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey]  = failureReason
            dict[NSUnderlyingErrorKey]              = error as NSError
            let wrappedError                        = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.//
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.//
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}

