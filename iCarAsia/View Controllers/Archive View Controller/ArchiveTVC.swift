//
//  ArchiveTVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/19/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class ArchiveTVC: UITableViewController {

    
    var arrayDialogs      = NSMutableArray()
    var arraySelectedChat = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.navigationItem.title = "Archived Chats"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        
        self.tableView.tableFooterView                      = UIView()
        self.tableView.allowsMultipleSelectionDuringEditing = true
        
        self.navigationController?.toolbar.tintColor        = UIColor(red: 76/255.0, green: 118/255.0, blue: 234/255.0, alpha: 1.0)
        self.navigationController?.toolbar.barTintColor     = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
        
        self.setUpNavBarAndToolBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavBarAndToolBar (){
        
        let buttonEdit = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action: #selector(self.editAction))
        self.navigationItem.rightBarButtonItem          = buttonEdit
        self.navigationController!.setToolbarHidden(true, animated: true)

        if arrayDialogs.count == 0{
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
        
        
        let buttonSelectAll     = UIBarButtonItem(title: "Select All", style: .Plain, target: self, action: #selector(self.selectAllDialog))
        //let buttonUnarchive     = UIBarButtonItem(title: "Archive", style: .Plain, target: self, action: #selector(self.unArchiveChat))
        //buttonUnarchive.enabled  = false
        let buttonDelete        = UIBarButtonItem(title: "Delete", style: .Plain, target: self, action: #selector(self.deleteAction))
        buttonDelete.enabled    = false
        let flexibleSpace       = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        self.toolbarItems       = [buttonSelectAll , flexibleSpace , buttonDelete] // [buttonSelectAll , flexibleSpace , buttonUnarchive , flexibleSpace , buttonDelete]
    }
    
    
    func editAction(){
        
        if self.tableView.editing{
            
            arraySelectedChat.removeAllObjects()
            self.tableView.editing                          = false
            //self.changeTabBar(false, animated: true)
            self.navigationController!.setToolbarHidden(true, animated: true)
            
            self.navigationItem.rightBarButtonItem?.title   =  "Edit"
            self.toolbarItems![0].title                     = "Select All"
            self.toolbarItems![2].enabled                   = false
            //self.toolbarItems![4].enabled                   = false
            //self.tableView.tableHeaderView                  = searchController.searchBar
            
        }
        else{
            arraySelectedChat.removeAllObjects()
            self.tableView.editing          = true
            //self.changeTabBar(true, animated: true)
            self.navigationController!.setToolbarHidden(false, animated: true)
            
            self.navigationItem.rightBarButtonItem?.title   =  "Done"
            //self.tableView.tableHeaderView                  = UIView()
        }
    }
    
    func selectAllDialog(){
        
        
        if self.toolbarItems![0].title == "Select All"{
            
            self.toolbarItems![0].title         = "Deselect All"
            self.toolbarItems![2].enabled       = true
            //self.toolbarItems![4].enabled       = true
            
            arraySelectedChat.removeAllObjects()
            
            for i in 0..<arrayDialogs.count {
                self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0), animated: false, scrollPosition: .None)
                arraySelectedChat.addObject(NSIndexPath(forRow: i, inSection: 0))
            }
            
        }else{
            
            self.toolbarItems![0].title         = "Select All"
            self.toolbarItems![2].enabled       = false
            //self.toolbarItems![4].enabled       = false
            
            arraySelectedChat.removeAllObjects()
            for i in 0..<arrayDialogs.count {
                self.tableView.deselectRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0), animated: false)
            }
            
        }
    }
    
    func unArchiveChat(){
        
    }
    func deleteAction(){
        
        
        _ = AlertView(title:"DELETE_CHAT".localized , message:"SA_STR_DO_YOU_REALLY_WANT_TO_DELETE_SELECTED_DIALOG".localized , cancelButtonTitle: "SA_STR_CANCEL".localized, otherButtonTitle: ["SA_STR_DELETE".localized], didClick:{ (buttonIndex) -> Void in
            
            guard buttonIndex == 1 else {
                return
            }
            
            self.tableView.editing                          = false
            self.navigationItem.rightBarButtonItem?.title   = "Edit"
            self.toolbarItems![0].title                     = "Select All"
            self.toolbarItems![2].enabled                   = false
            //self.toolbarItems![4].enabled                   = false
            self.navigationController!.setToolbarHidden(true, animated: true)
            
            SVProgressHUD.showWithStatus("SA_STR_DELETING".localized, maskType: SVProgressHUDMaskType.Clear)
            var deletedDialogsCount = 0
            
            for i in 0 ..< self.arraySelectedChat.count{
                
                let indexPath           = self.arraySelectedChat[i] as! NSIndexPath
                let dialog              = self.arrayDialogs[indexPath.row] as! QBChatDialog;
                let deleteDialogBlock   = { (dialog: QBChatDialog!) -> Void in
                    
                    // Deletes dialog from server and cache. //
                    ServicesManager.instance().chatService.deleteDialogWithID(dialog.ID!, completion: { (response: QBResponse!) -> Void in
                        
                        guard response.success else {
                            deletedDialogsCount += 1
                            SVProgressHUD.showErrorWithStatus("SA_STR_ERROR_DELETING".localized)
                            print(response.error?.error)
                            return
                        }
                        deletedDialogsCount    += 1
                        if deletedDialogsCount  == self.arraySelectedChat.count {
                            
                            for i in 0 ..< self.arraySelectedChat.count{
                                let indexPath           = self.arraySelectedChat[i] as! NSIndexPath
                                self.arrayDialogs.removeObjectAtIndex(indexPath.row)
                            }
                            self.tableView.reloadData()
                            self.arraySelectedChat.removeAllObjects()
                            
                            if self.arrayDialogs.count == 0{
                                self.navigationItem.rightBarButtonItem?.enabled = false
                            }
                            
                            SVProgressHUD.showSuccessWithStatus("SA_STR_DELETED".localized)
                        }
                    })
                }
                
                if dialog.type == QBChatDialogType.Private {
                    deleteDialogBlock(dialog)
                } else {
                    // Group //
                    let occupantIDs = dialog.occupantIDs!.filter( {$0 != ServicesManager.instance().currentUser()?.ID} )
                    dialog.occupantIDs = occupantIDs
                    let userLogin = ServicesManager.instance().currentUser()?.login ?? ""
                    let notificationMessage = "User \(userLogin) " + "SA_STR_USER_HAS_LEFT".localized
                    // Notifies occupants that user left the dialog. //
                    ServicesManager.instance().chatService.sendNotificationMessageAboutLeavingDialog(dialog, withNotificationText: notificationMessage, completion: { (error : NSError?) -> Void in
                        deleteDialogBlock(dialog)
                    })
                }
            }
        })
    }

    // MARK: - Table view data source & delegates -
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayDialogs.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90.0
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 50.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let labelArchivedChatCount          = UILabel()
        labelArchivedChatCount.frame        = CGRectMake(8, 14, CGRectGetWidth(tableView.frame)-16, 22)
        labelArchivedChatCount.text         = "Archived chats (\(arrayDialogs.count))"
        labelArchivedChatCount.font         = UIFont.systemFontOfSize(14)
        
        let borderImageView                 = UIImageView()
        borderImageView.frame               = CGRectMake(0, 48.5, CGRectGetWidth(tableView.frame), 1.5)
        borderImageView.alpha               = 0.6
        borderImageView.backgroundColor     = UIColor.lightGrayColor()
        
        let viewHeader                      = UIView()
        viewHeader.frame                    = CGRectMake(0, 0, CGRectGetWidth(self.view.frame
            ), 50)
        viewHeader.backgroundColor          = UIColor.whiteColor()
        
        viewHeader.addSubview(labelArchivedChatCount)
        viewHeader.addSubview(borderImageView)
        return viewHeader
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("dialogcell", forIndexPath: indexPath) as! DialogTableViewCell
        //cell.selectionStyle = UITableViewCellSelectionStyle.Blue
        cell.tintColor = UIColor.redColor()

        
        if (self.arrayDialogs.count < indexPath.row) {
            return cell
        }
        
        let chatDialog = arrayDialogs[indexPath.row]
        
        cell.exclusiveTouch                     = true
        cell.contentView.exclusiveTouch         = true
        
        let randomNumber                        = arc4random_uniform(10) + 1
        
        cell.tag                                = indexPath.row
        cell.dialogID                           = chatDialog.ID!!
        
        let cellModel                           = ArchiveTableViewCellModel(dialog: chatDialog as! QBChatDialog)
        cell.dialogLastMessage?.text            = chatDialog.lastMessageText
        cell.dialogName?.text                   = cellModel.textLabelText
        cell.dialogTypeImage.image              = UIImage(named: "ImgChatProfileBG-\(randomNumber)") // cellModel.dialogIcon //
        cell.unreadMessageCounterLabel.text     = cellModel.unreadMessagesCounterLabelText
        
        if cellModel.textLabelText.characters.count > 1{
            cell.dialogNameInitial?.text            = cellModel.textLabelText.substringToIndex(cellModel.textLabelText.startIndex.advancedBy(2)).uppercaseString
        }
        cell.unreadMessageCounterHolder.hidden  = cellModel.unreadMessagesCounterHiden
        
        if let date : NSDate = chatDialog.lastMessageDate{
            cell.timeLabel.text                 = Utility.sharedInstance.timeStringByDate(date)
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        
        if tableView.editing {
            
            arraySelectedChat.addObject(indexPath)
            if arraySelectedChat.count > 0 {
                self.toolbarItems![2].enabled                   = true
                //self.toolbarItems![4].enabled                   = true
            }else{
                self.toolbarItems![2].enabled                   = false
                //self.toolbarItems![4].enabled                   = false
            }
            
            if arraySelectedChat.count == self.arrayDialogs.count{
                self.toolbarItems![0].title = "Deselect All"
            }
            
        }
        else{
            
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            if (ServicesManager.instance().isProcessingLogOut!) {
                return
            }
            
            let dialog : QBChatDialog  = arrayDialogs[indexPath.row] as! QBChatDialog
            //guard dialog == self.dialogs()?[indexPath.row] else {
            //    return
            //}
            //self.performSegueWithIdentifier("SA_STR_SEGUE_GO_TO_CHAT".localized , sender: dialog)
            
            let chatViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
            chatViewController.dialog                   = dialog
            chatViewController.hidesBottomBarWhenPushed = true
            self.navigationController!.pushViewController(chatViewController, animated: true)

        }
    }
    
    override func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath?{
        
        if tableView.editing {
            arraySelectedChat.removeObject(indexPath)
            self.toolbarItems![0].title = "Select All"
            if arraySelectedChat.count > 0 {
                self.toolbarItems![2].enabled                   = true
                //self.toolbarItems![4].enabled                   = true
            }else{
                self.toolbarItems![2].enabled                   = false
                //self.toolbarItems![4].enabled                   = false
            }
        }
        return indexPath
    }
    
    
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
     }
    
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let unArchiveAction               = UITableViewRowAction(style: .Normal, title: "         ") { (rowAction:UITableViewRowAction, indexPath:NSIndexPath) -> Void in
            
            
            let arrayArchiveChatIds = Utility.sharedInstance.fetchArchivedChatIDs()
            let chatDialog          = self.arrayDialogs[indexPath.row] as! QBChatDialog
            arrayArchiveChatIds.removeObject(chatDialog.ID!)
            Utility.sharedInstance.saveArchivedChatIDs(arrayArchiveChatIds)
            
            self.arrayDialogs.removeObjectAtIndex(indexPath.row)
            self.tableView.reloadData()
            
            if self.arrayDialogs.count == 0{
                self.navigationItem.rightBarButtonItem?.enabled = false
            }
            
            NSNotificationCenter.defaultCenter().postNotificationName("updateDialogsTable", object: nil)
        }
        unArchiveAction.backgroundColor   = UIColor(patternImage: UIImage(named: "ArchiveButton")!)
        return [unArchiveAction]
    }
    

     /*override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    
     }*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


class ArchiveTableViewCellModel: NSObject {
    
    var detailTextLabelText: String     = ""
    var textLabelText: String           = ""
    var unreadMessagesCounterLabelText : String?
    var unreadMessagesCounterHiden      = true
    var dialogIcon : UIImage?
    
    init(dialog: QBChatDialog){
        
        super.init()
        
        switch (dialog.type){
        case .PublicGroup:
            self.detailTextLabelText = "SA_STR_PUBLIC_GROUP".localized
        case .Group:
            self.detailTextLabelText = "SA_STR_GROUP".localized
        case .Private:
            self.detailTextLabelText = "SA_STR_PRIVATE".localized
            
            if dialog.recipientID == -1 {
                return
            }
            /* Getting recipient from users service */
            if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.userWithID(UInt(dialog.recipientID)) {
                self.textLabelText = recipient.login ?? recipient.email!
            }
        }
        
        if self.textLabelText.isEmpty {
            /* Group chat */
            if let dialogName = dialog.name {
                self.textLabelText = dialogName
            }
        }
        // Unread messages counter label //
        if (dialog.unreadMessagesCount > 0) {
            
            var trimmedUnreadMessageCount : String
            if dialog.unreadMessagesCount > 99 {
                trimmedUnreadMessageCount = "99+"
            } else {
                trimmedUnreadMessageCount = String(format: "%d", dialog.unreadMessagesCount)
            }
            self.unreadMessagesCounterLabelText = trimmedUnreadMessageCount
            self.unreadMessagesCounterHiden     = false
        } else {
            self.unreadMessagesCounterLabelText = nil
            self.unreadMessagesCounterHiden     = true
        }
        
        /* Dialog icon */
        if dialog.type == .Private {
            self.dialogIcon = UIImage(named: "user")
        } else {
            self.dialogIcon = UIImage(named: "group")
        }
    }
}


