//
//  LoadingVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/22/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.performSelector(#selector(self.showLoadingView), withObject: nil, afterDelay: 0.5)
    }
    
    func showLoadingView () {
        SVProgressHUD.showWithStatus("SA_STR_LOADING".localized, maskType: SVProgressHUDMaskType.Clear)
        //self.getAppData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func getAppData () {
        
        SVProgressHUD.showWithStatus("SA_STR_LOADING".localized, maskType: SVProgressHUDMaskType.Clear)
        
        let urlString: String               = "http://ex-api.used246.icarasia.net/v2.0/id/id/dealerappdata"
        let request: NSMutableURLRequest    = NSMutableURLRequest(URL: NSURL(string: urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
        request.HTTPMethod                  = "GET"
        request.setValue("bf9d0bc5-829e-4fab-9ccf-737a34e28f44", forHTTPHeaderField: "token")
        request.setValue("iOS_MDA", forHTTPHeaderField: "platform")
        request.setValue("1.0", forHTTPHeaderField: "api_version")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.timeoutInterval             = 90.0
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            
            do{
                let resonseDictionary : NSMutableDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSMutableDictionary
                //print(resonseDictionary)
                
                let appDelegate     = UIApplication.sharedApplication().delegate as! AppDelegate
                let managedContext  = appDelegate.managedObjectContext
                
                /* Save Vehicle Info in DB ( Smile ) */
                
                let arrayVehicleInfo = resonseDictionary.valueForKey("vehicleInfo") as! NSArray
                
                for vehicleInfoIndex in 0..<arrayVehicleInfo.count {
                    
                    print("Index : \(vehicleInfoIndex)")
                    
                    let dictVehicleInfo =  arrayVehicleInfo[vehicleInfoIndex]
                    let entity          =  NSEntityDescription.entityForName("VehicleInfo", inManagedObjectContext:managedContext)
                    let vehicleInfo     = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                    
                    if let value = dictVehicleInfo["icar_vcode"] as? String {
                        vehicleInfo.setValue(value, forKey: "icar_vcode")
                    }else{
                        vehicleInfo.setValue("", forKey: "icar_vcode")
                    }
                    
                    if let value = dictVehicleInfo["series"] as? String {
                        vehicleInfo.setValue(value, forKey: "series")
                    }else{
                        vehicleInfo.setValue("", forKey: "series")
                    }
                    
                    if let value = dictVehicleInfo["make"] as? String {
                        vehicleInfo.setValue(value, forKey: "make")
                    }else{
                        vehicleInfo.setValue("", forKey: "make")
                    }
                    
                    if let value = dictVehicleInfo["model"] as? String {
                        vehicleInfo.setValue(value, forKey: "model")
                    }else{
                        vehicleInfo.setValue("", forKey: "model")
                    }
                    
                    if let value = dictVehicleInfo["status"] as? String {
                        vehicleInfo.setValue(value, forKey: "status")
                    }else{
                        vehicleInfo.setValue("", forKey: "status")
                    }
                    
                    if let year    = dictVehicleInfo["year"]{
                        vehicleInfo.setValue(year, forKey: "year")
                    }else{
                        vehicleInfo.setValue(0, forKey: "year")
                    }
                    
                    if let value = dictVehicleInfo["engine_cc"] as? String{
                        
                        if let engineCC = dictVehicleInfo["engine_cc"]{
                            vehicleInfo.setValue(engineCC, forKey: "engine_cc")
                        }else{
                            vehicleInfo.setValue(0, forKey: "engine_cc")
                        }
                        
                    }else{
                        vehicleInfo.setValue(0, forKey: "engine_cc")
                    }
                    
                    if let value = dictVehicleInfo["vehicle_name"] as? String {
                        vehicleInfo.setValue(value, forKey: "vehicle_name")
                    }else{
                        vehicleInfo.setValue("", forKey: "vehicle_name")
                    }
                    
                    if let value = dictVehicleInfo["fuel_type"] as? String {
                        vehicleInfo.setValue(value, forKey: "fuel_type")
                    }else{
                        vehicleInfo.setValue("", forKey: "fuel_type")
                    }
                    
                    do {
                        try managedContext.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                }
                
                
                /* Save Descriptions in DB ( Smile ) */
                
                let arrayDescriptions = resonseDictionary.valueForKey("descriptions") as! NSArray
                
                for desIndex in 0..<arrayDescriptions.count {
                    
                    print("Index : \(desIndex)")
                    
                    let dictDescription =  arrayDescriptions[desIndex]
                    let entity          =  NSEntityDescription.entityForName("Descriptions", inManagedObjectContext:managedContext)
                    let descriptions    = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                    
                    if let value = dictDescription["id"] as? NSInteger {
                        descriptions.setValue(value, forKey: "id")
                    }else{
                        descriptions.setValue("", forKey: "id")
                    }
                    
                    if let value = dictDescription["name"] as? String {
                        descriptions.setValue(value, forKey: "name")
                    }else{
                        descriptions.setValue("", forKey: "name")
                    }
                    
                    if let value = dictDescription["description"] as? String {
                        descriptions.setValue(value, forKey: "dscription")
                    }else{
                        descriptions.setValue("", forKey: "dscription")
                    }
                    
                    do {
                        try managedContext.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                }
                
                /* Save Colors in DB */
                
                let arrayColors = resonseDictionary.valueForKey("color") as! NSArray
                
                for colorIndex in 0..<arrayColors.count {
                    
                    let dictColor       =  arrayColors[colorIndex]
                    let entity          =  NSEntityDescription.entityForName("Color", inManagedObjectContext:managedContext)
                    let color           = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                    
                    if let value = dictColor["name"] as? String {
                        color.setValue(value, forKey: "color_name")
                    }else{
                        color.setValue("", forKey: "color_name")
                    }
                    
                    if let value = dictColor["hex_value"] as? String {
                        color.setValue(value, forKey: "hex_value")
                    }else{
                        if let value = dictColor["hex_value"] as? NSInteger {
                            color.setValue("\(value)", forKey: "hex_value")
                        }else{
                            color.setValue("", forKey: "hex_value")
                        }
                    }
                    if let value = dictColor["css_class"] as? String {
                        color.setValue(value, forKey: "css_class")
                    }else{
                        color.setValue("", forKey: "css_class")
                    }
                    
                    do {
                        try managedContext.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                }
                
                
                /* Save Location , Area & Cities in DB */
                
                let arrayLocationsKeys = NSArray (arrayLiteral: (resonseDictionary.valueForKey("locations")?.allKeys)!)
                print("\(arrayLocationsKeys)")
                let arrayAllLocations  = arrayLocationsKeys[0];
                
                for locationIndex in 0..<arrayAllLocations.count-1 {
                    
                    print("Saved location Index : \(locationIndex)")
                    
                    let dictLocationDetails = resonseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex))")
                    let entity              = NSEntityDescription.entityForName("Locations", inManagedObjectContext:managedContext)
                    let location            = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                    
                    location.setValue(dictLocationDetails?.valueForKey("name"), forKey: "location_name")
                    location.setValue(dictLocationDetails?.valueForKey("id"), forKey: "location_id")
                    
                    do {
                        try managedContext.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    
                    
                    let arrayAreaKeys = NSArray (arrayLiteral: (resonseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area")?.allKeys)!)
                    let arrayAllAreas  = arrayAreaKeys[0];
                    
                    for areaIndex in 0..<arrayAllAreas.count {
                        
                        print("Saved area Index : \(areaIndex)")
                        
                        let dictAreaDetails = resonseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area.\(arrayAllAreas.objectAtIndex(areaIndex))")
                        let entity          = NSEntityDescription.entityForName("Areas", inManagedObjectContext:managedContext)
                        let area            = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                        
                        area.setValue(dictAreaDetails?.valueForKey("id"), forKey: "area_id")
                        area.setValue(dictAreaDetails?.valueForKey("name"), forKey: "area_name")
                        area.setValue(dictLocationDetails?.valueForKey("name"), forKey: "location_name")
                        
                        do {
                            try managedContext.save()
                        } catch let error as NSError  {
                            print("Could not save \(error), \(error.userInfo)")
                        }
                        
                        
                        
                        if((resonseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area.\(arrayAllAreas.objectAtIndex(areaIndex)).city")) != nil){
                            
                            let arrayCitiesKeys = NSArray (arrayLiteral: (resonseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area.\(arrayAllAreas.objectAtIndex(areaIndex)).city")?.allKeys)!)
                            let arrayAllCities  = arrayCitiesKeys[0];
                            
                            for cityIndex in 0..<arrayAllCities.count {
                                
                                print("Saved City Index : \(cityIndex)")
                                
                                let dictCityDetails = resonseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area.\(arrayAllAreas.objectAtIndex(areaIndex)).city.\(arrayAllCities.objectAtIndex(cityIndex))")
                                let entity          = NSEntityDescription.entityForName("Cities", inManagedObjectContext:managedContext)
                                let city            = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                                city.setValue(dictCityDetails?.valueForKey("id"), forKey: "city_id")
                                city.setValue(dictCityDetails?.valueForKey("name"), forKey: "city_name")
                                city.setValue(dictAreaDetails?.valueForKey("name"), forKey: "area_name")
                                
                                
                                do {
                                    try managedContext.save()
                                } catch let error as NSError  {
                                    print("Could not save \(error), \(error.userInfo)")
                                }
                            }
                        }
                    }
                }
                
                
                dispatch_async(dispatch_get_main_queue()) { // [unowned self] in
                    SVProgressHUD.dismiss()
                }
            }
            catch {
                print("json error: \(error)")
            }
        }
        task.resume()
        
    }
    
}
