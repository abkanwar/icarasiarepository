//
//  CreateAdVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class CreateAdVC: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var textFieldPlateNumber: UITextField!
    @IBOutlet weak var textFieldRefrenceNumber: UITextField!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Create Ad"
        self.navigationItem.leftBarButtonItem   = nil
        self.navigationItem.hidesBackButton     = true
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        
        let croosButton = Utility.sharedInstance.navigationBarCrossButton( "Cancel" )
        croosButton.addTarget(self, action: #selector(popToRoot), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView:croosButton)
        
        self.container.layer.borderColor    = UIColor( red: 236/255.0 , green: 236/255.0 , blue: 236/255.0 , alpha: 1.0).CGColor
        self.buttonNext.selected            = true
        
        /*if !Utility.sharedInstance.isDBCreated() {
            let appDelegate     = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.clearCoreDataStore()
            ServiceManager.sharedInstance.getAppData()
        }*/
    }
    
    func popToRoot(){
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentIndexChanged(segmentedControl: UISegmentedControl) {
       
        switch segmentedControl.selectedSegmentIndex{
        case 0:
            buttonNext.selected     = true
        case 1:
            if (textFieldPlateNumber.text?.characters.count > 0){
                buttonNext.selected = true
            }else{
                buttonNext.selected = false
            }
        default:
            break;
        }
    }
    
    @IBAction func buttonNextMethod(sender: AnyObject) {
        
        if buttonNext.selected {
            
            AdDetailStorage.sharedInstance.plateNumber      = self.textFieldPlateNumber.text!
            AdDetailStorage.sharedInstance.refrenceNumber   = self.textFieldRefrenceNumber.text!
            
            let adDetaillsVC                   = self.storyboard!.instantiateViewControllerWithIdentifier("AdDetaillsVC") as! AdDetaillsVC
            self.navigationController?.pushViewController(adDetaillsVC, animated: true)

        }
}
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool{
        
        if segmentControl.selectedSegmentIndex == 0{
            buttonNext.selected = true
        }
        else{
            if (textFieldPlateNumber.text?.characters.count > 0){
                buttonNext.selected = true
            }else{
                buttonNext.selected = false
            }
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        //let trimmed             = string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if (string == " " && range.location == 0) {
            return false
        }
        if textField.text!.length > 20 && range.length == 0 && textField == textFieldPlateNumber {
            return false
        }
        else if textField.text!.length > 30 && range.length == 0 && textField == textFieldRefrenceNumber {
            return false
        }
        else {
            return true
        }
        
    }
    
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        
        if textField == textFieldPlateNumber{
            textFieldRefrenceNumber.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

    
}
