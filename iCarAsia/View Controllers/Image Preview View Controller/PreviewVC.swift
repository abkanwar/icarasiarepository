//
//  PreviewVC.swift
//  CameraDemo
//
//  Created by Raman Kant on 8/30/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class PreviewVC: UIViewController {
    
    
    @IBOutlet weak var collectionViewImages: UICollectionView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var libraryButton: UIButton!
    
    @IBOutlet weak var headerCountLabel: UILabel!
    
    //var assets = [DKAsset]()
    
    internal var indexSelected : NSIndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongGesture))
        self.collectionViewImages.addGestureRecognizer(longPressGesture)
        
        self.setInitialImage()
        self.updateImagesCountLabels()
        
        
        if NSUserDefaults.standardUserDefaults().boolForKey("TutorialShown") == false {
            self.addTutorialView()
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reloadCollectionViewImages), name:"ReloadCollectionViewImages", object: nil)
        }
        

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController!.interactivePopGestureRecognizer!.enabled  = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController!.interactivePopGestureRecognizer!.enabled  = true
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    func reloadCollectionViewImages () {
        self.collectionViewImages.reloadData()
        self.setInitialImage()
    }
    
    func setInitialImage () {
        
        indexSelected = NSIndexPath( forRow: 0 , inSection: 0 )
        let asset   = AdDetailStorage.sharedInstance.assets[indexSelected.row]
        asset.fetchImageWithSize(previewImageView.frame.size.toPixel(), completeBlock: { image, info in
            self.previewImageView.image = image
        })
    }
    
    func updateImagesCountLabels () {
        
        if AdDetailStorage.sharedInstance.assets.count == 0 {
            headerCountLabel.text = "Select Images"
        }
        else{
            if AdDetailStorage.sharedInstance.assets.count == 1 {
                headerCountLabel.text = "\(AdDetailStorage.sharedInstance.assets.count) Image"
            }else{
                headerCountLabel.text = "\(AdDetailStorage.sharedInstance.assets.count) Images"
            }
        }
    }
    
    @IBAction func deleteAction(sender: AnyObject) {
        
        
        if AdDetailStorage.sharedInstance.assets.count == 5{
            
            SWMessage.sharedInstance.dismissActiveNotification()
            SWMessage.sharedInstance.showNotificationWithTitle("You must add atleat 5 images per listing.", subtitle: nil, type: .Error)
            return
        }
        
        let deleteAlert = UIAlertController(title: "Delete Image ?", message: nil, preferredStyle: .Alert)
        deleteAlert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Cancel, handler: nil));
        deleteAlert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
            
            AdDetailStorage.sharedInstance.assets.removeAtIndex(self.indexSelected.row)
            self.collectionViewImages.deleteItemsAtIndexPaths([self.indexSelected])
            
            self.previewImageView.image = nil
            if AdDetailStorage.sharedInstance.assets.count > 0 {
                
                if self.indexSelected.row-1 >= 0 {
                    
                    let asset   = AdDetailStorage.sharedInstance.assets[self.indexSelected.row-1]
                    asset.fetchImageWithSize(self.previewImageView.frame.size.toPixel(), completeBlock: { image, info in
                        self.previewImageView.image = image
                    })
                    self.indexSelected = NSIndexPath( forRow: self.indexSelected.row-1 , inSection: 0 )
                }else{
                    
                    let asset   = AdDetailStorage.sharedInstance.assets[self.indexSelected.row]
                    asset.fetchImageWithSize(self.previewImageView.frame.size.toPixel(), completeBlock: { image, info in
                        self.previewImageView.image = image
                    })
                    self.indexSelected = NSIndexPath( forRow: self.indexSelected.row , inSection: 0 )
                }
            }
            self.updateImagesCountLabels()
            
        }));
        presentViewController(deleteAlert, animated: true, completion: nil);
        
    }
    
    @IBAction func editAction(sender: AnyObject) {
        
        let editImageVC    = self.storyboard?.instantiateViewControllerWithIdentifier("EditImageVC") as! EditImageVC
        self.navigationController?.pushViewController(editImageVC, animated: true)
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func nextAction(sender: AnyObject) {
        
        let reorderImagesVC = self.storyboard?.instantiateViewControllerWithIdentifier("ReorderImagesVC")
        self.navigationController?.pushViewController(reorderImagesVC!, animated: true)
        
    }
    
    @IBAction func cameraAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func libraryAction(sender: AnyObject) {
        
        let pickerController                    = DKImagePickerController()
        pickerController.assetType              = .AllPhotos
        pickerController.allowsLandscape        = false
        pickerController.allowMultipleTypes     = true
        pickerController.sourceType             = .Photo
        pickerController.singleSelect           = false
        pickerController.defaultSelectedAssets  = AdDetailStorage.sharedInstance.assets
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            AdDetailStorage.sharedInstance.assets = assets
            self.updateImagesCountLabels()
            self.setInitialImage()
            self.collectionViewImages.reloadData()
        }
        if UI_USER_INTERFACE_IDIOM() == .Pad {
            pickerController.modalPresentationStyle = .FormSheet
        }
        self.presentViewController(pickerController, animated: true) {
        }
        
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    internal func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return AdDetailStorage.sharedInstance.assets.count ?? 0
    }
    
    internal func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        //let asset       = self.assets[indexPath.row]
        var cell:               UICollectionViewCell?
        var imageView:          UIImageView?
        var imageViewRibbon:    UIImageView?
        
        //if asset.isVideo {
        //    cell        = collectionView.dequeueReusableCellWithReuseIdentifier("CellVideo", forIndexPath: indexPath)
        //    imageView   = cell?.contentView.viewWithTag(1) as? UIImageView
        //} else {
        cell            = collectionView.dequeueReusableCellWithReuseIdentifier("CellImage", forIndexPath: indexPath)
        imageView       = cell?.contentView.viewWithTag(1) as? UIImageView
        //}
        imageViewRibbon = cell?.contentView.viewWithTag(2) as? UIImageView
        indexPath.row  == 0 ? (imageViewRibbon?.hidden = false) : (imageViewRibbon?.hidden = true)
        
        if let cell = cell, imageView = imageView
        {
            let layout  = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            let tag     = indexPath.row + 1
            cell.tag    = tag
            
            if indexPath.row < AdDetailStorage.sharedInstance.assets.count {
                
                let asset   = AdDetailStorage.sharedInstance.assets[indexPath.row]
                asset.fetchImageWithSize(layout.itemSize.toPixel(), completeBlock: { image, info in
                    if cell.tag == tag {
                        imageView.image = image
                    }
                })
            }else{
                imageView.image = UIImage( named:  "ImgThumbnail")
            }
        }
        return cell!
    }
    
    internal func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let asset   = AdDetailStorage.sharedInstance.assets[indexPath.row]
        asset.fetchImageWithSize(previewImageView.frame.size.toPixel(), completeBlock: { image, info in
            self.previewImageView.image = image
        })
        indexSelected = indexPath
    }
    
    // MARK: collection view cell layout / size
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let   returnSize = CGSizeMake((CGRectGetWidth(self.view.frame)-15)/4, (CGRectGetWidth(self.view.frame)-15)/4)
        return returnSize
    }
    
    
    // MARK: collection view cell paddings
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
        // top, left, bottom, right
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, moveItemAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        
        let tempAsset                           = AdDetailStorage.sharedInstance.assets[sourceIndexPath.row]
        //self.assets[sourceIndexPath.row]        = self.assets[destinationIndexPath.row]
        //self.assets[destinationIndexPath.row]   = tempAsset
        AdDetailStorage.sharedInstance.assets.removeAtIndex(sourceIndexPath.row)
        AdDetailStorage.sharedInstance.assets.insert(tempAsset, atIndex: destinationIndexPath.row)
        
        
        if sourceIndexPath.row != 1 && destinationIndexPath.row != 1{
            destinationIndexPath.row == 0 ?  (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath])) : (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath , NSIndexPath (forItem: 1, inSection: 0)]))
        }
        else{
            destinationIndexPath.row == 0 ?  (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath])) : (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath]))

        }
        
    }
    
    
    //MARK:  - Long Press Gesture -
    
    func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        
        switch(gesture.state) {
            
        case UIGestureRecognizerState.Began:
            guard let selectedIndexPath = self.collectionViewImages.indexPathForItemAtPoint(gesture.locationInView(self.collectionViewImages)) else {
                break
            }
            if #available(iOS 9.0, *) {
                if selectedIndexPath.row < AdDetailStorage.sharedInstance.assets.count  {
                    self.collectionViewImages.beginInteractiveMovementForItemAtIndexPath(selectedIndexPath)
                }
            } else {
                // Fallback on earlier versions //
            }
        case UIGestureRecognizerState.Changed:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.updateInteractiveMovementTargetPosition(gesture.locationInView(gesture.view!))
            } else {
                // Fallback on earlier versions //
            }
        case UIGestureRecognizerState.Ended:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.endInteractiveMovement()
            } else {
                // Fallback on earlier versions //
            }
        default:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.cancelInteractiveMovement()
            } else {
                // Fallback on earlier versions //
            }
        }
    }
    
    
    //MARK:  - Add Tutorial Action -
    
    func addTutorialView () {
        
        let tutorialView    = TutorialView(frame: self.view.frame)
        self.view.addSubview(tutorialView)
        
        let indexPath            = NSIndexPath( forRow: 1 , inSection: 0)
        let attributes           = self.collectionViewImages.layoutAttributesForItemAtIndexPath(indexPath)
        let cellRect             = attributes!.frame
        let cellFrameInSuperview = self.collectionViewImages.convertRect(cellRect, toView: self.view)
        
        let imageViewHiglighted            = UIImageView()
        imageViewHiglighted.frame          = cellFrameInSuperview
        imageViewHiglighted.contentMode    = .ScaleAspectFill
        imageViewHiglighted.clipsToBounds  = true
        imageViewHiglighted.userInteractionEnabled = true
        let asset   = AdDetailStorage.sharedInstance.assets[indexPath.row]
        asset.fetchImageWithSize(imageViewHiglighted.frame.size.toPixel(), completeBlock: { image, info in
            imageViewHiglighted.image = image
        })
        tutorialView.addSubview(imageViewHiglighted)
        
        let imageViewTouchIcon          = UIImageView()
        imageViewTouchIcon.frame        = cellFrameInSuperview
        imageViewTouchIcon.contentMode  = .ScaleAspectFit
        imageViewTouchIcon.image        = UIImage( named: "IconTouch")
        imageViewTouchIcon.frame        = CGRectMake(CGRectGetMidX(imageViewHiglighted.frame) + 15, CGRectGetMidY(imageViewHiglighted.frame) + 15, CGRectGetWidth(self.view.frame) * 0.096875, CGRectGetHeight(self.view.frame) * 0.0725)
        tutorialView.addSubview(imageViewTouchIcon)
        
        UIView.animateWithDuration(0.7, delay:0, options: [.Repeat, .Autoreverse], animations: {
            
            imageViewTouchIcon.frame        = CGRectMake(CGRectGetMidX(imageViewHiglighted.frame) + 15 , CGRectGetMidY(imageViewHiglighted.frame) + 35, CGRectGetWidth(self.view.frame) * 0.096875, CGRectGetHeight(self.view.frame) * 0.0725)
            }, completion: {(finished: Bool) -> Void in
                CGRectMake(CGRectGetMidX(imageViewHiglighted.frame) + 15, CGRectGetMidY(imageViewHiglighted.frame) + 15, CGRectGetWidth(self.view.frame) * 0.096875, CGRectGetHeight(self.view.frame) * 0.0725)
        })
        
        let lngPrsGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lngPrsGesture.minimumPressDuration = 0.5
        imageViewHiglighted.addGestureRecognizer(lngPrsGesture)
    }
    
    func handleLongPress(gestureReconizer: UILongPressGestureRecognizer)  {
        
       if gestureReconizer.state == .Began{
            let tutorialView    = self.view.viewWithTag(222)
            tutorialView?.removeFromSuperview()
            
            let reorderImagesVC = self.storyboard?.instantiateViewControllerWithIdentifier("ReorderImagesVC")
            self.navigationController?.pushViewController(reorderImagesVC!, animated: false)
        }
    }
    
    //MARK: - Tutorial View Class -
    
    private class TutorialView: UIView {
        
        private let imageView   = UIImageView()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.userInteractionEnabled = true
            self.tag                    = 222
            self.createUI()
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func createUI() {
            
            let transImageView              = UIImageView(frame: self.frame)
            transImageView.alpha            = 0.75
            transImageView.backgroundColor  = UIColor.blackColor()
            self.addSubview(transImageView)
            
            let space       = CGRectGetWidth(self.frame) * 0.0109375
            var xPos        = CGRectGetWidth(self.frame) * 0.1375
            let yPos        = CGRectGetHeight(self.frame) * 0.331
            for i in 0..<5 {
                
                var imageName        =  "MainPlaceholder"
                var labelText        =  "MainPlaceholder"
                i == 0 ?  (imageName = "MainPlaceholder") : (imageName = "ImgThumbnail")
                i == 0 ?  (labelText = "Main") : (labelText = "\(i+1)")
                
                let frame            = CGRectMake(xPos ,CGRectGetHeight(self.frame) * 0.331, CGRectGetWidth(self.frame) * 0.1484375, CGRectGetWidth(self.frame) * 0.1484375)
                self.addSubview(imageViewBox(frame , imageName: imageName , textLabel : labelText))
                xPos = xPos + CGRectGetWidth(self.frame) * 0.1484375 + space
            }
            
            
            let labelInfo             = UILabel()
            labelInfo.frame           = CGRectMake(CGRectGetWidth(self.frame) * 0.190625, yPos + CGRectGetWidth(self.frame) * 0.1484375 + CGRectGetWidth(self.frame) * 0.044 , CGRectGetWidth(self.frame) * 0.625, CGRectGetHeight(self.frame) * 0.057)
            labelInfo.textColor       = UIColor.lightGrayColor()
            labelInfo.textAlignment   = .Center
            labelInfo.numberOfLines   = 2
            labelInfo.text            = "To sort images,\nlong press on an image."
            labelInfo.font            = UIFont.boldSystemFontOfSize(14)
            self.addSubview(labelInfo)
            
            
            
        }
        
        func imageViewBox ( frame : CGRect , imageName : String , textLabel : String) -> UIImageView {
            
            let imageView               = UIImageView()
            imageView.frame             = frame
            imageView.image             = UIImage ( named: imageName )
            
            let labelNumber             = UILabel()
            labelNumber.frame           = CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame))
            labelNumber.textColor       = UIColor.lightGrayColor()
            labelNumber.textAlignment   = .Center
            labelNumber.text            = textLabel
            labelNumber.font            = UIFont.boldSystemFontOfSize(12)
            imageView.addSubview(labelNumber)
            
            return imageView
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
