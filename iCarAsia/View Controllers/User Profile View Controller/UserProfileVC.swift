//
//  UserProfileVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/22/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class UserProfileVC: UIViewController , UIActionSheetDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutAndHelpMethod ( sender : UIButton){
        
        let alert           = UIAlertController(title: nil , message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet);
        //alert.view.tintColor = UIColor.redColor();

        alert.addAction(UIAlertAction(title: "Help & Support", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
            print("Help & Support")
        }));
        
        alert.addAction(UIAlertAction(title: "Log Out", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
            print("Log Out")
            self.logOut()
        }));
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(action:UIAlertAction) in
            print("Cancel")
        }));
        
        //let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(action:UIAlertAction) in
        //    print("Cancel")
        //})
        //alert.addAction(cancelAction)
        //let accessoryImage = UIImage(named: "IconRectangle")!
        //cancelAction.setValue(accessoryImage, forKey: "image")
        
        presentViewController(alert, animated: true, completion: nil);
        
    }
    
    func logOut () {
        
        let logOutAlert = UIAlertController(title: "Log Out Account", message: "Are you sure you want to log out?", preferredStyle: UIAlertControllerStyle.Alert);
        logOutAlert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: {(action:UIAlertAction) in
            print("NO")
        }));
        logOutAlert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
            print("YES")
        }));
        presentViewController(logOutAlert, animated: true, completion: nil);
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
