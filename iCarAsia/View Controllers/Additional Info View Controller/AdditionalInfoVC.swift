//
//  AdditionalInfoVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 9/7/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class AdditionalInfoVC: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource {

    @IBOutlet weak var tableViewVehiclesInfo:   UITableView!
    
    @IBOutlet weak var nextButton:   UIButton!
    
    //var arrayTransmission : NSMutableArray!
    //var arrayFuelType : NSMutableArray!
    //var arrayVariant : NSMutableArray!
    //var arrayEngineCapacity : NSMutableArray!
    
    var arrayAllData = NSMutableArray()
    
    var selectedTransmissionIndex    = Int()
    var selectedFuelTypeIndex        = Int()
    var selectedVarientIndex         = Int()
    var selectedEngineCapacityIndex  = Int()
    
    var selectedTransmissionValue    = String()
    var selectedFuelTypeValue        = String()
    var selectedVarientValue         = String()
    var selectedEngineCapacityValue  = NSNumber()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Additional Info"
        tableViewVehiclesInfo.tableFooterView = UIView()
        
        let buttonBack                           = UIButton()
        buttonBack.setImage(UIImage( named: "IconBack" ), forState: .Normal)
        buttonBack.sizeToFit()
        buttonBack.addTarget(self, action: #selector(popVC), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.leftBarButtonItem    = UIBarButtonItem(customView:buttonBack)
        self.getDataToFilter("", fuelType: "", variant: "", engineCC: 0, roloadData: false)
        
        selectedTransmissionIndex    = -1
        selectedFuelTypeIndex        = -1
        selectedVarientIndex         = -1
        selectedEngineCapacityIndex  = -1
        
        selectedTransmissionValue    = ""
        selectedFuelTypeValue        = ""
        selectedVarientValue         = ""
        selectedEngineCapacityValue  = 0
    }
    
    func getDataToFilter ( transmission : String , fuelType : String , variant : String , engineCC : NSInteger , roloadData : Bool) {
        
        arrayAllData.removeAllObjects()
        
        var arrayVehicleDetails = DBHelper.sharedInstance.fetchVehiclesByYearBrandModelFromDB( AdDetailStorage.sharedInstance.year , brand: AdDetailStorage.sharedInstance.brand , model: AdDetailStorage.sharedInstance.model) as NSMutableArray
        
        
        //let predicate           = NSPredicate(format: "SELF.transmission Contains[cd] %@ AND SELF.fuel_type Contains[cd] %@ AND SELF.variant Contains[cd] %@",transmission , fuelType , variant)
        //predicate.evaluateWithObject([ "transmission" : NSNull() , "fuel_type" : NSNull() , "variant" : NSNull() ])
        //let predicate           = NSPredicate(format: "((transmission Contains[cd] %@) || (transmission != nil)) AND ((fuel_type Contains[cd] %@) || (fuel_type != nil)) AND ((variant Contains[cd] %@) || (variant != nil))",transmission , fuelType , variant)
        // AND SELF.vengine_cc Contains[cd] %d  - -   engineCC //
        
        
        
        
        //let arrayVehicleDetailsAll = DBHelper.sharedInstance.fetchVehiclesByYearBrandModelFromDB( AdDetailStorage.sharedInstance.year , brand: AdDetailStorage.sharedInstance.brand , model: AdDetailStorage.sharedInstance.model) as NSMutableArray
        let arrayTransmissionTemp   = arrayVehicleDetails.valueForKeyPath("@distinctUnionOfObjects.transmission") as! NSArray
        let arrayTransmission           = arrayTransmissionTemp.mutableCopy() as! NSMutableArray
        arrayTransmission.removeObject("")
        
        let dictTransmission        = NSMutableDictionary()
        dictTransmission.setValue("Transmission", forKey: "type")
        dictTransmission.setObject(arrayTransmission, forKey: "values")
        
        if arrayTransmission.count > 0{
            arrayAllData.addObject(dictTransmission)
        }
        
        let fromatString = NSMutableString()
        if transmission != ""{
            fromatString.appendString("SELF.transmission Contains[cd]")
            fromatString.appendString(" \"\(transmission)\"")
        }
        if fromatString != ""{
            
            let predicate           = NSPredicate(format: fromatString as String)
            let filteredArray: NSArray      = arrayVehicleDetails.filteredArrayUsingPredicate(predicate)
            print(filteredArray.count)
            
            arrayVehicleDetails = filteredArray.mutableCopy() as! NSMutableArray
        }
        
        let arrayFuelTypeTemp       = arrayVehicleDetails.valueForKeyPath("@distinctUnionOfObjects.fuel_type") as! NSArray
        let sortedArray : NSArray    = arrayFuelTypeTemp.sortedArrayUsingDescriptors([
            NSSortDescriptor(key: "", ascending: true)
            ])
        let arrayFuelType           = sortedArray.mutableCopy() as! NSMutableArray
        arrayFuelType.removeObject("")
        
        let dictFuelType            = NSMutableDictionary()
        dictFuelType.setValue("Fuel Type", forKey: "type")
        dictFuelType.setObject(arrayFuelType, forKey: "values")
        
        if arrayFuelType.count > 0{
            arrayAllData.addObject(dictFuelType)
        }
        
        
        
        if fuelType != ""{
            
            if fromatString != ""{
                fromatString .appendString(" AND ")
            }
            
            fromatString.appendString("SELF.fuel_type Contains[cd]")
            fromatString.appendString(" \"\(fuelType)\"")
        }
        if fromatString != ""{
            
            let predicate           = NSPredicate(format: fromatString as String)
            let filteredArray: NSArray      = arrayVehicleDetails.filteredArrayUsingPredicate(predicate)
            print(filteredArray.count)
            
            let sortedArray : NSArray    = filteredArray.sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "fuel_type", ascending: true)
                ])
            
            arrayVehicleDetails = sortedArray.mutableCopy() as! NSMutableArray
        }
        
        
        let arrayVariantTemp        = arrayVehicleDetails.valueForKeyPath("@distinctUnionOfObjects.variant") as! NSArray
        let arrayVariant                = arrayVariantTemp.mutableCopy() as! NSMutableArray
        arrayVariant.removeObject("")
        
        let dictVarient             = NSMutableDictionary()
        dictVarient.setValue("Variant", forKey: "type")
        dictVarient.setObject(arrayVariant, forKey: "values")
        
        if arrayVariant.count > 0{
            arrayAllData.addObject(dictVarient)
        }
        
        
        if variant != ""{
            
            if fromatString != ""{
                fromatString .appendString(" AND ")
            }
            
            fromatString.appendString("SELF.variant Contains[cd]")
            fromatString.appendString(" \"\(variant)\"")
        }
        
        if fromatString != ""{
            
            let predicate           = NSPredicate(format: fromatString as String)
            let filteredArray: NSArray      = arrayVehicleDetails.filteredArrayUsingPredicate(predicate)
            print(filteredArray.count)
            
            arrayVehicleDetails = filteredArray.mutableCopy() as! NSMutableArray
        }
        
        
        let arrayEngineCapacityTemp  = arrayVehicleDetails.valueForKeyPath("@distinctUnionOfObjects.engine_cc") as! NSArray
        let sortedArrayEngineCapacity : NSArray    = arrayEngineCapacityTemp.sortedArrayUsingDescriptors([
            NSSortDescriptor(key: "", ascending: true)
            ])
        let arrayEngineCapacity         = sortedArrayEngineCapacity.mutableCopy() as! NSMutableArray
        arrayEngineCapacity.removeObject(0)

        let dictEngineCC            = NSMutableDictionary()
        dictEngineCC.setValue("Engine Capacity", forKey: "type")
        dictEngineCC.setObject(arrayEngineCapacity, forKey: "values")
        
        if arrayEngineCapacity.count > 0{
            arrayAllData.addObject(dictEngineCC)
        }
        
        
        if roloadData {
            tableViewVehiclesInfo.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func popVC () {
        
        for controller in (self.navigationController?.viewControllers)! {
            if controller.isKindOfClass(ModelTVC){
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
    
    //MARK: - Next & Info Actions -
    
    @IBAction func nextAction ( sender : AnyObject ){
    
        if nextButton.selected {
            
            let arrayVehicleDetails = DBHelper.sharedInstance.fetchVehiclesByYearBrandModelFromDB( AdDetailStorage.sharedInstance.year , brand: AdDetailStorage.sharedInstance.brand , model: AdDetailStorage.sharedInstance.model) as NSMutableArray
            
            let fromatString = NSMutableString()
            if selectedTransmissionValue != ""{
                fromatString.appendString("SELF.transmission Contains[cd]")
                fromatString.appendString(" \"\(selectedTransmissionValue)\"")
            }
            if selectedFuelTypeValue != ""{
                
                if fromatString != ""{
                    fromatString .appendString(" AND ")
                }
                
                fromatString.appendString("SELF.fuel_type Contains[cd]")
                fromatString.appendString(" \"\(selectedFuelTypeValue)\"")
            }
            if selectedVarientValue != ""{
                
                if fromatString != ""{
                    fromatString .appendString(" AND ")
                }
                
                fromatString.appendString("SELF.variant Contains[cd]")
                fromatString.appendString(" \"\(selectedVarientValue)\"")
            }
            
            if selectedEngineCapacityValue != 0{
                
                if fromatString != ""{
                    fromatString .appendString(" AND ")
                }
                
                fromatString.appendString("SELF.engine_cc ==")
                fromatString.appendString(" \(selectedEngineCapacityValue)")
            }
            
            
            let predicate                   = NSPredicate(format: fromatString as String)
            let filteredArray: NSArray      = arrayVehicleDetails.filteredArrayUsingPredicate(predicate)
            print(filteredArray.count)
            
            if filteredArray.count > 0{
                
                let gifAnimationVC                  = self.storyboard?.instantiateViewControllerWithIdentifier("GifAnimationVC") as! GifAnimationVC
                gifAnimationVC.arrayFilteredResults = filteredArray
                self.navigationController?.pushViewController(gifAnimationVC, animated: true)
            }
            
           
            
        }
        
    }
    
    @IBAction func infoButtonTapped ( sender : AnyObject ){
        
    }

    //MARK: - TableView Methods -

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAllData.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 94.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let CellIdentifier: String  = "InfoCell"
        let cell: UITableViewCell   = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
        cell.selectionStyle         = .None
       
        
        let imageView       = cell.viewWithTag(11) as! UIImageView
        let type        = arrayAllData.objectAtIndex(indexPath.row).valueForKey("type") as! String
        switch String(type) {
        case "Transmission":
            imageView.image     = UIImage ( named: "IconTransmission" )
            break
            
        case "Fuel Type":
            imageView.image     = UIImage ( named: "IconFuel" )
            break
            
        case "Variant":
            imageView.image     = UIImage ( named: "IconCar" )
            break
            
        case "Engine Capacity":
            imageView.image     = UIImage ( named: "IconEngine" )
            break
            
            
        default:
            break
            
        }
        
        
        let labelHeader     = cell.viewWithTag(12) as! UILabel
        labelHeader.text    = arrayAllData.objectAtIndex(indexPath.row).valueForKey("type") as? String
        
        
        let collectionView  = cell.viewWithTag(13) as! UICollectionView
        collectionView.delegate        = self
        collectionView.dataSource      = self
        collectionView.layer.setValue(indexPath.row, forKey: "IndexValue")
        
        collectionView.reloadData()
        
        return cell
    }
    
    
    // MARK: - UICollectionViewDataSource protocol -
    
    /* Tell the Collection View How Many Cells to Make */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        let indexValue = collectionView.layer.valueForKey("IndexValue") as! Int
        print("Collection Cell Count = \(arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.count)")
        return (arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.count)!
    }
    
    /* Make a Cell for Each Cell Index Path */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
        let cell        = collectionView.dequeueReusableCellWithReuseIdentifier("CellInfo", forIndexPath: indexPath)
        cell.backgroundColor = UIColor.whiteColor()
        
        let button      = cell.viewWithTag(1) as! UIButton
        let indexValue  = collectionView.layer.valueForKey("IndexValue") as! Int
        let type        = arrayAllData.objectAtIndex(indexValue).valueForKey("type") as! String
        switch String(type) {
        case "Transmission":
            let vehicleInfo = arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.objectAtIndex(indexPath.row) as! String
            button.setTitle(vehicleInfo, forState: .Normal)
            
            selectedTransmissionIndex == indexPath.row ? (button.selected = true) : (button.selected = false)
            
            break
            
        case "Fuel Type":
            let vehicleInfo = arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.objectAtIndex(indexPath.row) as! String
            button.setTitle(vehicleInfo, forState: .Normal)
            
            selectedFuelTypeIndex == indexPath.row ? (button.selected = true) : (button.selected = false)
            
            break
            
        case "Variant":
            let vehicleInfo = arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.objectAtIndex(indexPath.row) as! String
            button.setTitle(vehicleInfo, forState: .Normal)
            
            selectedVarientIndex == indexPath.row ? (button.selected = true) : (button.selected = false)
            
            break
            
        case "Engine Capacity":
            let vehicleInfo = arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.objectAtIndex(indexPath.row) as! NSNumber
            let engineCC                        = vehicleInfo as Double
            let multiplier                      = pow(10.0, 1)
            let num                             = engineCC/1000.0
            let rounded                         = round(num * multiplier) / multiplier
            button.setTitle(String (rounded), forState: .Normal)
            
            selectedEngineCapacityIndex == indexPath.row ? (button.selected = true) : (button.selected = false)
            
            break
        default:
            break
            
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol -
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell        = collectionView.cellForItemAtIndexPath(indexPath)! as UICollectionViewCell
        let button      = cell.viewWithTag(1) as! UIButton
        button.selected = true
        
        let indexValue  = collectionView.layer.valueForKey("IndexValue") as! Int
        let type        = arrayAllData.objectAtIndex(indexValue).valueForKey("type") as! String
        
        switch String(type) {
            
        case "Transmission":
            
            selectedTransmissionIndex       = indexPath.row
            selectedFuelTypeIndex           = -1
            selectedVarientIndex            = -1
            selectedEngineCapacityIndex     = -1
            
            
            selectedTransmissionValue    = arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.objectAtIndex(indexPath.row) as! String
            self.getDataToFilter(selectedTransmissionValue, fuelType: "", variant: "", engineCC: 0, roloadData: true)
           
            selectedFuelTypeValue        = ""
            selectedVarientValue         = ""
            selectedEngineCapacityValue  = 0
            
            break
            
        case "Fuel Type":
            
            selectedFuelTypeIndex           = indexPath.row
            selectedVarientIndex            = -1
            selectedEngineCapacityIndex     = -1
            
            
            selectedFuelTypeValue           = arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.objectAtIndex(indexPath.row) as! String
            self.getDataToFilter(selectedTransmissionValue, fuelType: selectedFuelTypeValue, variant: "", engineCC: 0, roloadData: true)
            
            selectedVarientValue            = ""
            selectedEngineCapacityValue     = 0

            
            break
            
        case "Variant":
            
            selectedVarientIndex            = indexPath.row
            selectedEngineCapacityIndex     = -1
            
            
            selectedVarientValue            = arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.objectAtIndex(indexPath.row) as! String
            self.getDataToFilter(selectedTransmissionValue, fuelType: selectedFuelTypeValue, variant: selectedVarientValue, engineCC: 0, roloadData: true)
            
            selectedEngineCapacityValue     = 0

            break
            
        case "Engine Capacity":
            
            selectedEngineCapacityIndex     = indexPath.row
            selectedEngineCapacityValue     = arrayAllData.objectAtIndex(indexValue).valueForKey("values")?.objectAtIndex(indexPath.row) as! NSNumber
            
            break
            
        default:
            break
            
        }
        
        
        // Activate / Deactivate Next Button //
        
        var totalSelectValuesCount      = 0
        
        if selectedTransmissionValue    != ""{
            totalSelectValuesCount += 1
        }
        if selectedFuelTypeValue        != ""{
            totalSelectValuesCount += 1
        }
        if selectedVarientValue         != ""{
            totalSelectValuesCount += 1
        }
        if selectedEngineCapacityValue  != 0{
          totalSelectValuesCount += 1
        }
        
        if totalSelectValuesCount == arrayAllData.count{
            nextButton.selected = true
        }else{
            nextButton.selected = false
        }
    
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath){
        
        if collectionView.indexPathsForVisibleItems().contains(indexPath){
           
            let cell        = collectionView.cellForItemAtIndexPath(indexPath)! as UICollectionViewCell
            let button      = cell.viewWithTag(1) as! UIButton
            button.selected = false
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
