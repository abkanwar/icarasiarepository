//
//  ResultFoundVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 9/7/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class ResultFoundVC: UIViewController {

    var arrayFilteredResults : NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.title = String(AdDetailStorage.sharedInstance.year) + " \(AdDetailStorage.sharedInstance.brand)" + " \(AdDetailStorage.sharedInstance.model)"
        self.performSelector(#selector (showVehiclesResultVC), withObject: nil, afterDelay: 2.0)

    }
    
    func showVehiclesResultVC () {
        
        //let arrayVehicleDetails = DBHelper.sharedInstance.fetchVehiclesByYearBrandModelFromDB( AdDetailStorage.sharedInstance.year , brand: AdDetailStorage.sharedInstance.brand , model: AdDetailStorage.sharedInstance.model) as NSMutableArray
        
        //if arrayVehicleDetails.count > 5 {
            
        //    let additionalInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("AdditionalInfoVC") as! AdditionalInfoVC
        //    self.navigationController?.pushViewController(additionalInfoVC, animated: false)
        //}
        //else{
            let vehiclesResultVC                    = self.storyboard?.instantiateViewControllerWithIdentifier("VehiclesResultVC") as! VehiclesResultVC
            vehiclesResultVC.arrayFilteredResults   = arrayFilteredResults
            self.navigationController?.pushViewController(vehiclesResultVC, animated: false)
        //}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
