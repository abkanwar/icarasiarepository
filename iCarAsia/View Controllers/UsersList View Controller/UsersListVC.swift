//
//  UsersListVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/8/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class UsersListVC: UIViewController {
    
    @IBOutlet weak var tableViewUsers: UITableView!
    
    var arrayUsers          = NSArray()
    var arrayAllUsers       = NSArray()
    var loading             = Bool()

    //var selectedIndex       = NSInteger()
    
    let searchController    = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        searchController.searchResultsUpdater               = self
        searchController.searchBar.delegate                 = self
        definesPresentationContext                          = true
        searchController.dimsBackgroundDuringPresentation   = false
        
        tableViewUsers.tableHeaderView                      = searchController.searchBar
        
        //selectedIndex       = -1
        tableViewUsers.tableFooterView = UIView()
        self.getUsers()
    }
    
    func getUsers(){
        
        SVProgressHUD.showWithStatus("SA_STR_LOADING_USERS".localized, maskType: SVProgressHUDMaskType.Clear)
        let page: QBGeneralResponsePage = QBGeneralResponsePage()
        page.currentPage    = 1
        page.perPage        = 50
        QBRequest.usersForPage(page, successBlock: { (response, responsePage, users) in
           
            print("Users : \(users)")
            
            SVProgressHUD.dismiss()
            self.arrayUsers     = users!
            self.arrayAllUsers  = users!
            self.tableViewUsers.reloadData()
            
            }, errorBlock: {(response: QBResponse) in
                // Handle error here //
                SVProgressHUD.dismiss()
                print("Error : \(response.description)")
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView Delegates & Data Source //
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayUsers.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 64.0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let simpleTableIdentifier: String = "userCell"
        let cell: UITableViewCell   = tableView.dequeueReusableCellWithIdentifier(simpleTableIdentifier)!
        cell.selectionStyle         = UITableViewCellSelectionStyle.None
        
        let user : QBUUser          = self.arrayUsers.objectAtIndex(indexPath.row) as! QBUUser
        
        let imageViewUser           = cell.viewWithTag(1) as! UIImageView
        //imageViewUser.image         = nil
        
        
        let labelUserName           = cell.viewWithTag(2) as! UILabel
        labelUserName.text          = user.login
        
        if user.ID == ServicesManager.instance().currentUser()?.ID {
            
            labelUserName.enabled                   = false
            imageViewUser.userInteractionEnabled    = false
            cell.userInteractionEnabled             = false
        }

        /*let imageViewRadio          = cell.viewWithTag(3) as! UIImageView
        
        if selectedIndex == indexPath.row{
            imageViewRadio.image        = UIImage(named:"Radio-Fill")!
        }
        else{
            imageViewRadio.image        = UIImage(named:"Radio")!
        }
        */
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        /*
        let cell : UITableViewCell  = tableViewUsers.cellForRowAtIndexPath(indexPath)!
        let imageViewRadio          = cell.viewWithTag(3) as! UIImageView
        imageViewRadio.image        = UIImage(named:"Radio-Fill")!
         */
        
        //selectedIndex               = indexPath.row
        
        
        SVProgressHUD.showWithStatus("SA_STR_LOADING_DIALOG".localized, maskType: SVProgressHUDMaskType.Clear)
        let user : QBUUser          = self.arrayUsers.objectAtIndex(indexPath.row) as! QBUUser
        //let userID: NSNumber!       = user.ID as NSNumber
        //self.createPrivateDialog(userID)
        self.createGroupDialog([user])
    }
    
    /*
     func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath){
        
        
        if rowVisible(indexPath){
            
            let cell : UITableViewCell  = tableViewUsers.cellForRowAtIndexPath(indexPath)!
            let imageViewRadio          = cell.viewWithTag(3) as! UIImageView
            imageViewRadio.image        = UIImage(named:"Radio")!
        }
    }
     */
    
    
    
    
    /*func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tableViewUsers {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !loading{
                    loading = true
                    self.getUsers()
                    }
            }
        }
    }*/
    
    
    func rowVisible(index : NSIndexPath) -> Bool {
        
        let arrayIndexes = tableViewUsers.indexPathsForVisibleRows
        for indexPath in arrayIndexes!{
            
            if indexPath.row ==  index.row{
             return true
            }
        }
        
        return false
    }
    
    func updatedMessageWithUsers(users: [QBUUser],isNewDialog:Bool) -> String {
        
        let dialogMessage = isNewDialog ? "SA_STR_CREATE_NEW".localized : "SA_STR_ADDED".localized
        
        var message: String = "\(QBSession.currentSession().currentUser!.login!) " + dialogMessage + " "
        for user: QBUUser in users {
            message = "\(message)\(user.login!),"
        }
        message = message.substringToIndex(message.endIndex.predecessor())
        return message
    }
    
    func createGroupDialog (users : [QBUUser]) {
        
        let randomNumber                        = arc4random_uniform(10) + 1
        ServicesManager.instance().chatService.createGroupChatDialogWithName("Group Dialog", photo: "\(randomNumber)", occupants: users ) { [weak self] (response: QBResponse, chatDialog: QBChatDialog?) -> Void in
            
            
            guard response.error == nil else {
                
                SVProgressHUD.showErrorWithStatus(response.error?.error?.localizedDescription)
                return
            }
            
            guard let unwrappedDialog = chatDialog else {
                return
            }
            
            guard let dialogOccupants = chatDialog?.occupantIDs else {
                print("Chat dialog has not occupants")
                return
            }
            
            guard let strongSelf = self else { return }
            
            let notificationText = strongSelf.updatedMessageWithUsers(users, isNewDialog: true)
            ServicesManager.instance().chatService.sendSystemMessageAboutAddingToDialog(unwrappedDialog, toUsersIDs: dialogOccupants, withText:notificationText, completion: { (error: NSError?) -> Void in
                
                ServicesManager.instance().chatService.sendNotificationMessageAboutAddingOccupants(dialogOccupants, toDialog: unwrappedDialog, withNotificationText: notificationText)
                //completion?(response: response, createdDialog: unwrappedDialog)
            })
            
            SVProgressHUD.showSuccessWithStatus("STR_DIALOG_CREATED".localized);
            NSNotificationCenter.defaultCenter().postNotificationName("refreshDialogsTable", object: nil)
            self!.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    
    func createPrivateDialog (occupantID : NSNumber){
        
        let chatDialog          = QBChatDialog(dialogID: nil, type: QBChatDialogType.Private)
        //chatDialog.name         = "Chat Dialog"
        chatDialog.occupantIDs  = [occupantID] // , (ServicesManager.instance().currentUser()?.ID)!
        
        QBRequest.createDialog(chatDialog, successBlock: { (response: QBResponse?, createdDialog : QBChatDialog?) in
            
            SVProgressHUD.showSuccessWithStatus("STR_DIALOG_CREATED".localized);
            NSNotificationCenter.defaultCenter().postNotificationName("refreshDialogsTable", object: nil)
            self.navigationController?.popViewControllerAnimated(true)
            
            }) { (responce : QBResponse!) in
                SVProgressHUD.showErrorWithStatus("SA_STR_CANNOT_CREATE_DIALOG".localized);
        }
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UsersListVC: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate -
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    }
}

extension UsersListVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate -
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let text      = searchController.searchBar.text! as String
        
        if text.characters.count > 0{
            searchController.dimsBackgroundDuringPresentation   = false
            let predicate: NSPredicate      = NSPredicate(format: "SELF.login CONTAINS[c] %@",searchBar.text!)
            let filteredArray: NSArray      = self.arrayAllUsers.filteredArrayUsingPredicate(predicate)
            self.arrayUsers = filteredArray
            tableViewUsers.reloadData()
        }else{
            searchController.dimsBackgroundDuringPresentation   = true
            self.arrayUsers = arrayAllUsers
            tableViewUsers.reloadData()
        }
        
        
    }
}
