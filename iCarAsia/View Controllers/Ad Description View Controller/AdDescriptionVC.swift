//
//  AdDescriptionVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/17/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class AdDescriptionVC: UIViewController , UITextViewDelegate {

    @IBOutlet weak var textViewDescription: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title                      = "Ad description"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        
        let croosButton = Utility.sharedInstance.navigationBarCrossButton( "Close" )
        croosButton.addTarget(self, action: #selector(saveDescription), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.rightBarButtonItem   = UIBarButtonItem(customView:croosButton)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        textViewDescription.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveDescription () {
        AdDetailStorage.sharedInstance.descriptionAd = textViewDescription.text
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
