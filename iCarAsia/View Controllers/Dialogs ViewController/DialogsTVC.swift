//
//  DialogsTVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/19/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class DialogsTVC: UITableViewController, QMChatServiceDelegate, QMChatConnectionDelegate, QMAuthServiceDelegate {
    
    
    private var didEnterBackgroundDate: NSDate?
    private var observer: NSObjectProtocol?
    
    var searchString        = String()
    let arrayChatToDelete   = NSMutableArray()
    let searchController  = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Chats"//ServicesManager.instance().currentUser()?.login!
        //self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.allowsMultipleSelectionDuringEditing = true
        
        //SVProgressHUD.showWithStatus("SA_STR_LOADING".localized, maskType: SVProgressHUDMaskType.Clear)
        //self.navigationItem.leftBarButtonItem = self.createLogoutButton()
        ServicesManager.instance().chatService.addDelegate(self)
        ServicesManager.instance().authService.addDelegate(self)
        self.observer = NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationDidBecomeActiveNotification, object: nil, queue: NSOperationQueue.mainQueue()) { (notification: NSNotification) -> Void in
            if !QBChat.instance().isConnected {
                SVProgressHUD.showWithStatus("SA_STR_CONNECTING_TO_CHAT".localized, maskType: SVProgressHUDMaskType.Clear)
            }
        }
                
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DialogsTVC.didEnterBackgroundNotification), name: UIApplicationDidEnterBackgroundNotification, object: nil)
        self.tableView.tableFooterView  = UIView()
        
        if (QBChat.instance().isConnected){
            self.getDialogs()
        }
        
        /* Do any additional setup after loading the view. */
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DialogsTVC.getDialogs), name: "refreshDialogsTable", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DialogsTVC.reloadTableViewIfNeeded), name: "updateDialogsTable", object: nil)
        
        /* Add search Bar Table View */
        searchController.searchResultsUpdater               = self
        searchController.searchBar.delegate                 = self
        searchController.definesPresentationContext         = true
        searchController.dimsBackgroundDuringPresentation   = false
        self.tableView.tableHeaderView                      = searchController.searchBar
        
        //self.extendedLayoutIncludesOpaqueBars               = false
        //self.edgesForExtendedLayout                         = .Bottom
        //self.view.autoresizingMask                          = .FlexibleHeight;
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        self.setUpNavBarAndToolBar()
        if self.dialogs()?.isEmpty == true{
            self.navigationController?.view.addSubview(bumpNowView())
        }else{
            self.navigationItem.rightBarButtonItem?.enabled = true
        }
        
        if #available(iOS 9.0, *) {
            UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).backgroundColor = UIColor.whiteColor()
        } else {
            for view in searchController.searchBar.subviews {
                for subview in view.subviews {
                    if subview .isKindOfClass(UITextField) {
                        let textField: UITextField = subview as! UITextField
                        textField.backgroundColor = UIColor.whiteColor()
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Empty Chat View & Bump Now -
   
    func bumpNowView() -> UIView {
        
        removeBumpNowView()
        
        let buttonArchived : UIButton       = UIButton(frame: CGRectMake(CGRectGetWidth(self.tableView.frame) - 108, 5, 100, 40))
        buttonArchived.backgroundColor      = UIColor.clearColor()
        buttonArchived.setTitle("Archived", forState: .Normal)
        buttonArchived.titleLabel?.font     = UIFont.boldSystemFontOfSize(13)
        buttonArchived.setTitleColor(UIColor.redColor(), forState: .Normal)
        buttonArchived.setImage(UIImage(named: "Archive"), forState: .Normal)
        buttonArchived.titleEdgeInsets      = UIEdgeInsetsMake(0, 8, 0, 0);
        buttonArchived.addTarget(self, action:#selector(self.buttonArchiveClicked), forControlEvents: .TouchUpInside)

        
        let viewEmptyChat               = UIView()
        viewEmptyChat.backgroundColor   = UIColor.whiteColor()
        viewEmptyChat.tag               = 111
        viewEmptyChat.frame             = CGRectMake(0, 64, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - 114)
        
        let imageViewEmptyChat          = UIImageView()
        imageViewEmptyChat.image        = UIImage(named: "ChatEmptyState")
        imageViewEmptyChat.frame        = CGRectMake(CGRectGetWidth(self.view.frame) * 0.172, CGRectGetHeight(self.view.frame) * 0.075, CGRectGetWidth(self.view.frame) * 0.64, CGRectGetHeight(self.view.frame) * 0.31)
        
        
        let labelText                   = UILabel()
        labelText.frame                 = CGRectMake(CGRectGetWidth(self.view.frame) * 0.172, CGRectGetMaxY(imageViewEmptyChat.frame) + 30, CGRectGetWidth(self.view.frame) * 0.64, CGRectGetHeight(self.view.frame) * 0.084)
        labelText.numberOfLines         = 0
        labelText.textAlignment         = NSTextAlignment.Center
        labelText.font                  = UIFont.systemFontOfSize(15)
        labelText.text                  = "Messages are empty \nBump to get buyers interested"
        
        
        let buttonBumpNow               = UIButton(frame: CGRect(x: CGRectGetWidth(self.view.frame) * 0.246, y: CGRectGetMaxY(labelText.frame)+55, width: CGRectGetWidth(self.view.frame) *  0.5, height: CGRectGetHeight(self.view.frame) * 0.074))
        buttonBumpNow.backgroundColor   = .clearColor()
        buttonBumpNow.titleLabel?.font  = UIFont.systemFontOfSize(14)
        //buttonBumpNow.setTitle("Bump Now", forState: .Normal)
        buttonBumpNow.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        buttonBumpNow.setImage(UIImage(named: "EmptyState"), forState: .Normal)
        buttonBumpNow.addTarget(self, action: #selector(bumpNowAction), forControlEvents: .TouchUpInside)
        
        
        if Utility.sharedInstance.fetchArchivedChatIDs().count > 0{
            viewEmptyChat.addSubview(buttonArchived)
        }
        
        
        viewEmptyChat.addSubview(imageViewEmptyChat)
        viewEmptyChat.addSubview(labelText)
        viewEmptyChat.addSubview(buttonBumpNow)
        
        return viewEmptyChat
    }
    
    func removeBumpNowView (){
        
        let bumpNowView = self.navigationController?.view.viewWithTag(111)
        if bumpNowView != nil{
            bumpNowView?.removeFromSuperview()
        }
    }
    
    func changeTabBar(hidden:Bool, animated: Bool){
        
        let tabBar                  =  self.tabBarController?.tabBar
        if tabBar!.hidden           == hidden{ return }
        let frame                   =  tabBar?.frame
        let offset                  =  (hidden ? (frame?.size.height)! : -(frame?.size.height)!)
        let duration:NSTimeInterval =  (animated ? 0.5 : 0.0)
        tabBar?.hidden              = false
        
        if frame != nil
        {
            UIView.animateWithDuration(duration, animations: {
                tabBar!.frame = CGRectOffset(frame!, 0, offset)
                },
                                       completion: {
                                        print($0)
                                        if $0 {tabBar?.hidden = hidden
                                        }
            })
        }
    }
    
    func bumpNowAction() {
        
        removeBumpNowView()
        let usersListVC = self.storyboard!.instantiateViewControllerWithIdentifier("UsersListVC") as! UsersListVC
        self.navigationController!.pushViewController(usersListVC, animated: true)
    }
    
    // MARK: - Nav & Tool Bar Setup ( Edit , Archicve , Chat ,Select All ) -
    
    func setUpNavBarAndToolBar (){
        
        if self.tableView.editing {
            self.tableView.editing  = false
        }
        
        self.navigationController!.setToolbarHidden(true, animated: true)
        let buttonEdit = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action: #selector(self.editAction))
        self.navigationItem.rightBarButtonItem          = buttonEdit
        self.navigationItem.rightBarButtonItem?.enabled = false
        
        
        let buttonSelectAll     = UIBarButtonItem(title: "Select All", style: .Plain, target: self, action: #selector(self.selectAllDialogs))
        let buttonArchive       = UIBarButtonItem(title: "Archive", style: .Plain, target: self, action: #selector(self.archiveChat))
        buttonArchive.enabled    = false
        let buttonDelete        = UIBarButtonItem(title: "Delete", style: .Plain, target: self, action: #selector(self.deleteAction))
        buttonDelete.enabled    = false
        let flexibleSpace       = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        self.toolbarItems       = [buttonSelectAll , flexibleSpace , buttonArchive , flexibleSpace , buttonDelete]
    }
    
    func selectAllDialogs () {
        
        
        if self.toolbarItems![0].title == "Select All"{
            
            self.toolbarItems![0].title         = "Deselect All"
            self.toolbarItems![2].enabled       = true
            self.toolbarItems![4].enabled       = true
            
            arrayChatToDelete.removeAllObjects()
            
            for i in 0..<self.dialogs()!.count {
                self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0), animated: false, scrollPosition: .Top)
                arrayChatToDelete.addObject(NSIndexPath(forRow: i, inSection: 0))
            }
            
        }else{
            
            self.toolbarItems![0].title         = "Select All"
            self.toolbarItems![2].enabled       = false
            self.toolbarItems![4].enabled       = false
            
            arrayChatToDelete.removeAllObjects()
            for i in 0..<self.dialogs()!.count {
                self.tableView.deselectRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0), animated: false)
            }
            
        }
    }
    
    func archiveChat (){
        
        self.tableView.editing                          = false
        self.navigationItem.rightBarButtonItem?.title   = "Edit"
        self.toolbarItems![0].title                     = "Select All"
        self.toolbarItems![2].enabled                   = false
        self.toolbarItems![4].enabled                   = false
        self.tableView.tableHeaderView                  = searchController.searchBar
        
        self.navigationController!.setToolbarHidden(true, animated: true)


        
        let arrayArchiveChatIds = Utility.sharedInstance.fetchArchivedChatIDs()
        let arrayDialogs       = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAtWithAscending(false)
        for i in 0 ..< self.arrayChatToDelete.count{
            
            let indexPath = self.arrayChatToDelete[i] as! NSIndexPath
            let dialog    = arrayDialogs[indexPath.row]
            if !arrayArchiveChatIds.containsObject(dialog.ID!){
                arrayArchiveChatIds.addObject(dialog.ID!)
            }
        }
        Utility.sharedInstance.saveArchivedChatIDs(arrayArchiveChatIds)
        self.reloadTableViewIfNeeded()
    }
    
    func editAction  (){
        
        if self.tableView.editing{
            
            arrayChatToDelete.removeAllObjects()
            self.tableView.editing                          = false
            //self.changeTabBar(false, animated: true)
            self.navigationController?.toolbar.tintColor    = UIColor(red: 76/255.0, green: 118/255.0, blue: 234/255.0, alpha: 1.0)
            self.navigationController?.toolbar.barTintColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
            
            
            self.navigationController!.setToolbarHidden(true, animated: true)
            self.navigationItem.rightBarButtonItem?.title   =  "Edit"
            self.toolbarItems![0].title                     = "Select All"
            self.toolbarItems![2].enabled                   = false
            self.toolbarItems![4].enabled                   = false
            self.tableView.tableHeaderView                  = searchController.searchBar
            
        }
        else{
            arrayChatToDelete.removeAllObjects()
            self.tableView.editing          = true
            //self.changeTabBar(true, animated: true)
            self.navigationController?.toolbar.tintColor    = UIColor(red: 76/255.0, green: 118/255.0, blue: 234/255.0, alpha: 1.0)
            self.navigationController?.toolbar.barTintColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
            self.navigationController!.setToolbarHidden(false, animated: true)
            self.navigationItem.rightBarButtonItem?.title   =  "Done"
            self.tableView.tableHeaderView                  = UIView()
            
        }
    }
    
    func buttonArchiveClicked() {
       
        
        print("Archive Button Action")
        
        arrayChatToDelete.removeAllObjects()
        self.tableView.editing                          = false
        //self.changeTabBar(false, animated: true)
        self.navigationController?.toolbar.tintColor    = UIColor(red: 76/255.0, green: 118/255.0, blue: 234/255.0, alpha: 1.0)
        self.navigationController?.toolbar.barTintColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
        self.navigationController!.setToolbarHidden(true, animated: true)
        
        self.navigationItem.rightBarButtonItem?.title   =  "Edit"
        self.toolbarItems![0].title                     = "Select All"
        self.toolbarItems![2].enabled                   = false
        self.toolbarItems![4].enabled                   = false
        if self.searchController.active{
            self.searchController.active                = false
        }else{
            self.tableView.tableHeaderView              = searchController.searchBar
        }
        
        self.removeBumpNowView ()
        
        let archiveTVC                  = self.storyboard!.instantiateViewControllerWithIdentifier("ArchiveTVC") as! ArchiveTVC

        let arrayArchiveChatIds = Utility.sharedInstance.fetchArchivedChatIDs()
        if arrayArchiveChatIds.count > 0 {
            
            let arrayAllDialogs        = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAtWithAscending(false)
            let arrayFilteredChat   = NSMutableArray()
            for i in 0..<arrayAllDialogs.count{
                
                let chatDialog = arrayAllDialogs[i]
                if  arrayArchiveChatIds.containsObject(chatDialog.ID!){
                    arrayFilteredChat.addObject(chatDialog)
                }
            }
            let arrayDialogs : NSArray  = arrayFilteredChat
             archiveTVC.arrayDialogs    = arrayDialogs.mutableCopy() as! NSMutableArray
        }else{
            archiveTVC.arrayDialogs  = NSMutableArray()
        }
        self.navigationController?.pushViewController(archiveTVC, animated: true)
    }
    
   func deleteAction  (){
    
    
    _ = AlertView(title:"DELETE_CHAT".localized , message:"SA_STR_DO_YOU_REALLY_WANT_TO_DELETE_SELECTED_DIALOG".localized , cancelButtonTitle: "SA_STR_CANCEL".localized, otherButtonTitle: ["SA_STR_DELETE".localized], didClick:{ (buttonIndex) -> Void in
        
        guard buttonIndex == 1 else {
            return
        }
        
        self.tableView.editing                          = false
        self.navigationItem.rightBarButtonItem?.title   = "Edit"
        self.toolbarItems![0].title                     = "Select All"
        self.toolbarItems![2].enabled                   = false
        self.toolbarItems![4].enabled                   = false
        self.navigationController!.setToolbarHidden(true, animated: true)

        SVProgressHUD.showWithStatus("SA_STR_DELETING".localized, maskType: SVProgressHUDMaskType.Clear)
        var deletedDialogsCount = 0
        
        for i in 0 ..< self.arrayChatToDelete.count{
            
            let indexPath = self.arrayChatToDelete[i] as! NSIndexPath
            guard let dialog = self.dialogs()?[indexPath.row] else {
                return
            }
            
            let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                
                // Deletes dialog from server and cache. //
                ServicesManager.instance().chatService.deleteDialogWithID(dialog.ID!, completion: { (response: QBResponse!) -> Void in
                    
                    guard response.success else {
                        deletedDialogsCount += 1
                        SVProgressHUD.showErrorWithStatus("SA_STR_ERROR_DELETING".localized)
                        print(response.error?.error)
                        return
                    }
                    deletedDialogsCount    += 1
                    if deletedDialogsCount  == self.arrayChatToDelete.count {
                        self.arrayChatToDelete.removeAllObjects()
                        SVProgressHUD.showSuccessWithStatus("SA_STR_DELETED".localized)
                    }
                })
            }
            
            if dialog.type == QBChatDialogType.Private {
                deleteDialogBlock(dialog)
            } else {
                // Group //
                let occupantIDs = dialog.occupantIDs!.filter( {$0 != ServicesManager.instance().currentUser()?.ID} )
                dialog.occupantIDs = occupantIDs
                let userLogin = ServicesManager.instance().currentUser()?.login ?? ""
                let notificationMessage = "User \(userLogin) " + "SA_STR_USER_HAS_LEFT".localized
                // Notifies occupants that user left the dialog. //
                ServicesManager.instance().chatService.sendNotificationMessageAboutLeavingDialog(dialog, withNotificationText: notificationMessage, completion: { (error : NSError?) -> Void in
                    deleteDialogBlock(dialog)
                })
            }
        }
    })
    }
    
    
    func didEnterBackgroundNotification() {
        self.didEnterBackgroundDate = NSDate()
    }
    
    func getDialogs() {
        
        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
            
            ServicesManager.instance().chatService.fetchDialogsUpdatedFromDate(lastActivityDate, andPageLimit: kDialogsPageLimit, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDs: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                
                }, completionBlock: { (response: QBResponse?) -> Void in
                    guard let unwrappedResponse = response else {
                        print("fetchDialogsUpdatedFromDate error")
                        return
                    }
                    guard unwrappedResponse.success else {
                        print("fetchDialogsUpdatedFromDate error \(response)")
                        return
                    }
                    ServicesManager.instance().lastActivityDate = NSDate()
            })
        }
        else {
            SVProgressHUD.showWithStatus("SA_STR_LOADING_DIALOGS".localized, maskType: SVProgressHUDMaskType.Clear)
            
            ServicesManager.instance().chatService.allDialogsWithPageLimit(kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                
                }, completion: { (response: QBResponse?) -> Void in
                    
                    guard response != nil && response!.success else {
                        SVProgressHUD.showErrorWithStatus("SA_STR_FAILED_LOAD_DIALOGS".localized)
                        return
                    }
                    SVProgressHUD.showSuccessWithStatus("SA_STR_COMPLETED".localized)
                    ServicesManager.instance().lastActivityDate = NSDate()
            })
        }
    }
    
    
    func dialogs() -> [QBChatDialog]? {
        // Returns dialogs sorted by updatedAt date. //
        
        
        if searchString.isEmpty {
            
            let arrayArchiveChatIds = Utility.sharedInstance.fetchArchivedChatIDs()
            
            if arrayArchiveChatIds.count > 0 {
                
                let arrayAllDialogs        = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAtWithAscending(false)
                let arrayFilteredChat   = NSMutableArray()
                for i in 0..<arrayAllDialogs.count{
                    
                    let chatDialog = arrayAllDialogs[i]
                    if !arrayArchiveChatIds.containsObject(chatDialog.ID!){
                        arrayFilteredChat.addObject(chatDialog)
                    }
                }
                let arrayDialogs : NSArray = arrayFilteredChat
                return  arrayDialogs as? [QBChatDialog]
            }
            else{
                return ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAtWithAscending(false)
            }
       
        }else{
            
            
            let arrayArchiveChatIds = Utility.sharedInstance.fetchArchivedChatIDs()
            
            if arrayArchiveChatIds.count > 0 {
                
                let arrayAllDialogs        = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAtWithAscending(false)
                let arrayFilteredChat   = NSMutableArray()
                for i in 0..<arrayAllDialogs.count{
                    
                    let chatDialog = arrayAllDialogs[i]
                    if !arrayArchiveChatIds.containsObject(chatDialog.ID!){
                        arrayFilteredChat.addObject(chatDialog)
                    }
                }
                let arrayDialogs : NSArray = arrayFilteredChat
                let predicate: NSPredicate      = NSPredicate(format: "SELF.name CONTAINS[c] %@",searchString)
                let filteredArray: NSArray      = arrayDialogs.filteredArrayUsingPredicate(predicate)
                return filteredArray as? [QBChatDialog]
            }
            else{
                let arrayDialogs = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAtWithAscending(false) as NSArray
                let predicate: NSPredicate      = NSPredicate(format: "SELF.name CONTAINS[c] %@",searchString)
                let filteredArray: NSArray      = arrayDialogs.filteredArrayUsingPredicate(predicate)
                return filteredArray as? [QBChatDialog]
            }
        }
        
    }
    
    
    // MARK: - Table view data source & delegates -
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if self.dialogs()!.count > 0 {
            self.tableView.backgroundView = nil
            return 1
            
        } else {
            
            if searchController.active{
                
                let emptyDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height))
                emptyDataLabel.text             = "No results"
                emptyDataLabel.font             = UIFont.boldSystemFontOfSize(20)
                emptyDataLabel.textColor        = UIColor.lightGrayColor()
                emptyDataLabel.textAlignment    = NSTextAlignment.Center
                self.tableView.backgroundView   = emptyDataLabel
            }
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dialogs = self.dialogs() {
            return dialogs.count
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90.0
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 50.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let buttonArchived : UIButton       = UIButton(frame: CGRectMake(CGRectGetWidth(self.tableView.frame) - 108, 5, 100, 40))
        buttonArchived.backgroundColor      = UIColor.clearColor()
        buttonArchived.setTitle("Archived", forState: .Normal)
        buttonArchived.titleLabel?.font     = UIFont.boldSystemFontOfSize(13)
        buttonArchived.setTitleColor(UIColor.redColor(), forState: .Normal)
        buttonArchived.setImage(UIImage(named: "Archive"), forState: .Normal)
        buttonArchived.titleEdgeInsets      = UIEdgeInsetsMake(0, 8, 0, 0);
        buttonArchived.addTarget(self, action:#selector(self.buttonArchiveClicked), forControlEvents: .TouchUpInside)
        
        let borderImageView                 = UIImageView()
        borderImageView.frame               = CGRectMake(0, 48.5, CGRectGetWidth(tableView.frame), 1.5)
        borderImageView.alpha               = 0.6
        borderImageView.backgroundColor     = UIColor.lightGrayColor()
        
        let viewHeader                      = UIView()
        viewHeader.frame                    = CGRectMake(0, 0, CGRectGetWidth(self.view.frame
            ), 50)
        viewHeader.backgroundColor          = UIColor.whiteColor()
        
        viewHeader.addSubview(buttonArchived)
        viewHeader.addSubview(borderImageView)
        
        return viewHeader
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("dialogcell", forIndexPath: indexPath) as! DialogTableViewCell
        //cell.selectionStyle = UITableViewCellSelectionStyle.Blue
        cell.tintColor = UIColor.redColor()
        
        if (self.dialogs()?.count < indexPath.row) {
            return cell
        }
        
        guard let chatDialog = self.dialogs()?[indexPath.row] else {
            return cell
        }
        
        cell.exclusiveTouch                     = true
        cell.contentView.exclusiveTouch         = true
        
        cell.tag                                = indexPath.row
        cell.dialogID                           = chatDialog.ID!
        
        let cellModel                           = DialogTableViewCellModel(dialog: chatDialog)
        cell.dialogLastMessage?.text            = chatDialog.lastMessageText
        cell.dialogName?.text                   = cellModel.textLabelText
        if cellModel.textLabelText.characters.count > 1{
            cell.dialogNameInitial?.text            = cellModel.textLabelText.substringToIndex(cellModel.textLabelText.startIndex.advancedBy(2)).uppercaseString
        }
        
        //let randomNumber                        = arc4random_uniform(10) + 1
        //cell.dialogTypeImage.image              = UIImage(named: "ImgChatProfileBG-" + "\(randomNumber)")
        
        //cell.dialogTypeImage.image              = UIImage(named: "ImgChatProfileBG-" + chatDialog.photo!)
        
        cell.dialogTypeImage.image              = cellModel.dialogIcon
        cell.unreadMessageCounterLabel.text     = cellModel.unreadMessagesCounterLabelText
        cell.unreadMessageCounterHolder.hidden  = cellModel.unreadMessagesCounterHiden
        
        
        if chatDialog.lastMessageDate != nil{
            cell.timeLabel.text                     = Utility.sharedInstance.timeStringByDate(chatDialog.lastMessageDate!)
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        if tableView.editing {
            arrayChatToDelete.addObject(indexPath)
            if arrayChatToDelete.count > 0 {
                self.toolbarItems![2].enabled                   = true
                self.toolbarItems![4].enabled                   = true
            }else{
                self.toolbarItems![2].enabled                   = false
                self.toolbarItems![4].enabled                   = false
            }
            
            if arrayChatToDelete.count == self.dialogs()?.count{
                self.toolbarItems![0].title = "Deselect All"
            }
        }
        else{
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            if (ServicesManager.instance().isProcessingLogOut!) {
                return
            }
            
            let dialog : QBChatDialog  = (self.dialogs()?[indexPath.row])!
            
            //guard dialog == self.dialogs()?[indexPath.row] else {
            //    return
            //}
            //self.performSegueWithIdentifier("SA_STR_SEGUE_GO_TO_CHAT".localized , sender: dialog)
            
            let chatViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
            chatViewController.dialog                   = dialog
            chatViewController.hidesBottomBarWhenPushed = true
            self.navigationController!.pushViewController(chatViewController, animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath?{
        
        if tableView.editing {
            arrayChatToDelete.removeObject(indexPath)
            self.toolbarItems![0].title = "Select All"
            if arrayChatToDelete.count > 0 {
                self.toolbarItems![2].enabled                   = true
                self.toolbarItems![4].enabled                   = true
            }else{
                self.toolbarItems![2].enabled                   = false
                self.toolbarItems![4].enabled                   = false
            }
        }
        return indexPath
    }
    
    /*override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let archiveAction               = UITableViewRowAction(style: .Normal, title: "         ") { (rowAction:UITableViewRowAction, indexPath:NSIndexPath) -> Void in
        }
        archiveAction.backgroundColor   = UIColor(patternImage: UIImage(named: "ArchiveButton")!)
        return [archiveAction]
    }
    
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        guard editingStyle == UITableViewCellEditingStyle.Delete else {
            return
        }
        
        guard let dialog = self.dialogs()?[indexPath.row] else {
            return
        }
        
        _ = AlertView(title:"SA_STR_WARNING".localized , message:"SA_STR_DO_YOU_REALLY_WANT_TO_DELETE_SELECTED_DIALOG".localized , cancelButtonTitle: "SA_STR_CANCEL".localized, otherButtonTitle: ["SA_STR_DELETE".localized], didClick:{ (buttonIndex) -> Void in
            
            guard buttonIndex == 1 else {
                return
            }
            
            SVProgressHUD.showWithStatus("SA_STR_DELETING".localized, maskType: SVProgressHUDMaskType.Clear)
            
            let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                
                // Deletes dialog from server and cache. //
                ServicesManager.instance().chatService.deleteDialogWithID(dialog.ID!, completion: { (response: QBResponse!) -> Void in
                    
                    guard response.success else {
                        SVProgressHUD.showErrorWithStatus("SA_STR_ERROR_DELETING".localized)
                        print(response.error?.error)
                        return
                    }
                    SVProgressHUD.showSuccessWithStatus("SA_STR_DELETED".localized)
                })
            }
            
            if dialog.type == QBChatDialogType.Private {
                deleteDialogBlock(dialog)
            } else {
                
                // Group //
                let occupantIDs = dialog.occupantIDs!.filter( {$0 != ServicesManager.instance().currentUser()?.ID} )
                dialog.occupantIDs = occupantIDs
                let userLogin = ServicesManager.instance().currentUser()?.login ?? ""
                let notificationMessage = "User \(userLogin) " + "SA_STR_USER_HAS_LEFT".localized
                // Notifies occupants that user left the dialog. //
                ServicesManager.instance().chatService.sendNotificationMessageAboutLeavingDialog(dialog, withNotificationText: notificationMessage, completion: { (error : NSError?) -> Void in
                    deleteDialogBlock(dialog)
                })
            }
        })
        
    }
    */
    
    override func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        return "SA_STR_DELETE".localized
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        if editing == true {
            // Your code for entering edit mode goes here
        }
        else {
            // Your code for exiting edit mode goes here
        }
    }
    
    // MARK: - QMChatServiceDelegate -
    
    func chatService(chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(chatService: QMChatService,didUpdateChatDialogsInMemoryStorage dialogs: [QBChatDialog]){
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String){
        self.reloadTableViewIfNeeded()
    }
    
    // MARK: - QMChatConnectionDelegate -
    func chatServiceChatDidFailWithStreamError(error: NSError) {
        SVProgressHUD.showErrorWithStatus(error.localizedDescription)
    }
    
    func chatServiceChatDidAccidentallyDisconnect(chatService: QMChatService) {
        SVProgressHUD.showErrorWithStatus("SA_STR_DISCONNECTED".localized)
    }
    
    func chatServiceChatDidConnect(chatService: QMChatService) {
        SVProgressHUD.showSuccessWithStatus("SA_STR_CONNECTED".localized, maskType:.Clear)
        if !ServicesManager.instance().isProcessingLogOut! {
            self.getDialogs()
        }
    }
    
    func chatService(chatService: QMChatService,chatDidNotConnectWithError error: NSError){
        SVProgressHUD.showErrorWithStatus(error.localizedDescription)
    }
    
    
    func chatServiceChatDidReconnect(chatService: QMChatService) {
        SVProgressHUD.showSuccessWithStatus("SA_STR_CONNECTED".localized, maskType: .Clear)
        if !ServicesManager.instance().isProcessingLogOut! {
            self.getDialogs()
        }
    }
    
    // MARK: - Helpers -
    func reloadTableViewIfNeeded() {
        
        //SVProgressHUD.dismiss()
        if !ServicesManager.instance().isProcessingLogOut!{
            self.tableView.reloadData()
            
            if let dialogs = self.dialogs() {
                if dialogs.count > 0{
                    self.removeBumpNowView()
                    self.navigationItem.rightBarButtonItem?.enabled     = true
                    self.navigationItem.rightBarButtonItem?.title       =  "Edit"

                }else{
                    
                    if !searchController.active {
                        self.navigationItem.rightBarButtonItem?.enabled  = false
                        self.navigationItem.rightBarButtonItem?.title    =  "Edit"
                        
                        if self.navigationController!.topViewController!.isKindOfClass(DialogsTVC) {
                            self.navigationController?.view.addSubview(bumpNowView())
                        }
                        
                    }
                }
                
            }else{
                if !searchController.active {
                    self.navigationItem.rightBarButtonItem?.enabled     = false
                    self.navigationItem.rightBarButtonItem?.title       =  "Edit"
                    
                    if self.navigationController!.topViewController!.isKindOfClass(DialogsTVC) {
                        self.navigationController?.view.addSubview(bumpNowView())
                    }
                }
            }
            
        }
    }
    
    /*
     // MARK: - Navigation -
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension DialogsTVC: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate -
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    }
}

extension DialogsTVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate -
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let text      = searchController.searchBar.text! as String
        
        if text.characters.count > 0{
            
            searchString    = searchBar.text!
            self.reloadTableViewIfNeeded()
            
            //let predicate: NSPredicate      = NSPredicate(format: "SELF CONTAINS[c] %@",searchBar.text!)
            //let filteredArray: NSArray      = self.arrayAllKotas.filteredArrayUsingPredicate(predicate)
            //self.arrayKotas                 = filteredArray.mutableCopy() as! NSMutableArray
            //self.tableViewKota.reloadData()
        }else{
            
            searchString    = ""
            self.reloadTableViewIfNeeded()
            
            //self.arrayKotas                 = self.arrayAllKotas.mutableCopy() as! NSMutableArray
            //self.tableViewKota.reloadData()
        }
    }
}


// MARK: - DialogTableViewCellModel -

class DialogTableViewCellModel: NSObject {
    
    var detailTextLabelText: String     = ""
    var textLabelText: String           = ""
    var unreadMessagesCounterLabelText : String?
    var unreadMessagesCounterHiden      = true
    var dialogIcon : UIImage?
    
    init(dialog: QBChatDialog){
        
        super.init()
        
        switch (dialog.type){
        case .PublicGroup:
            self.detailTextLabelText = "SA_STR_PUBLIC_GROUP".localized
        case .Group:
            self.detailTextLabelText = "SA_STR_GROUP".localized
        case .Private:
            self.detailTextLabelText = "SA_STR_PRIVATE".localized
            
            if dialog.recipientID == -1 {
                return
            }
            /* Getting recipient from users service */
            if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.userWithID(UInt(dialog.recipientID)) {
                self.textLabelText = recipient.login ?? recipient.email!
            }
        }
        
        if self.textLabelText.isEmpty {
            /* Group chat */
            if let dialogName = dialog.name {
                self.textLabelText = dialogName
            }
        }
        // Unread messages counter label //
        if (dialog.unreadMessagesCount > 0) {
            
            var trimmedUnreadMessageCount : String
            if dialog.unreadMessagesCount > 99 {
                trimmedUnreadMessageCount = "99+"
            } else {
                trimmedUnreadMessageCount = String(format: "%d", dialog.unreadMessagesCount)
            }
            self.unreadMessagesCounterLabelText = trimmedUnreadMessageCount
            self.unreadMessagesCounterHiden     = false
        } else {
            self.unreadMessagesCounterLabelText = nil
            self.unreadMessagesCounterHiden     = true
        }
        
        /* Dialog icon */
        if dialog.type == .Private {
           
            let randomNumber                    = arc4random_uniform(10) + 1
            self.dialogIcon                     = UIImage(named: "ImgChatProfileBG-" + "\(randomNumber)")
            
            //self.dialogIcon                     = UIImage(named: "ImgChatProfileBG-" + dialog.photo!)

        } else {
            let randomNumber                    = arc4random_uniform(10) + 1
            self.dialogIcon                     = UIImage(named: "ImgChatProfileBG-" + "\(randomNumber)")
            
            //self.dialogIcon                     = UIImage(named: "ImgChatProfileBG-" + dialog.photo!)
        }
    }
}


