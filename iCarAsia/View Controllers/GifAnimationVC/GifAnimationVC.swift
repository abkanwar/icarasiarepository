//
//  GifAnimationVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 9/7/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit


class GifAnimationVC: UIViewController {
    
    var arrayFilteredResults : NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.title = String(AdDetailStorage.sharedInstance.year) + " \(AdDetailStorage.sharedInstance.brand)" + " \(AdDetailStorage.sharedInstance.model)"
        
        let jeremyGif    = UIImage.gifWithName("Create_ad_loading")
        let imageView    = UIImageView(image: jeremyGif)
        imageView.frame  = CGRect(x: 0, y: 0, width: 205, height: 98)
        imageView.center = self.view.center
        imageView.tag    = 222
        self.view.addSubview(imageView)
        
        self.performSelector(#selector (showResultFoundVCOrAdditionalInfoScreen), withObject: nil, afterDelay: 5.0)
    }
    
    func showResultFoundVCOrAdditionalInfoScreen () {
        
        let imageView    = self.view.viewWithTag(222) as! UIImageView
        imageView.removeFromSuperview()
        
        
        if arrayFilteredResults.count > 0 {
            let resultFoundVC = self.storyboard?.instantiateViewControllerWithIdentifier("ResultFoundVC") as! ResultFoundVC
            resultFoundVC.arrayFilteredResults = arrayFilteredResults
            self.navigationController?.pushViewController(resultFoundVC, animated: false)
        }
        else{
            
            let arrayVehicleDetails = DBHelper.sharedInstance.fetchVehiclesByYearBrandModelFromDB( AdDetailStorage.sharedInstance.year , brand: AdDetailStorage.sharedInstance.brand , model: AdDetailStorage.sharedInstance.model) as NSMutableArray
            
            if arrayVehicleDetails.count > 5 {
                let additionalInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("AdditionalInfoVC") as! AdditionalInfoVC
                self.navigationController?.pushViewController(additionalInfoVC, animated: false)
            }
            else{
                let resultFoundVC = self.storyboard?.instantiateViewControllerWithIdentifier("ResultFoundVC") as! ResultFoundVC
                resultFoundVC.arrayFilteredResults = NSArray()
                self.navigationController?.pushViewController(resultFoundVC, animated: false)
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
