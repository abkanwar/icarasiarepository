//
//  AreaTVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/19/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class AreaTVC: UITableViewController {

    var arrayAreas         = NSMutableArray()
    var arrayAllAreas      = NSMutableArray()
    
    var locationName       = String()
    
    let searchController   = UISearchController(searchResultsController: nil)

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.title                      = "Area"
        self.tableView.tableFooterView  = UIView()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        
        arrayAreas          = DBHelper.sharedInstance.fetchAreaFromDB( locationName )
        arrayAllAreas       = arrayAreas
        
        searchController.searchResultsUpdater                   = self
        searchController.searchBar.delegate                     = self
        searchController.dimsBackgroundDuringPresentation       = false
        searchController.hidesNavigationBarDuringPresentation   = false

        
        //self.tableView.tableHeaderView                        = searchController.searchBar
        self.chnageTabBarAppearence()
        self.navigationItem.titleView                           = searchController.searchBar

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillDisappear(animated)
        definesPresentationContext                              = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        definesPresentationContext                              = false
    }
    
    func chnageTabBarAppearence (){
        
        if #available(iOS 9.0, *) {
            UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).backgroundColor = UIColor(red: 153/255.0 , green: 18/255.0 , blue: 19/255.0 , alpha: 1.0)
        } else {
            for view in searchController.searchBar.subviews {
                for subview in view.subviews {
                    if subview .isKindOfClass(UITextField) {
                        let textField: UITextField = subview as! UITextField
                        textField.backgroundColor = UIColor(red: 153/255.0 , green: 18/255.0 , blue: 19/255.0 , alpha: 1.0)
                    }
                }
            }
        }
        let textFieldInsideSearchBar                = searchController.searchBar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor         = UIColor.whiteColor()
        
        let textFieldInsideSearchBarLabel           = textFieldInsideSearchBar!.valueForKey("placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor    = UIColor(red: 240/255.0 , green: 240/255.0 , blue: 240/255.0 , alpha: 0.6)
        
        searchController.searchBar.setImage(UIImage(named: "IconSearch")!, forSearchBarIcon: .Search, state: .Normal)
        searchController.searchBar.placeholder      = "Type to find"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - TableView Delegates & Data Sources -
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if self.arrayAreas.count > 0 {
            self.tableView.backgroundView = nil
            return 1
            
        } else {
            
            if searchController.active{
                
                let emptyDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height))
                emptyDataLabel.text             = "No results"
                emptyDataLabel.font             = UIFont.boldSystemFontOfSize(20)
                emptyDataLabel.textColor        = UIColor.lightGrayColor()
                emptyDataLabel.textAlignment    = NSTextAlignment.Center
                self.tableView.backgroundView   = emptyDataLabel
            }
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAreas.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let CellIdentifier: String  = "AreaCell"
        let cell: UITableViewCell   = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
        cell.textLabel?.textColor   = UIColor.darkGrayColor()
        cell.textLabel?.text        = arrayAreas.objectAtIndex(indexPath.row) as? String
        cell.textLabel?.font        = UIFont.systemFontOfSize(14)
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 40.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let labelHeaderTitle        = UILabel()
        labelHeaderTitle.text       = "Select location:"
        labelHeaderTitle.font       = UIFont.systemFontOfSize(13)
        labelHeaderTitle.backgroundColor = UIColor.whiteColor()
        labelHeaderTitle.textColor  = UIColor.darkGrayColor()
        labelHeaderTitle.frame      = CGRectMake(15, 9, CGRectGetWidth(self.view.frame
            ) - 30, 22)
        
        let viewHeader              = UIView()
        viewHeader.frame            = CGRectMake(0, 0, CGRectGetWidth(self.view.frame
            ), 40)
        viewHeader.backgroundColor  = UIColor.whiteColor()
        
        viewHeader.addSubview(labelHeaderTitle)
        
        return viewHeader
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        let kotaTVC                         = self.storyboard!.instantiateViewControllerWithIdentifier("KotaTVC") as! KotaTVC
        kotaTVC.areaName                    = String (arrayAreas.objectAtIndex(indexPath.row) as! String)
        AdDetailStorage.sharedInstance.area = String (arrayAreas.objectAtIndex(indexPath.row) as! String)
        self.navigationController?.pushViewController(kotaTVC, animated: true)
    }
    deinit {
        searchController.view.removeFromSuperview()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension AreaTVC: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate -
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    }
}

extension AreaTVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate -
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let text      = searchController.searchBar.text! as String
        
        if text.characters.count > 0{
            //searchController.dimsBackgroundDuringPresentation   = false
            let predicate: NSPredicate      = NSPredicate(format: "SELF CONTAINS[c] %@",searchBar.text!)
            let filteredArray: NSArray      = self.arrayAllAreas.filteredArrayUsingPredicate(predicate)
            self.arrayAreas                 = filteredArray.mutableCopy() as! NSMutableArray
            self.tableView.reloadData()
        }else{
            //searchController.dimsBackgroundDuringPresentation   = true
            self.arrayAreas                 = self.arrayAllAreas.mutableCopy() as! NSMutableArray
            self.tableView.reloadData()
        }
    }
}

