//
//  EditImageVC.swift
//  CameraDemo
//
//  Created by Raman Kant on 8/30/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class EditImageVC: UIViewController {

    @IBOutlet weak var rotaleLeftButton: UIButton!
    @IBOutlet weak var rotaleRightButton: UIButton!
    @IBOutlet weak var brightnessButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
   
    @IBOutlet weak var viewSlider: UIView!
    @IBOutlet weak var sliderBrightness: UISlider!
    @IBOutlet weak var brightnessDoneButton: UIButton!
    @IBOutlet weak var brightnessCancelButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.sliderBrightness.setThumbImage(self.imageWithImage(UIImage ( named: "CircleSlider")!, scaledToSize: CGSize(width: 15, height: 15)), forState: .Normal)

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController!.interactivePopGestureRecognizer!.enabled  = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController!.interactivePopGestureRecognizer!.enabled  = true
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    @IBAction func backAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }

    @IBAction func saveAction(sender: AnyObject) {
        
    }
    
    @IBAction func rotateLeftAction(sender: AnyObject) {
        
    }
    
    @IBAction func rotateRightAction(sender: AnyObject) {
        
    }
    
    @IBAction func bightnessAction(sender: AnyObject) {
        
    }
    
    @IBAction func resetAction(sender: AnyObject) {
        
    }
    
    @IBAction func brightnessDoneAction(sender: AnyObject) {
        
    }
    
    @IBAction func brightnessCancelAction(sender: AnyObject) {
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
