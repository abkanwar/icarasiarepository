//
//  LoginVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/6/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class LoginVC: UIViewController , NSURLSessionDelegate , GIDSignInDelegate , GIDSignInUIDelegate {
    
    
    @IBOutlet weak var textFieldUserName: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    @IBOutlet weak var userNameErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    
    var stringAuthType          = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().clientID = "1027727829451-mpqoecb1fdt3n1mfn3os08u4p9l1f7ui.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
    }
    
    // MARK: - Mannual Login -
    
    @IBAction func mannualLogin(sender: AnyObject){
        /* login With UserName/Email & Password */
        
        self.view.endEditing( true )
        if validateInputData(){
            let dictUserDetails = NSMutableDictionary()
            dictUserDetails.setValue(textFieldUserName.text!, forKey: "userName")
            dictUserDetails.setValue(textFieldPassword.text!, forKey: "password")
            Utility.sharedInstance.saveUserDetails(dictUserDetails)
            ServiceManager.sharedInstance.logIn(textFieldUserName.text!, password: textFieldPassword.text! , loginVC: self)
        }
    }
    
    // MARK: - Facebook Login -
    
    @IBAction func facebookLogin(sender: AnyObject) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logInWithReadPermissions(["public_profile","email"], handler: { (result, error) -> Void in
            
            if ((error) != nil){
            }
            else if result.isCancelled{
            }
            else{
                // If you ask for multiple permissions at once, you //
                // Should check if specific permissions missing     //
                if result.grantedPermissions.contains("email"){
                    stringInstance.sharedString = "facebook"
                    self.getUserDataForFacebook()
                    fbLoginManager.logOut()
                }
            }
        })
    }
    
    func getUserDataForFacebook(){
        
        if((FBSDKAccessToken.currentAccessToken()) != nil){
             let accessToken  = FBSDKAccessToken.currentAccessToken().tokenString
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    let url         = NSURL(string:result .valueForKey("picture")?.valueForKey("data")?.valueForKey("url") as! String)
                    let stringUrl   = url!.absoluteString
                    let id          = result .valueForKey("id") as! String
                    let name        = result .valueForKey("name") as! String
                    
                    print("FBCEBOOK Access Token = \(accessToken) \nFBCEBOOK ID = \(id) \nFBCEBOOK Name = \(name) & \nFBCEBOOK Pic URL = \(stringUrl)")
                }
                else{
                    let alertViewShow = UIAlertController(title:"Facebook Error !" as String, message: "An error encountered while getting info please try again." as String, preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                    }
                    alertViewShow.addAction(okAction)
                    self.presentViewController(alertViewShow, animated: true, completion: nil)
                }
            })
        }
    }
    
    // MARK: - Facebook Login -
    
    @IBAction func googleLogin(sender: AnyObject) {
     
        stringAuthType              = "google"
        stringInstance.sharedString = "google"
        GIDSignIn.sharedInstance().signIn()

    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!){
        
        if (error != nil) {
            print(error)
        }
        else {
            var accessToken = String()
            if (GIDSignIn.sharedInstance().currentUser != nil){
                accessToken = GIDSignIn.sharedInstance().currentUser.authentication.accessToken
            }
            print(GIDSignIn.sharedInstance().currentUser)
            print(GIDSignIn.sharedInstance().currentUser.userID)
            print(GIDSignIn.sharedInstance().currentUser.profile.hasImage)
            print(GIDSignIn.sharedInstance().currentUser.profile.imageURLWithDimension(120))
            print(GIDSignIn.sharedInstance().currentUser.profile.email)
            print(GIDSignIn.sharedInstance().currentUser.profile.name)
            
            let id     = (GIDSignIn.sharedInstance().currentUser.userID) as String
            let name   = (GIDSignIn.sharedInstance().currentUser.profile.name) as String
            
            print("Access Token = \(accessToken) - - id = \(id) - - name \(name)")
        }
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user: GIDGoogleUser!, withError error: NSError!){
        print(" - - didDisconnectWithUser - -")
    }

    
    func updateUserErrorLabels ( text : String , color: UIColor ){
        
        userNameErrorLabel.text         = text
        userNameErrorLabel.textColor    = color
    }
    
    func updatePasswordErrorLabels ( text : String , color: UIColor ){
        
        passwordErrorLabel.text         = text
        passwordErrorLabel.textColor    = color
    }
    
    func validateInputData () -> Bool {
        
        if textFieldUserName.text?.characters.count == 0{
            self.updateUserErrorLabels("Enter username or email", color: UIColor (red: 203/255.0, green: 58/255.0 , blue: 41/255.0, alpha: 1.0))
            return false
        }
        else if textFieldPassword.text?.characters.count == 0{
            self.updatePasswordErrorLabels("Enter password", color: UIColor (red: 203/255.0, green: 58/255.0 , blue: 41/255.0, alpha: 1.0))
            return false
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
       
        //let trimmed             = string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if (string == " " && range.location == 0) {
            return false
        }
        if textField.text!.length > 20 && range.length == 0 && textField == textFieldPassword {
            return false
        }
        else if textField.text!.length > 30 && range.length == 0 && textField == textFieldUserName {
            return false
        }
        else {
            return true
        }
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool{
        
        if textField == textFieldUserName{
            self.updateUserErrorLabels("Username or Email", color: UIColor.darkGrayColor())
        }else{
            self.updatePasswordErrorLabels("Password", color: UIColor.darkGrayColor())
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        
        if textField == textFieldUserName{
            textFieldPassword.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
