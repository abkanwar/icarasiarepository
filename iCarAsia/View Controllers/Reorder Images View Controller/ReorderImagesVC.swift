//
//  ReorderImagesVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/31/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class ReorderImagesVC: UIViewController , UINavigationControllerDelegate , UICollectionViewDelegate , UICollectionViewDataSource {
    
    @IBOutlet weak var collectionViewImages: UICollectionView!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongGesture))
        self.collectionViewImages.addGestureRecognizer(longPressGesture)
        
        if NSUserDefaults.standardUserDefaults().boolForKey("TutorialShown") == false {
            self.addTutorialView ()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.delegate                                  = self
        self.navigationController!.interactivePopGestureRecognizer!.enabled  = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController!.interactivePopGestureRecognizer!.enabled  = true
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    
    @IBAction func backAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func saveAction(sender: AnyObject) {
        
        
        if let tutorialView = self.view.viewWithTag(222) as? TutorialView {
            tutorialView .removeFromSuperview()
            NSNotificationCenter.defaultCenter().postNotificationName("ReloadCollectionViewImages", object: nil)
            self.navigationController?.popViewControllerAnimated(false)
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "TutorialShown")
        }
        else{
            for vc : UIViewController in (self.navigationController?.viewControllers)! {
                if vc.isKindOfClass(AdDetaillsVC){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods -
    
    internal func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        if collectionView.tag == 444{
            return AdDetailStorage.sharedInstance.assets.count ?? 0
        }else{
            return 2
        }
    }
    
    internal func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 444{
            
            //let asset       = self.assets[indexPath.row]
            var cell:               UICollectionViewCell?
            var imageView:          UIImageView?
            var imageViewRibbon:    UIImageView?
            
            //if asset.isVideo {
            //    cell        = collectionView.dequeueReusableCellWithReuseIdentifier("CellVideo", forIndexPath: indexPath)
            //    imageView   = cell?.contentView.viewWithTag(1) as? UIImageView
            //} else {
            cell            = collectionView.dequeueReusableCellWithReuseIdentifier("CellImage", forIndexPath: indexPath)
            imageView       = cell?.contentView.viewWithTag(1) as? UIImageView
            //}
            imageViewRibbon = cell?.contentView.viewWithTag(2) as? UIImageView
            indexPath.row  == 0 ? (imageViewRibbon?.hidden = false) : (imageViewRibbon?.hidden = true)
            
            
            if let cell = cell, imageView = imageView
            {
                let layout  = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                let tag     = indexPath.row + 1
                cell.tag    = tag
                
                if indexPath.row < AdDetailStorage.sharedInstance.assets.count {
                    
                    let asset   = AdDetailStorage.sharedInstance.assets[indexPath.row]
                    asset.fetchImageWithSize(layout.itemSize.toPixel(), completeBlock: { image, info in
                        if cell.tag == tag {
                            imageView.image = image
                        }
                    })
                }else{
                    imageView.image = UIImage( named:  "ImgThumbnail")
                }
            }
            return cell!
            
            
        }else{
            let cell                = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath)
            cell.backgroundColor    = UIColor.clearColor()
            
            //let layout              = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            
            let imageView           = UIImageView()
            imageView.contentMode   = .ScaleAspectFill
            imageView.clipsToBounds = true
            imageView.frame         = CGRectMake(0 , 0 , 92 , 92)
            
            if indexPath.row < AdDetailStorage.sharedInstance.assets.count {
                
                let asset   = AdDetailStorage.sharedInstance.assets[indexPath.row]
                
                var size            = imageView.frame.size
                size.width          = size.width * 2
                size.height         = size.height * 2
                asset.fetchImageWithSize(size.toPixel(), completeBlock: { image, info in
                        imageView.image = image
                })
            }
            cell.addSubview(imageView)
            
            if indexPath.row == 0{
                let imageViewRibbon             =    UIImageView()
                imageViewRibbon.contentMode     = .ScaleAspectFill
                imageViewRibbon.clipsToBounds   = true
                imageViewRibbon.frame           = CGRectMake(0 , 0 , 92 , 92)
                imageViewRibbon.image           = UIImage ( named: "MainPlaceholder" )
                cell.addSubview(imageViewRibbon)
            }
            return cell
        }
    }
    
    internal func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
   
    }
    
    // MARK: collection view cell layout / size
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let   returnSize = CGSizeMake((CGRectGetWidth(self.view.frame)-15)/4, (CGRectGetWidth(self.view.frame)-15)/4)
        return returnSize
    }
    
    
    // MARK: collection view cell paddings
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
        // top, left, bottom, right
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, moveItemAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        
         if collectionView.tag == 444{
            let tempAsset                           = AdDetailStorage.sharedInstance.assets[sourceIndexPath.row]
            AdDetailStorage.sharedInstance.assets.removeAtIndex(sourceIndexPath.row)
            AdDetailStorage.sharedInstance.assets.insert(tempAsset, atIndex: destinationIndexPath.row)
            
            destinationIndexPath.row == 0 ?  (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath])) : (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath]))

        }
         else{
            
            let tempAsset                           = AdDetailStorage.sharedInstance.assets[sourceIndexPath.row]
            AdDetailStorage.sharedInstance.assets.removeAtIndex(sourceIndexPath.row)
            AdDetailStorage.sharedInstance.assets.insert(tempAsset, atIndex: destinationIndexPath.row)
            
            destinationIndexPath.row == 0 ?  (collectionView.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath])) : (collectionView.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath]))
            
            
            if sourceIndexPath.row != 1 && destinationIndexPath.row != 1{
                destinationIndexPath.row == 0 ?  (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath])) : (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath , NSIndexPath (forItem: 1, inSection: 0)]))
            }
            else{
                destinationIndexPath.row == 0 ?  (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath])) : (self.collectionViewImages.reloadItemsAtIndexPaths([sourceIndexPath , destinationIndexPath]))
                
            }
            
            for recognizer in collectionView.gestureRecognizers ?? [] {
                collectionView.removeGestureRecognizer(recognizer)
            }
            
            // Update Tutorial View //
            self.updateTutorial()
        }
        
    }
    
    func updateTutorial () {
        
        self.view.bringSubviewToFront(saveButton)
        
        let buttonMoveImage         = self.view.viewWithTag(666) as! UIButton
        buttonMoveImage.removeFromSuperview()
        
        
        let tutorialView            = self.view.viewWithTag(222) as! TutorialView
        
        for i in 1000..<1005 {
            
            let imageViewTemp       = tutorialView.viewWithTag(i) as! UIImageView
            imageViewTemp.removeFromSuperview()
        }
        
        let imageView               = UIImageView()
        imageView.frame             = CGRectMake(CGRectGetMidX(saveButton.frame)-6, CGRectGetMaxY(saveButton.frame) + 10, 12, 17)
        imageView.image             = UIImage ( named: "ShapeCopy" )
        tutorialView.addSubview(imageView)
        
        UIView.animateWithDuration(0.7, delay:0, options: [.Repeat, .Autoreverse], animations: {
            
            imageView.frame   = CGRectMake(CGRectGetMidX(self.saveButton.frame)-6, CGRectGetMaxY(self.saveButton.frame) + 25, 12, 17)
            }, completion: {(finished: Bool) -> Void in
                CGRectMake(CGRectGetMinX(self.saveButton.frame)-6, CGRectGetMaxY(self.saveButton.frame) + 10, 12, 17)
        })
        
        let labelInfo               = tutorialView.viewWithTag(555) as! UILabel
        labelInfo.text              = "Save to confirm changes."
    }

    
    //MARK:  - Long Press Gesture -
    
    func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        
        switch(gesture.state) {
            
        case UIGestureRecognizerState.Began:
            guard let selectedIndexPath = self.collectionViewImages.indexPathForItemAtPoint(gesture.locationInView(self.collectionViewImages)) else {
                break
            }
            if #available(iOS 9.0, *) {
                if selectedIndexPath.row < AdDetailStorage.sharedInstance.assets.count  {
                    self.collectionViewImages.beginInteractiveMovementForItemAtIndexPath(selectedIndexPath)
                }
            } else {
                // Fallback on earlier versions //
            }
        case UIGestureRecognizerState.Changed:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.updateInteractiveMovementTargetPosition(gesture.locationInView(gesture.view!))
            } else {
                // Fallback on earlier versions //
            }
        case UIGestureRecognizerState.Ended:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.endInteractiveMovement()
            } else {
                // Fallback on earlier versions //
            }
        default:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.cancelInteractiveMovement()
            } else {
                // Fallback on earlier versions //
            }
        }
    }
    
    
    func addTutorialView () {
        
        let tutorialView        = TutorialView(frame: self.view.frame)
        self.view.addSubview(tutorialView)
        
        let buttonMoveImage     = UIButton( type: .Custom )
        buttonMoveImage.frame   = CGRectMake(CGRectGetWidth(self.view.frame) * 0.2 , CGRectGetHeight(self.view.frame) * 0.242 + 20, CGRectGetWidth(self.view.frame) * 0.3 , CGRectGetHeight(self.view.frame) * 0.024)
        buttonMoveImage.setImage(UIImage ( named: "IconArrowLeft"), forState: .Normal)
        buttonMoveImage.setTitle("Move image", forState: .Normal)
        buttonMoveImage.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        buttonMoveImage.titleLabel?.font    = UIFont.boldSystemFontOfSize(12)
        buttonMoveImage.imageEdgeInsets     = UIEdgeInsetsMake(0, 0, 0, 6);
        buttonMoveImage.titleEdgeInsets     = UIEdgeInsetsMake(0, 6, 0, 0);
        buttonMoveImage.tag                 = 666
        tutorialView.addSubview(buttonMoveImage)
        
        UIView.animateWithDuration(0.7, delay:0, options: [.Repeat, .Autoreverse], animations: {
            
            buttonMoveImage.frame   = CGRectMake(CGRectGetWidth(self.view.frame) * 0.2 - 15, CGRectGetHeight(self.view.frame) * 0.242 + 20, CGRectGetWidth(self.view.frame) * 0.3 , CGRectGetHeight(self.view.frame) * 0.024)
            }, completion: {(finished: Bool) -> Void in
                CGRectMake(CGRectGetWidth(self.view.frame) * 0.2 , CGRectGetHeight(self.view.frame) * 0.242 + 20, CGRectGetWidth(self.view.frame) * 0.3 , CGRectGetHeight(self.view.frame) * 0.024)
        })
        
        
        let layout                    = UICollectionViewFlowLayout()
        let collectionView            = UICollectionView(frame: CGRectMake(CGRectGetMinX(collectionViewImages.frame), CGRectGetMinY(collectionViewImages.frame) + 20, CGRectGetWidth(collectionViewImages.frame), 92), collectionViewLayout: layout)
        collectionView.dataSource     = self
        collectionView.delegate       = self
        collectionView.tag            = 333
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifier")
        collectionView.backgroundColor = UIColor.clearColor()
        tutorialView.addSubview(collectionView)
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongGestureTutorialCollection))
        collectionView.addGestureRecognizer(longPressGesture)
        
        /*let indexPath            = NSIndexPath( forRow: 1 , inSection: 0)
         let attributes           = self.collectionViewImages.layoutAttributesForItemAtIndexPath(indexPath)
         let cellRect             = attributes!.frame
         let cellFrameInSuperview = self.collectionViewImages.convertRect(cellRect, toView: self.view)
         
         let imageViewHiglighted            = UIImageView()
         imageViewHiglighted.frame          = cellFrameInSuperview
         imageViewHiglighted.contentMode    = .ScaleAspectFill
         imageViewHiglighted.clipsToBounds  = true
         imageViewHiglighted.userInteractionEnabled = true
         let asset   = AdDetailStorage.sharedInstance.assets[indexPath.row]
         asset.fetchImageWithSize(imageViewHiglighted.frame.size.toPixel(), completeBlock: { image, info in
         imageViewHiglighted.image = image
         })
         tutorialView.addSubview(imageViewHiglighted)*/
        
        /*let imageViewTouchIcon          = UIImageView()
         imageViewTouchIcon.frame        = cellFrameInSuperview
         imageViewTouchIcon.contentMode  = .ScaleAspectFit
         imageViewTouchIcon.image        = UIImage( named: "IconTouch")
         imageViewTouchIcon.frame        = CGRectMake(CGRectGetMidX(imageViewHiglighted.frame) + 15, CGRectGetMidY(imageViewHiglighted.frame) + 15, CGRectGetWidth(self.view.frame) * 0.096875, CGRectGetHeight(self.view.frame) * 0.0725)
         tutorialView.addSubview(imageViewTouchIcon)
         
         UIView.animateWithDuration(0.7, delay:0, options: [.Repeat, .Autoreverse], animations: {
         
         imageViewTouchIcon.frame        = CGRectMake(CGRectGetMidX(imageViewHiglighted.frame) + 15 , CGRectGetMidY(imageViewHiglighted.frame) + 35, CGRectGetWidth(self.view.frame) * 0.096875, CGRectGetHeight(self.view.frame) * 0.0725)
         }, completion: {(finished: Bool) -> Void in
         CGRectMake(CGRectGetMidX(imageViewHiglighted.frame) + 15, CGRectGetMidY(imageViewHiglighted.frame) + 15, CGRectGetWidth(self.view.frame) * 0.096875, CGRectGetHeight(self.view.frame) * 0.0725)
         })*/
    }
    
    //MARK:  - Long Press Gesture -
    
    func handleLongGestureTutorialCollection(gesture: UILongPressGestureRecognizer) {
        
        let collectionView = gesture.view as! UICollectionView

        
        switch(gesture.state) {
        case UIGestureRecognizerState.Began:
            guard let selectedIndexPath = collectionView.indexPathForItemAtPoint(gesture.locationInView(collectionView)) else {
                break
            }
        
            if selectedIndexPath.row == 0{
                break
            }
            
            if #available(iOS 9.0, *) {
                if selectedIndexPath.row < AdDetailStorage.sharedInstance.assets.count  {
                    collectionView.beginInteractiveMovementForItemAtIndexPath(selectedIndexPath)
                }
            } else {
                // Fallback on earlier versions //
            }
        case UIGestureRecognizerState.Changed:
            if #available(iOS 9.0, *) {
                collectionView.updateInteractiveMovementTargetPosition(gesture.locationInView(gesture.view!))
            } else {
                // Fallback on earlier versions //
            }
        case UIGestureRecognizerState.Ended:
            if #available(iOS 9.0, *) {
                collectionView.endInteractiveMovement()
            } else {
                // Fallback on earlier versions //
            }
        default:
            if #available(iOS 9.0, *) {
                collectionView.cancelInteractiveMovement()
            } else {
                // Fallback on earlier versions //
            }
        }
    }
    
    //MARK: - Tutorial View Class -
    
    private class TutorialView: UIView {
        
        private let imageView   = UIImageView()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.userInteractionEnabled = true
            self.tag                    = 222
            self.createUI()
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func createUI() {
            
            let transImageView              = UIImageView(frame: self.frame)
            transImageView.alpha            = 0.8
            transImageView.backgroundColor  = UIColor.blackColor()
            self.addSubview(transImageView)
            
            let space       = CGRectGetWidth(self.frame) * 0.0109375
            var xPos        = CGRectGetWidth(self.frame) * 0.1375
            let yPos        = CGRectGetHeight(self.frame) * 0.331
            for i in 0..<5 {
                
                var imageName        =  "MainPlaceholder"
                var labelText        =  "MainPlaceholder"
                i == 0 ?  (imageName = "MainPlaceholder") : (imageName = "ImgThumbnail")
                i == 0 ?  (labelText = "Main") : (labelText = "\(i+1)")
                
                let frame            = CGRectMake(xPos ,CGRectGetHeight(self.frame) * 0.331 + 20, CGRectGetWidth(self.frame) * 0.1484375, CGRectGetWidth(self.frame) * 0.1484375)
                self.addSubview(imageViewBox(frame , imageName: imageName , textLabel : labelText , tagValue: 1000 + i))
                xPos = xPos + CGRectGetWidth(self.frame) * 0.1484375 + space
            }
            
            
            let labelInfo             = UILabel()
            labelInfo.frame           = CGRectMake(CGRectGetWidth(self.frame) * 0.190625, yPos + CGRectGetWidth(self.frame) * 0.1484375 + CGRectGetWidth(self.frame) * 0.044 + 20, CGRectGetWidth(self.frame) * 0.625, CGRectGetHeight(self.frame) * 0.057)
            labelInfo.textColor       = UIColor.lightGrayColor()
            labelInfo.textAlignment   = .Center
            labelInfo.numberOfLines   = 2
            labelInfo.tag             = 555
            labelInfo.text            = "To set main image, place selected\nimage in the first position."
            labelInfo.font            = UIFont.boldSystemFontOfSize(14)
            self.addSubview(labelInfo)
        }
        
        func imageViewBox ( frame : CGRect , imageName : String , textLabel : String , tagValue: NSInteger) -> UIImageView {
            
            let imageView               = UIImageView()
            imageView.frame             = frame
            imageView.tag               = tagValue
            imageView.image             = UIImage ( named: imageName )
            
            let labelNumber             = UILabel()
            labelNumber.frame           = CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame))
            labelNumber.textColor       = UIColor.lightGrayColor()
            labelNumber.textAlignment   = .Center
            labelNumber.text            = textLabel
            labelNumber.font            = UIFont.boldSystemFontOfSize(12)
            imageView.addSubview(labelNumber)
            
            return imageView
        }
    }
    
    
}
