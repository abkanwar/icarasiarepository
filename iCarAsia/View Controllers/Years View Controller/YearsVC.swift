//
//  YearsVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/19/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class YearsVC: UITableViewController {

    var arrayYears          = NSMutableArray()
    var arrayAllYears       = NSMutableArray()
    
    let searchController    = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.title                      = "Year"
        self.tableView.tableFooterView  = UIView()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        
        arrayYears          = DBHelper.sharedInstance.fetchYearsListFromDB()
        let tempArray       = NSMutableArray()
        arrayYears.enumerateObjectsUsingBlock { (value, index, stop) in
            let valueString   = String(value)
            tempArray.addObject(valueString)
        }
        print("List of years Temp :\(tempArray)")
        arrayYears      = tempArray
        arrayAllYears   = arrayYears
        
        searchController.searchResultsUpdater                   = self
        searchController.searchBar.delegate                     = self
        definesPresentationContext                              = true
        searchController.dimsBackgroundDuringPresentation       = false
        searchController.hidesNavigationBarDuringPresentation   = false
        
        //self.tableView.tableHeaderView                        = searchController.searchBar
        self.chnageTabBarAppearence()
        self.navigationItem.titleView                           = searchController.searchBar
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillDisappear(animated)
        definesPresentationContext                              = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        definesPresentationContext                              = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func chnageTabBarAppearence (){
        
        if #available(iOS 9.0, *) {
            UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).backgroundColor = UIColor(red: 153/255.0 , green: 18/255.0 , blue: 19/255.0 , alpha: 1.0)
        } else {
            for view in searchController.searchBar.subviews {
                for subview in view.subviews {
                    if subview .isKindOfClass(UITextField) {
                        let textField: UITextField = subview as! UITextField
                        textField.backgroundColor = UIColor(red: 153/255.0 , green: 18/255.0 , blue: 19/255.0 , alpha: 1.0)
                    }
                }
            }
        }
        let textFieldInsideSearchBar                = searchController.searchBar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor         = UIColor.whiteColor()
        
        let textFieldInsideSearchBarLabel           = textFieldInsideSearchBar!.valueForKey("placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor    = UIColor(red: 240/255.0 , green: 240/255.0 , blue: 240/255.0 , alpha: 0.6)
        
        searchController.searchBar.setImage(UIImage(named: "IconSearch")!, forSearchBarIcon: .Search, state: .Normal)
        searchController.searchBar.placeholder      = "Type to find"
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if self.arrayYears.count > 0 {
            self.tableView.backgroundView = nil
            return 1
            
        } else {
            
            if searchController.active{
                
                let emptyDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height))
                emptyDataLabel.text             = "No results"
                emptyDataLabel.font             = UIFont.boldSystemFontOfSize(20)
                emptyDataLabel.textColor        = UIColor.lightGrayColor()
                emptyDataLabel.textAlignment    = NSTextAlignment.Center
                self.tableView.backgroundView   = emptyDataLabel
            }
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayYears.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let CellIdentifier: String  = "YearCell"
        let cell: UITableViewCell   = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
        cell.textLabel?.textColor   = UIColor.darkGrayColor()
        cell.textLabel?.text        = "\(arrayYears.objectAtIndex(indexPath.row))"
        cell.textLabel?.font        = UIFont.systemFontOfSize(15)
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 40.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let labelHeaderTitle        = UILabel()
        labelHeaderTitle.text       = "What is the vehicle’s manufacture year?"
        labelHeaderTitle.font       = UIFont.systemFontOfSize(13)
        labelHeaderTitle.backgroundColor = UIColor.whiteColor()
        labelHeaderTitle.textColor  = UIColor.darkGrayColor()
        labelHeaderTitle.frame      = CGRectMake(15, 9, CGRectGetWidth(self.view.frame
            ) - 30, 22)
        
        let viewHeader              = UIView()
        viewHeader.frame            = CGRectMake(0, 0, CGRectGetWidth(self.view.frame
            ), 40)
        viewHeader.backgroundColor  = UIColor.whiteColor()
        viewHeader.addSubview(labelHeaderTitle)
        
        return viewHeader
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        let brandTVC                        = self.storyboard!.instantiateViewControllerWithIdentifier("BrandTVC") as! BrandTVC
        brandTVC.selectedYear               = NSInteger(arrayYears.objectAtIndex(indexPath.row) as! String)!
        AdDetailStorage.sharedInstance.year = NSInteger(arrayYears.objectAtIndex(indexPath.row) as! String)!
        self.navigationController?.pushViewController(brandTVC, animated: true)
    }

    deinit {
        searchController.view.removeFromSuperview()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension YearsVC: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate -
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    }
}

extension YearsVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate -
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let text      = searchController.searchBar.text! as String
        
        if text.characters.count > 0{
            //searchController.dimsBackgroundDuringPresentation   = false
            let searchYear                  = searchBar.text!
            let predicate: NSPredicate      = NSPredicate(format: "SELF CONTAINS[c] %@",searchYear)
            let filteredArray: NSArray      = self.arrayAllYears.filteredArrayUsingPredicate(predicate)
            self.arrayYears                 = filteredArray.mutableCopy() as! NSMutableArray
            self.tableView.reloadData()
        }else{
            //searchController.dimsBackgroundDuringPresentation   = true
            self.arrayYears                 = self.arrayAllYears.mutableCopy() as! NSMutableArray
            self.tableView.reloadData()
        }
        
        
    }
}

