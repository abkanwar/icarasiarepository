//
//  AdDetaillsVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class AdDetaillsVC: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UITextFieldDelegate {
    
    @IBOutlet weak var tableViewAdDetails: UITableView!
    
    weak var textFieldMilage: UITextField!
    weak var textFieldPrice : UITextField!
    weak var textFieldTitle : UITextField!
    
    
    weak var viewColor  : UIView!
    var selectedColor   : Color!
    var removedIndex    : NSIndexPath!
    
    
    var currentString   = String()
    
    var isColor         = Bool()
    var arrayColors     = NSMutableArray()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Create Ad"
        
        self.navigationItem.backBarButtonItem   = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        self.navigationItem.leftBarButtonItem   = nil
        self.navigationItem.hidesBackButton     = true
        
        
        let buttonPublish                       = UIButton()
        buttonPublish.frame                     = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 60)
        buttonPublish.backgroundColor           = UIColor.lightGrayColor()
        buttonPublish.setTitle("Publish", forState: .Normal)
        buttonPublish.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        buttonPublish.titleLabel?.font          = UIFont.boldSystemFontOfSize(20)
        buttonPublish.addTarget(self, action: #selector(pulishAction), forControlEvents: .TouchUpInside)
        self.tableViewAdDetails.tableFooterView = buttonPublish
        
        let croosButton = Utility.sharedInstance.navigationBarCrossButton( "Close" )
        croosButton.addTarget(self, action: #selector(popToRoot), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.rightBarButtonItem   = UIBarButtonItem(customView:croosButton)
        self.tableViewAdDetails.separatorStyle   = UITableViewCellSeparatorStyle.None
        
        isColor     = false
        arrayColors = DBHelper.sharedInstance.fetchColorsFromDB()
        
        AdDetailStorage.sharedInstance.plateNumber      == "" ? (AdDetailStorage.sharedInstance.plateNumber = "- -") : (AdDetailStorage.sharedInstance.plateNumber = AdDetailStorage.sharedInstance.plateNumber)
        AdDetailStorage.sharedInstance.refrenceNumber   == "" ? (AdDetailStorage.sharedInstance.refrenceNumber = "- -") : (AdDetailStorage.sharedInstance.refrenceNumber = AdDetailStorage.sharedInstance.refrenceNumber)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
        tableViewAdDetails.reloadData()
    }
    
    func popToRoot(){
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.//
    }
    
    func pulishAction () {
        
        /*let dictDetails = NSMutableDictionary()
        dictDetails.setValue("IDVKIA_2002AABK", forKey: "icar_vcode")
        dictDetails.setValue("41de9f34-8b78-e411-94b6-06e4185eac05", forKey: "seller_id")
        dictDetails.setValue("45de9f34-8b78-e411-94b6-06e4185eac05", forKey: "seller_profile_id")
        dictDetails.setValue("Recon", forKey: "type")
        dictDetails.setValue("Merah", forKey: "color")
        dictDetails.setValue("MS   Sedan Pertamax 5st 4dr Automatic 6cyl 2493cc FWD", forKey: "description")
        dictDetails.setValue("Banten", forKey: "state")
        dictDetails.setValue("Cilegon", forKey: "area")
        dictDetails.setValue("Jombang", forKey: "city")
        dictDetails.setValue("99999999999", forKey: "price")
        dictDetails.setValue("MS   Sedan Pertamax 5st 4dr Automatic 6cyl 2493cc FWD", forKey: "title")
        dictDetails.setValue("CBU", forKey: "assemble_type")
        dictDetails.setValue("22", forKey: "mileage")
        dictDetails.setValue("20k-25k km", forKey: "mileage_range")
        dictDetails.setValue("2002", forKey: "unique_identifier")
        dictDetails.setValue("2016", forKey: "registered_year")*/
        
        
        let dictDetails: NSMutableDictionary       = ["icar_vcode":"IDVKIA_2002AABK" , "seller_id": "41de9f34-8b78-e411-94b6-06e4185eac05" , "seller_profile_id":"45de9f34-8b78-e411-94b6-06e4185eac05" , "type":"Recon" , "color":"Merah" , "description":"MS   Sedan Pertamax 5st 4dr Automatic 6cyl 2493cc FWD" , "state":"Banten" , "area":"Cilegon" , "city":"Jombang" , "price":"99999999999" , "title":"MS   Sedan Pertamax 5st 4dr Automatic 6cyl 2493cc FWD" , "assemble_type":"CBU" , "mileage":"22" , "mileage_range":"20k-25k km" , "unique_identifier":"2002" , "registered_year":"2016"]

        
        
        ServiceManager.sharedInstance.createNewListing(dictDetails) { (reponseDict, success, error) in
            
            print("Success : \(success) \nError : \(error) \nReponseDict : \(reponseDict) ")
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        var cellHeight = CGFloat()
        
        switch indexPath.row {
        case 0: cellHeight = 200.0
            break
        case 1: cellHeight = 76.0
            break
        case 2: cellHeight = 76.0
            break
        case 3: cellHeight = 90.0
            break
        case 4: cellHeight = 90.0
            break
        case 5: cellHeight = 121.0
            break
        case 6: cellHeight = 90.0
            break
        case 7: cellHeight = 90.0
            break
        default:
            break
        }
        return cellHeight
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell
        
        switch indexPath.row {
            
            
        case 0:
            let CellIdentifier: String  = "CarPicsCell"
            cell                        = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
            
            
            let numberLabel              = cell.viewWithTag(2) as! UILabel
            //if AdDetailStorage.sharedInstance.plateNumber == "" && AdDetailStorage.sharedInstance.refrenceNumber == ""{
            //    numberLabel.text         = ""
            //}else if AdDetailStorage.sharedInstance.plateNumber != "" && AdDetailStorage.sharedInstance.refrenceNumber == ""{
            //    numberLabel.text         = AdDetailStorage.sharedInstance.plateNumber
            //}else{
            numberLabel.text         = "\(AdDetailStorage.sharedInstance.plateNumber) / \( AdDetailStorage.sharedInstance.refrenceNumber)"
            //}
            
            let imagesCollectionView             = cell.viewWithTag(3) as! UICollectionView
            imagesCollectionView.delegate        = self
            imagesCollectionView.dataSource      = self
            imagesCollectionView.backgroundColor = UIColor(red: 246/255.0, green: 246/255.0, blue: 246/255.0, alpha: 1.0)
            imagesCollectionView.reloadData()
            
            break
            
        case 1:
            
            let CellIdentifier: String!
            AdDetailStorage.sharedInstance.brand == "" ? (CellIdentifier  = "VehicleDetailsCellTemp") : (CellIdentifier  = "VehicleDetailsCell")
            
            cell                        = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
            let imageView               = cell.viewWithTag(1) as! UIImageView
            imageView.image             = UIImage ( named: "IconCar")
            
            
            let labelTitle              = cell.viewWithTag(2) as! UILabel
            if AdDetailStorage.sharedInstance.brand == "" {
                labelTitle.text             = "Vehicle Details"
            }else{
                let vehicleInfo = AdDetailStorage.sharedInstance.vehicleInfo as VehicleInfo
                let engine_cc   = vehicleInfo.engine_cc! as NSNumber
                let year        = vehicleInfo.year! as NSNumber
                let month       = vehicleInfo.month! as NSNumber
                
                labelTitle.text             = vehicleInfo.variant! + " " + "\(engine_cc)" + " " + vehicleInfo.fuel_type! + " " + vehicleInfo.transmission! + " " + vehicleInfo.body! + " " + vehicleInfo.driven_wheel! + " " + vehicleInfo.type! + " " + "\(month)" + " " + "\(year)" + " " + vehicleInfo.make! + " " + vehicleInfo.model!
                
                let frame        = labelTitle.frame
                labelTitle.sizeToFit()
                labelTitle.frame = CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), CGRectGetHeight(labelTitle.frame))
            }
           
            
            break
            
        case 2:
            
            let CellIdentifier: String!
            AdDetailStorage.sharedInstance.location == "" ? (CellIdentifier  = "LocationCellTemp") : (CellIdentifier  = "LocationCell")
            
            cell                        = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
            
            let imageView               = cell.viewWithTag(1) as! UIImageView
            imageView.image             = UIImage ( named: "IconLocation")
            
            let labelTitle              = cell.viewWithTag(2) as! UILabel
            
            if AdDetailStorage.sharedInstance.location == "" {
                labelTitle.text         = "Location"
            }
            else{
                labelTitle.text         = "\(AdDetailStorage.sharedInstance.location)  \(AdDetailStorage.sharedInstance.area) \(AdDetailStorage.sharedInstance.city)"
                let frame        = labelTitle.frame
                labelTitle.sizeToFit()
                labelTitle.frame = CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), CGRectGetHeight(labelTitle.frame))
            }
            break
            
        case 3:
            let CellIdentifier: String  = "MileageCell"
            cell                        = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
            let textFieldMilage         = cell.viewWithTag(2) as! UITextField
            self.textFieldMilage        = textFieldMilage
            textFieldMilage.delegate    = self
            
            if AdDetailStorage.sharedInstance.milage > 0.0{
                textFieldMilage.text        = "\(AdDetailStorage.sharedInstance.milage)"
            }else{
                textFieldMilage.text        = ""
            }
            
            break
            
        case 4:
            let CellIdentifier: String  = "PriceCell"
            cell                        = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
            let textFieldPrice          = cell.viewWithTag(2) as! UITextField
            self.textFieldPrice         = textFieldPrice
            textFieldPrice.delegate     = self
            
            if AdDetailStorage.sharedInstance.price > 0{
                textFieldPrice.text        = "\(AdDetailStorage.sharedInstance.price)"
            }else{
                textFieldPrice.text        = ""
            }
            
            break
            
        case 5:
            let CellIdentifier: String  = "ColorCell"
            cell                        = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
            
            let colorCollectionView             = cell.viewWithTag(2) as! UICollectionView
            colorCollectionView.delegate        = self
            colorCollectionView.dataSource      = self
            colorCollectionView.backgroundColor = UIColor.whiteColor()
            
            let view  = cell.viewWithTag(3)! as UIView
            viewColor = view
            if isColor {
                view.hidden = false
                let imageView = cell.viewWithTag(4)! as! UIImageView
                let labelText = cell.viewWithTag(6)! as! UILabel
                
                let color = Utility.sharedInstance.hexStringToUIColor(selectedColor.hex_value! as String)
                imageView.backgroundColor       = color
                labelText.text                  = selectedColor.color_name
                
            }else{
                view.hidden = true
            }
            
            break
            
            
        case 6:
            let CellIdentifier: String      = "TitleCell"
            cell                            = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
            
            let textFieldTitle              = cell.viewWithTag(2) as! UITextField
            self.textFieldTitle             = textFieldTitle
            textFieldTitle.delegate         = self
            
            
            
            if AdDetailStorage.sharedInstance.brand == "" {
                textFieldTitle.text         = ""
            }
            else{
                let vehicleInfo             = AdDetailStorage.sharedInstance.vehicleInfo as VehicleInfo
                let engine_cc               = vehicleInfo.engine_cc! as NSNumber
                let year                    = vehicleInfo.year! as NSNumber
                
                //textFieldTitle.text         = "Hindi"
                
                let title                   = "\(year)" + " " + vehicleInfo.make! + " " + vehicleInfo.model! + " " + "\(engine_cc)" + " " + vehicleInfo.variant! + " " +  vehicleInfo.body! + " " + vehicleInfo.type! + " " + vehicleInfo.transmission!
                textFieldTitle.text         = title
                
                self.makePrefix(title)
            }
            
            break
            
        case 7:
            let CellIdentifier: String    = "DescriptionCell"
            cell                          = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
            
            let labelDescription    = cell.viewWithTag(3) as! UILabel
            
            if AdDetailStorage.sharedInstance.descriptionAd != ""{
                labelDescription.text   = AdDetailStorage.sharedInstance.descriptionAd
            }
            
            break
            
        default:
            cell                        = UITableViewCell()
            break
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        switch indexPath.row {
        case 1:
            //let yearsViewController                     = self.storyboard!.instantiateViewControllerWithIdentifier("YearsViewController") as! YearsViewController
            //self.navigationController?.pushViewController(yearsViewController, animated: true)
            
            let yearsVC                     = self.storyboard!.instantiateViewControllerWithIdentifier("YearsVC") as! YearsVC
            self.navigationController?.pushViewController(yearsVC, animated: true)
            
            break
            
        case 2:
            let locationTVC                     = self.storyboard!.instantiateViewControllerWithIdentifier("LocationTVC") as! LocationTVC
            self.navigationController?.pushViewController(locationTVC, animated: true)
            
            break
            
        case 7:
            let adDescriptionVC                     = self.storyboard!.instantiateViewControllerWithIdentifier("AdDescriptionVC") as! AdDescriptionVC
            self.navigationController?.pushViewController(adDescriptionVC, animated: true)
            
            break
            
        default:
            break
        }
    }
    
    
    
    // MARK: - UICollectionViewDataSource protocol -
    
    /* Tell the Collection View How Many Cells to Make */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 2 {
            return arrayColors.count
        }else{
            
            if AdDetailStorage.sharedInstance.assets.count == 0{
                return 1
            }else{
                return AdDetailStorage.sharedInstance.assets.count + 1
            }
        }
    }
    
    /* Make a Cell for Each Cell Index Path */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        if collectionView.tag == 2 {
            return self.configureCellForColorWith(collectionView, indexPath: indexPath)
        }
        else{
            /* Get a reference to Our Storyboard Cell */
            if AdDetailStorage.sharedInstance.assets.count == 0 {
                return self.configureCellForNewImageWith(collectionView, indexPath: indexPath)
            }
            else{
                if indexPath.row == 0{
                    return self.configureCellForNewImageWith(collectionView, indexPath: indexPath)
                }
                else{
                    return self.configureCellSelectedImageWith(collectionView, indexPath: indexPath)
                }
            }
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        if collectionView.tag == 2{
            
            if (isColor){
                return UIEdgeInsetsMake(0, 60, 0, 0)
            }
            else{
                return UIEdgeInsetsMake(0, 0, 0, 0)
            }
        }
        else{
            if AdDetailStorage.sharedInstance.assets.count == 0 {
                return UIEdgeInsetsMake(0, (CGRectGetWidth(collectionView.frame)-60)/2, 0, 0)
            }else{
                return UIEdgeInsetsMake(0, 0, 0, 0)
            }
        }
    }
    
    // MARK: - UICollectionViewDelegate protocol -
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        print("You Selected Cell : \(indexPath.item)!")
        if collectionView.tag == 2 {
            
            viewColor.hidden = false
            
            if !isColor{
                isColor = true
                
                let selectedColorTemp = arrayColors.objectAtIndex(indexPath.row) as! Color
                let imageView = viewColor.viewWithTag(4)! as! UIImageView
                let labelText = viewColor.viewWithTag(6)! as! UILabel
                let color    = Utility.sharedInstance.hexStringToUIColor(selectedColorTemp.hex_value! as String)
                imageView.backgroundColor       = color
                labelText.text                  = selectedColorTemp.color_name
                
                selectedColor = selectedColorTemp
                removedIndex  = indexPath
                arrayColors.removeObjectAtIndex(indexPath.row)
                
                collectionView.reloadData()
                
            }else{
                
                
                
                let selectedColorTemp = arrayColors.objectAtIndex(indexPath.row) as! Color
                let imageView = viewColor.viewWithTag(4)! as! UIImageView
                let labelText = viewColor.viewWithTag(6)! as! UILabel
                let color    = Utility.sharedInstance.hexStringToUIColor(selectedColorTemp.hex_value! as String)
                imageView.backgroundColor       = color
                labelText.text                  = selectedColorTemp.color_name
                
                arrayColors.insertObject(selectedColor, atIndex: removedIndex.row)
                selectedColor = selectedColorTemp
                removedIndex  = NSIndexPath (forItem: arrayColors.indexOfObject(selectedColorTemp), inSection: 1)
                arrayColors.removeObject(selectedColorTemp)
                
                collectionView.reloadData()
            }
        }else{
            
            /* Reload collection view layout to change cell layout */
            //collectionView.collectionViewLayout.invalidateLayout()
            if indexPath.row == 0 {
                let camera = self.storyboard!.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
                self.navigationController?.pushViewController(camera, animated: true)
            }
        }
        
        
        
    }
    
    func configureCellForNewImageWith ( collectionView : UICollectionView , indexPath : NSIndexPath ) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("EmptyCell", forIndexPath: indexPath)
        cell.backgroundColor = UIColor(red: 246/255.0, green: 246/255.0, blue: 246/255.0, alpha: 1.0)
        let imageView                   = cell.viewWithTag(1) as! UIImageView
        imageView.layer.borderWidth     = 0
        imageView.image                 = UIImage( named: "IconAddPhoto")
        return cell
    }
    
    func configureCellSelectedImageWith ( collectionView : UICollectionView , indexPath : NSIndexPath ) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImagesCollectionCell", forIndexPath: indexPath)
        cell.backgroundColor = UIColor(red: 246/255.0, green: 246/255.0, blue: 246/255.0, alpha: 1.0)
        let imageView                   = cell.viewWithTag(1) as! UIImageView
        imageView.layer.borderWidth     = 0
        
        let asset   = AdDetailStorage.sharedInstance.assets[indexPath.row-1]
        let layout  = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        asset.fetchImageWithSize(layout.itemSize.toPixel(), completeBlock: { image, info in
            imageView.image = image
        })
        return cell
    }
    
    func configureCellForColorWith ( collectionView : UICollectionView , indexPath : NSIndexPath ) -> UICollectionViewCell {
        
        /* Get a reference to Our Storyboard Cell */
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ColorCollectionCell", forIndexPath: indexPath)
        cell.backgroundColor = UIColor.whiteColor()
        
        
        let imageView                   = cell.viewWithTag(1) as! UIImageView
        //imageView.layer.borderWidth     = 0.5
        
        let colorModel                  = arrayColors.objectAtIndex(indexPath.row) as! Color
        
        let color = Utility.sharedInstance.hexStringToUIColor(colorModel.hex_value! as String)
        imageView.backgroundColor       = color
        
        let labelColorName              = cell.viewWithTag(2) as! UILabel
        labelColorName.text             = colorModel.color_name
        
        //let imageViewVerticalLine       = cell.viewWithTag(3) as! UIImageView
        //let imageViewTick               = cell.viewWithTag(4) as! UIImageView
        //if indexPath.row == 0 && isColor{
        //imageViewVerticalLine.hidden    = false
        //imageViewTick.hidden            = false
        //}else{
        //imageViewVerticalLine.hidden    = true
        //imageViewTick.hidden            = true
        //}
        
        return cell
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - UITextFieldDelegate -
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool{
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        
        if textField == textFieldMilage{
            
            if let milage = NSNumberFormatter().numberFromString(textField.text!) {
                AdDetailStorage.sharedInstance.milage = CGFloat(milage)
            }
        }
        
        if  textField == textFieldPrice{
            
            if let price = NSNumberFormatter().numberFromString(textField.text!) {
                AdDetailStorage.sharedInstance.price  = CGFloat(price)
            }
        }
        
        if textField == textFieldTitle{
            AdDetailStorage.sharedInstance.title  = textField.text!
        }
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if (string == " " && range.location == 0) {
            return false
        }
        if textField.text!.length > 4 && range.length == 0 && textField == textFieldMilage{
            return false
        }
        if textField.text!.length > 20 && range.length == 0 && textField == textFieldPrice{
            return false
        }
        //if textField.text!.length > 50 && range.length == 0 && textField == textFieldTitle{
        //    return false
        //}
        
        if textField == textFieldTitle{
            
            if let selectedRange = textField.selectedTextRange {
                let cursorPosition = textField.offsetFromPosition(textField.beginningOfDocument, toPosition: selectedRange.start)
                print("\(cursorPosition)")
            }
            
            if range.location < (textField.attributedText?.length)!
            {
                
                var arbitraryValue: Int         = 0
                
                if AdDetailStorage.sharedInstance.brand != "" {
                    
                    let vehicleInfo             = AdDetailStorage.sharedInstance.vehicleInfo as VehicleInfo
                    let engine_cc               = vehicleInfo.engine_cc! as NSNumber
                    let year                    = vehicleInfo.year! as NSNumber
                    let title                   = "\(year)" + " " + vehicleInfo.make! + " " + vehicleInfo.model! + " " + "\(engine_cc)" + " " + vehicleInfo.variant! + " " +  vehicleInfo.body! + " " + vehicleInfo.type! + " " + vehicleInfo.transmission!
                    
                    arbitraryValue     = title.length
                }
                
                if let newPosition = textField.positionFromPosition(textField.beginningOfDocument, inDirection: UITextLayoutDirection.Right, offset: arbitraryValue){
                    textField.selectedTextRange = textField.textRangeFromPosition(newPosition, toPosition: newPosition)
                }
            }
            // This makes the new text black. //
            textField.typingAttributes  = [NSForegroundColorAttributeName:UIColor.blackColor()]
            let protectedRange          = NSMakeRange(0, (textField.attributedText?.length)!)
            let intersection            = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                return false
            }
            if range.location == 12 {
                return true
            }
        }
        
        if textField == textFieldPrice {
            switch string
            {
            case "0","1","2","3","4","5","6","7","8","9":
                currentString += string
                formatCurrency(currentString)
            default:
                if string.characters.count == 0 && currentString.characters.count != 0
                {
                    currentString = String(currentString.characters.dropLast())
                    formatCurrency(currentString)
                }
            }
            return false
        }
        return true
    }

    func makePrefix( prefix : String ) {
        let attributedString = NSMutableAttributedString(string: prefix)
        //let attributedString = NSMutableAttributedString(string: "C-TAD-")
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSMakeRange(0,prefix.length))
        self.textFieldTitle.attributedText = attributedString
    }
    
    func formatCurrency(string: String)
    {
        let formatter         = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formatter.currencySymbol = ""
        //formatter.locale      = NSLocale(localeIdentifier: "en_MY")
        //let numberFromField   = (NSString(string: currentString).doubleValue)/100
        let numberFromField     = (NSString(string: currentString).doubleValue)
        self.textFieldPrice.text = formatter.stringFromNumber(numberFromField)
    }
    
}
