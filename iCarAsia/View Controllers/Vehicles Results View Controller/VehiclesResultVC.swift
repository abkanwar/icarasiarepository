//
//  VehiclesResultVC.swift
//  iCarAsia
//
//  Created by Raman Kant on 9/7/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class VehiclesResultVC: UIViewController {

    @IBOutlet weak var tableViewVehicles:   UITableView!
    @IBOutlet weak var buttonNext:   UIButton!
    
    var arrayFilteredResults : NSArray!
    var arrayVehicleDetails = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view. //
        
        tableViewVehicles.tableFooterView = UIView()
        
        self.title = String(AdDetailStorage.sharedInstance.year) + " \(AdDetailStorage.sharedInstance.brand)" + " \(AdDetailStorage.sharedInstance.model)"
        
        let buttonInfo                          = UIButton()
        buttonInfo.setImage(UIImage( named: "IconHelp" ), forState: .Normal)
        buttonInfo.sizeToFit()
        buttonInfo.addTarget(self, action: #selector(info), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.rightBarButtonItem   = UIBarButtonItem(customView:buttonInfo)

        let buttonBack                           = UIButton()
        buttonBack.setImage(UIImage( named: "IconBack" ), forState: .Normal)
        buttonBack.sizeToFit()
        buttonBack.addTarget(self, action: #selector(popVC), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.leftBarButtonItem    = UIBarButtonItem(customView:buttonBack)
        
        if arrayFilteredResults.count > 0 {
            arrayVehicleDetails = self.arrayFilteredResults.mutableCopy() as! NSMutableArray
        }
        else{
            arrayVehicleDetails = DBHelper.sharedInstance.fetchVehiclesByYearBrandModelFromDB( AdDetailStorage.sharedInstance.year , brand: AdDetailStorage.sharedInstance.brand , model: AdDetailStorage.sharedInstance.model) as NSMutableArray
            print("Filtered Results = \(arrayVehicleDetails)")

        }
       
    }

    func popVC () {
    
        for controller in (self.navigationController?.viewControllers.reverse())! {
            
            if controller.isKindOfClass(AdditionalInfoVC){
                self.navigationController?.popToViewController(controller, animated: true)
            }
            
            if controller.isKindOfClass(ModelTVC){
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
    
    func info () {
        
    }
    
    @IBAction func buttonNextAction (sender: AnyObject){
    
        for controller in (self.navigationController?.viewControllers.reverse())! {
            
            if controller.isKindOfClass(AdDetaillsVC){
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated. //
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayVehicleDetails.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let CellIdentifier: String          = "ResultCell"
        let cell: UITableViewCell           = tableView.dequeueReusableCellWithIdentifier(CellIdentifier)!
        
        let vehicleInfo                     = arrayVehicleDetails[indexPath.row] as! VehicleInfo
        
        let labelVarientCC                  = cell.viewWithTag(2) as! UILabel
        
        let engineCC                        = vehicleInfo.engine_cc! as Double
        let multiplier                      = pow(10.0, 1)
        let num                             = engineCC/1000.0
        let rounded                         = round(num * multiplier) / multiplier
        labelVarientCC.text                 = String (rounded) + " " + vehicleInfo.variant!

        
        var transmission = ""
        if vehicleInfo.transmission != ""{
            transmission = "\u{25CF} " + vehicleInfo.transmission!
        }
        var fuelType    = ""
        if vehicleInfo.fuel_type != ""{
            fuelType    = "   \u{25CF} " + vehicleInfo.fuel_type!
        }
        var drivenWheel = ""
        if vehicleInfo.driven_wheel != ""{
            drivenWheel = "   \u{25CF} " + vehicleInfo.driven_wheel!
        }
        var month       = ""
        if vehicleInfo.month != 0{
            
            let monthNumber = Int( vehicleInfo.month! )
            let formatter   = NSDateFormatter()
            let monthName   = formatter.shortMonthSymbols[(monthNumber - 1)]
            month = "   \u{25CF} Facelift " + monthName + " \(AdDetailStorage.sharedInstance.year)"
        }
        
        let labelDetails                    = cell.viewWithTag(3) as! UILabel
        labelDetails.text = transmission + fuelType + drivenWheel + month
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 45.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let labelHeaderTitle        = UILabel()
        labelHeaderTitle.text       = "Select variant that match and save it!"
        labelHeaderTitle.font       = UIFont.systemFontOfSize(13)
        labelHeaderTitle.backgroundColor = UIColor.whiteColor()
        labelHeaderTitle.textColor  = UIColor.darkGrayColor()
        labelHeaderTitle.frame      = CGRectMake(15, 11.5, CGRectGetWidth(self.view.frame
            ) - 30, 22)
        
        let viewHeader              = UIView()
        viewHeader.frame            = CGRectMake(0, 0, CGRectGetWidth(self.view.frame
            ), 45)
        viewHeader.backgroundColor  = UIColor.whiteColor()
        
        viewHeader.addSubview(labelHeaderTitle)
        
        return viewHeader
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
    
        let cell                = tableView.cellForRowAtIndexPath(indexPath)
        let radioButton         = cell!.viewWithTag(1) as! UIButton
        radioButton.selected    = true
        buttonNext.selected     = true
        
        AdDetailStorage.sharedInstance.vehicleInfo = arrayVehicleDetails[indexPath.row] as! VehicleInfo
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath){
       
        let cell                = tableView.cellForRowAtIndexPath(indexPath)
        let radioButton         = cell!.viewWithTag(1) as! UIButton
        radioButton.selected    = false
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
