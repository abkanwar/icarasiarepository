//
//  DBHelper.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/24/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit

class DBHelper: NSObject {
    
    class var sharedInstance: DBHelper {
        struct Static {
            static let instance: DBHelper = DBHelper()
        }
        return Static.instance
    }
    
    
    /* Save Dealer App Data In DB */
    func saveAllAppDataInDB ( resopnseDictionary : NSMutableDictionary ){
        
        let appDelegate     = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext  = appDelegate.managedObjectContext
        
        /* Save Vehicle Info in DB ( Smile ) */
        
        let arrayVehicleInfo = resopnseDictionary.valueForKey("vehicleInfo") as! NSArray
        for vehicleInfoIndex in 0..<arrayVehicleInfo.count {
            
            print("Index : \(vehicleInfoIndex)")
            let dictVehicleInfo =  arrayVehicleInfo[vehicleInfoIndex]
            let entity          =  NSEntityDescription.entityForName("VehicleInfo", inManagedObjectContext:managedContext)
            let vehicleInfo     = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
            
            if let value = dictVehicleInfo["icar_vcode"] as? String {
                vehicleInfo.setValue(value, forKey: "icar_vcode")
            }else{
                vehicleInfo.setValue("", forKey: "icar_vcode")
            }
            if let value = dictVehicleInfo["series"] as? String {
                vehicleInfo.setValue(value, forKey: "series")
            }else{
                vehicleInfo.setValue("", forKey: "series")
            }
            if let value = dictVehicleInfo["make"] as? String {
                vehicleInfo.setValue(value, forKey: "make")
            }else{
                vehicleInfo.setValue("", forKey: "make")
            }
            if let value = dictVehicleInfo["model"] as? String {
                vehicleInfo.setValue(value, forKey: "model")
            }else{
                vehicleInfo.setValue("", forKey: "model")
            }
            if let value = dictVehicleInfo["status"] as? String {
                vehicleInfo.setValue(value, forKey: "status")
            }else{
                vehicleInfo.setValue("", forKey: "status")
            }
            if let year    = dictVehicleInfo["year"]{
                vehicleInfo.setValue(year, forKey: "year")
            }else{
                vehicleInfo.setValue(0, forKey: "year")
            }
            
            if let engineCC = dictVehicleInfo["engine_cc"]{
                if engineCC is NSNull{
                    vehicleInfo.setValue(0, forKey: "engine_cc")
                }else{
                    vehicleInfo.setValue(engineCC, forKey: "engine_cc")
                }
            }else{
                vehicleInfo.setValue(0, forKey: "engine_cc")
            }
            
            if let value = dictVehicleInfo["vehicle_name"] as? String {
                vehicleInfo.setValue(value, forKey: "vehicle_name")
            }else{
                vehicleInfo.setValue("", forKey: "vehicle_name")
            }
            if let value = dictVehicleInfo["fuel_type"] as? String {
                vehicleInfo.setValue(value, forKey: "fuel_type")
            }else{
                vehicleInfo.setValue("", forKey: "fuel_type")
            }
            
            
            
            if let value = dictVehicleInfo["group"] as? String {
                vehicleInfo.setValue(value, forKey: "group")
            }else{
                vehicleInfo.setValue("", forKey: "group")
            }
            
            if let year    = dictVehicleInfo["month"]{
                vehicleInfo.setValue(year, forKey: "month")
            }else{
                vehicleInfo.setValue(0, forKey: "month")
            }
            
            if let value = dictVehicleInfo["variant"] as? String {
                vehicleInfo.setValue(value, forKey: "variant")
            }else{
                vehicleInfo.setValue("", forKey: "variant")
            }
            
            if let value = dictVehicleInfo["body"] as? String {
                vehicleInfo.setValue(value, forKey: "body")
            }else{
                vehicleInfo.setValue("", forKey: "body")
            }
            
            if let value = dictVehicleInfo["class"] as? String {
                vehicleInfo.setValue(value, forKey: "clas")
            }else{
                vehicleInfo.setValue("", forKey: "clas")
            }
            
            if let value = dictVehicleInfo["lifestyle"] as? String {
                vehicleInfo.setValue(value, forKey: "lifestyle")
            }else{
                vehicleInfo.setValue("", forKey: "lifestyle")
            }
            
            if let value = dictVehicleInfo["lifecycle"] as? String {
                vehicleInfo.setValue(value, forKey: "lifecycle")
            }else{
                vehicleInfo.setValue("", forKey: "lifecycle")
            }
            
            if let engineCC = dictVehicleInfo["doors"]{
                if engineCC is NSNull{
                    vehicleInfo.setValue(0, forKey: "doors")
                }else{
                    vehicleInfo.setValue(engineCC, forKey: "doors")
                }
            }else{
                vehicleInfo.setValue(0, forKey: "doors")
            }
            
            
            if let value = dictVehicleInfo["type"] as? String {
                vehicleInfo.setValue(value, forKey: "type")
            }else{
                vehicleInfo.setValue("", forKey: "type")
            }
            
            if let value = dictVehicleInfo["provider"] as? String {
                vehicleInfo.setValue(value, forKey: "provider")
            }else{
                vehicleInfo.setValue("", forKey: "provider")
            }
            
            if let value = dictVehicleInfo["nickname"] as? String {
                vehicleInfo.setValue(value, forKey: "nickname")
            }else{
                vehicleInfo.setValue("", forKey: "nickname")
            }
            
            if let value = dictVehicleInfo["driven_wheel"] as? String {
                vehicleInfo.setValue(value, forKey: "driven_wheel")
            }else{
                vehicleInfo.setValue("", forKey: "driven_wheel")
            }
            
            if let value = dictVehicleInfo["model_alias"] as? String {
                vehicleInfo.setValue(value, forKey: "model_alias")
            }else{
                vehicleInfo.setValue("", forKey: "model_alias")
            }
            if let value = dictVehicleInfo["transmission"] as? String {
                vehicleInfo.setValue(value, forKey: "transmission")
            }else{
                vehicleInfo.setValue("", forKey: "transmission")
            }
            
            
        }
        
        
        /* Save Descriptions in DB ( Smile ) */
        
        let arrayDescriptions = resopnseDictionary.valueForKey("descriptions") as! NSArray
        
        for desIndex in 0..<arrayDescriptions.count {
            
            print("Index : \(desIndex)")
            let dictDescription =  arrayDescriptions[desIndex]
            let entity          =  NSEntityDescription.entityForName("Descriptions", inManagedObjectContext:managedContext)
            let descriptions    = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
            
            if let value = dictDescription["id"] as? NSInteger {
                descriptions.setValue(value, forKey: "id")
            }else{
                descriptions.setValue("", forKey: "id")
            }
            if let value = dictDescription["name"] as? String {
                descriptions.setValue(value, forKey: "name")
            }else{
                descriptions.setValue("", forKey: "name")
            }
            if let value = dictDescription["description"] as? String {
                descriptions.setValue(value, forKey: "dscription")
            }else{
                descriptions.setValue("", forKey: "dscription")
            }
        }
        
        /* Save Colors in DB */
        
        let arrayColors = resopnseDictionary.valueForKey("color") as! NSArray
        
        for colorIndex in 0..<arrayColors.count {
            
            let dictColor       =  arrayColors[colorIndex]
            let entity          =  NSEntityDescription.entityForName("Color", inManagedObjectContext:managedContext)
            let color           = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
            
            if let value = dictColor["name"] as? String {
                color.setValue(value, forKey: "color_name")
            }else{
                color.setValue("", forKey: "color_name")
            }
            if let value = dictColor["hex_value"] as? String {
                color.setValue(value, forKey: "hex_value")
            }else{
                if let value = dictColor["hex_value"] as? NSInteger {
                    color.setValue("\(value)", forKey: "hex_value")
                }else{
                    color.setValue("", forKey: "hex_value")
                }
            }
            if let value = dictColor["css_class"] as? String {
                color.setValue(value, forKey: "css_class")
            }else{
                color.setValue("", forKey: "css_class")
            }
        }
        
        
        /* Save Location , Area & Cities in DB */
        
        let arrayLocationsKeys = NSArray (arrayLiteral: (resopnseDictionary.valueForKey("locations")?.allKeys)!)
        print("\(arrayLocationsKeys)")
        let arrayAllLocations  = arrayLocationsKeys[0];
        
        for locationIndex in 0..<arrayAllLocations.count-1 {
            
            print("Saved location Index : \(locationIndex)")
            
            let dictLocationDetails = resopnseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex))")
            let entity              = NSEntityDescription.entityForName("Locations", inManagedObjectContext:managedContext)
            let location            = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
            location.setValue(dictLocationDetails?.valueForKey("name"), forKey: "location_name")
            location.setValue(dictLocationDetails?.valueForKey("id"), forKey: "location_id")
            
            let arrayAreaKeys = NSArray (arrayLiteral: (resopnseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area")?.allKeys)!)
            let arrayAllAreas  = arrayAreaKeys[0];
            
            for areaIndex in 0..<arrayAllAreas.count {
                
                print("Saved area Index : \(areaIndex)")
                
                let dictAreaDetails = resopnseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area.\(arrayAllAreas.objectAtIndex(areaIndex))")
                let entity          = NSEntityDescription.entityForName("Areas", inManagedObjectContext:managedContext)
                let area            = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                area.setValue(dictAreaDetails?.valueForKey("id"), forKey: "area_id")
                area.setValue(dictAreaDetails?.valueForKey("name"), forKey: "area_name")
                area.setValue(dictLocationDetails?.valueForKey("name"), forKey: "location_name")
                
                
                if((resopnseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area.\(arrayAllAreas.objectAtIndex(areaIndex)).city")) != nil){
                    
                    let arrayCitiesKeys = NSArray (arrayLiteral: (resopnseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area.\(arrayAllAreas.objectAtIndex(areaIndex)).city")?.allKeys)!)
                    let arrayAllCities  = arrayCitiesKeys[0];
                    
                    for cityIndex in 0..<arrayAllCities.count {
                        
                        print("Saved City Index : \(cityIndex)")
                        
                        let dictCityDetails = resopnseDictionary.valueForKeyPath("locations.\(arrayAllLocations.objectAtIndex(locationIndex)).area.\(arrayAllAreas.objectAtIndex(areaIndex)).city.\(arrayAllCities.objectAtIndex(cityIndex))")
                        let entity          = NSEntityDescription.entityForName("Cities", inManagedObjectContext:managedContext)
                        let city            = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                        city.setValue(dictCityDetails?.valueForKey("id"), forKey: "city_id")
                        city.setValue(dictCityDetails?.valueForKey("name"), forKey: "city_name")
                        city.setValue(dictAreaDetails?.valueForKey("name"), forKey: "area_name")
                    }
                }
            }
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    /* Fetch years listing from DB */
    
    func fetchYearsListFromDB () -> NSMutableArray {
        
        let appDelegate         = UIApplication.sharedApplication().delegate as! AppDelegate
        let context             = appDelegate.managedObjectContext
        
        let fetch               = NSFetchRequest()
        let entityDescription   = NSEntityDescription.entityForName("VehicleInfo", inManagedObjectContext: context)
        fetch.entity            = entityDescription!
        fetch.propertiesToFetch = ["year"]
        //let sortByYear          = NSSortDescriptor(key: "year", ascending: true)
        //let sortDescriptors     = [sortByYear]
        //fetch.sortDescriptors   = sortDescriptors
        do {
            let fetchedObjects           = try context.executeFetchRequest(fetch) as NSArray
            let uniqueYears              = fetchedObjects.valueForKeyPath("@distinctUnionOfObjects.year") as! NSArray
            
            let sortedYears : NSArray    = uniqueYears.sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "", ascending: false)
                ])
            print("List of years :\(sortedYears)")
            return sortedYears.mutableCopy() as! NSMutableArray
        }
        catch let error {
            print("Error : \(error)")
        }
        
        return NSMutableArray()
    }
    
    /* Fetch Brands listing from DB */
    
    func fetchBrandByYearFromDB ( year : NSInteger ) -> NSMutableArray {
        
        let appDelegate         = UIApplication.sharedApplication().delegate as! AppDelegate
        let context             = appDelegate.managedObjectContext
        
        let fetch               = NSFetchRequest()
        let entityDescription   = NSEntityDescription.entityForName("VehicleInfo", inManagedObjectContext: context)
        fetch.entity            = entityDescription!
        fetch.propertiesToFetch = ["make", "year"]
        
        let predicate           = NSPredicate(format: "year Contains[cd] %d",year)
        fetch.predicate         = predicate
        
        do {
            let fetchedObjects           = try context.executeFetchRequest(fetch) as NSArray
            let uniqueBrands             = fetchedObjects.valueForKeyPath("@distinctUnionOfObjects.make") as! NSArray
            
            let sortedBrands : NSArray    = uniqueBrands.sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "", ascending: true)
                ])
            print("List of Brands :\(sortedBrands)")
            return sortedBrands.mutableCopy() as! NSMutableArray
        }
        catch let error {
            print("Error : \(error)")
        }
        
        return NSMutableArray()
    }
    
    /* Fetch Models listing from DB */
    
    func fetchModelByBrandFromDB ( year : NSInteger , brand : String ) -> NSMutableArray {
        
        let appDelegate         = UIApplication.sharedApplication().delegate as! AppDelegate
        let context             = appDelegate.managedObjectContext
        
        let fetch               = NSFetchRequest()
        let entityDescription   = NSEntityDescription.entityForName("VehicleInfo", inManagedObjectContext: context)
        fetch.entity            = entityDescription!
        fetch.propertiesToFetch = ["make","model"]
        
        let predicate           = NSPredicate(format: "year Contains[cd] %d AND make Contains[cd] %@" , year , brand)
        fetch.predicate         = predicate
        
        do {
            let fetchedObjects           = try context.executeFetchRequest(fetch) as NSArray
            let uniqueModels             = fetchedObjects.valueForKeyPath("@distinctUnionOfObjects.model") as! NSArray
            
            let sortedModels : NSArray    = uniqueModels.sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "", ascending: true)
                ])
            print("List of Models :\(sortedModels)")
            return sortedModels.mutableCopy() as! NSMutableArray
        }
        catch let error {
            print("Error : \(error)")
        }
        
        return NSMutableArray()
    }
    
    
    func fetchVehiclesByYearBrandModelFromDB ( year : NSInteger , brand : String , model : String ) -> NSMutableArray {
        
        let appDelegate         = UIApplication.sharedApplication().delegate as! AppDelegate
        let context             = appDelegate.managedObjectContext
        
        let fetch               = NSFetchRequest()
        let entityDescription   = NSEntityDescription.entityForName("VehicleInfo", inManagedObjectContext: context)
        fetch.entity            = entityDescription!
        //fetch.propertiesToFetch = ["make", "year"]
        
        let predicate           = NSPredicate(format: "year Contains[cd] %d AND make Contains[cd] %@ AND model Contains[cd] %@",year , brand , model)
        fetch.predicate         = predicate
        
        do {
            let fetchedObjects           = try context.executeFetchRequest(fetch) as NSArray
            //let uniqueBrands           = fetchedObjects.valueForKeyPath("@distinctUnionOfObjects.engine_cc") as! NSArray
            let sortedBrands : NSArray   = fetchedObjects.sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "engine_cc", ascending: true)
                ])
            print("List of Brands :\(sortedBrands)")
            return sortedBrands.mutableCopy() as! NSMutableArray
        }
        catch let error {
            print("Error : \(error)")
        }
        
        return NSMutableArray()
    }
    
    
    /* Fetch locations listing from DB */
    
    func fetchLocationsFromDB () -> NSMutableArray {
        
        let appDelegate         = UIApplication.sharedApplication().delegate as! AppDelegate
        let context             = appDelegate.managedObjectContext
        
        let fetch               = NSFetchRequest()
        let entityDescription   = NSEntityDescription.entityForName("Locations", inManagedObjectContext: context)
        fetch.entity            = entityDescription!
        //fetch.propertiesToFetch = ["make","model"]
        
        //let predicate           = NSPredicate(format: "make Contains[cd] %@",brand)
        //fetch.predicate         = predicate
        
        do {
            let fetchedObjects           = try context.executeFetchRequest(fetch) as NSArray
            let uniqueLocations          = fetchedObjects.valueForKeyPath("@distinctUnionOfObjects.location_name") as! NSArray
            
            let sortedLocations : NSArray    = uniqueLocations.sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "", ascending: true)
                ])
            print("List of locations :\(sortedLocations)")
            return sortedLocations.mutableCopy() as! NSMutableArray
        }
        catch let error {
            print("Error : \(error)")
        }
        
        return NSMutableArray()
    }
    
    /* Fetch Area listing from DB */
    
    func fetchAreaFromDB ( locationName : String ) -> NSMutableArray {
        
        let appDelegate         = UIApplication.sharedApplication().delegate as! AppDelegate
        let context             = appDelegate.managedObjectContext
        
        let fetch               = NSFetchRequest()
        let entityDescription   = NSEntityDescription.entityForName("Areas", inManagedObjectContext: context)
        fetch.entity            = entityDescription!
        //fetch.propertiesToFetch = ["make","model"]
        
        let predicate           = NSPredicate(format: "location_name Contains[cd] %@",locationName)
        fetch.predicate         = predicate
        
        do {
            let fetchedObjects  = try context.executeFetchRequest(fetch) as NSArray
            let uniqueAreas     = fetchedObjects.valueForKeyPath("@distinctUnionOfObjects.area_name") as! NSArray
            
            let sortedAreas : NSArray    = uniqueAreas.sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "", ascending: true)
                ])
            print("List of Areas :\(sortedAreas)")
            return sortedAreas.mutableCopy() as! NSMutableArray
        }
        catch let error {
            print("Error : \(error)")
        }
        
        return NSMutableArray()
    }
    
    /* Fetch Cities listing from DB */
    
    func fetchCitiesFromDB ( areaName : String ) -> NSMutableArray {
        
        let appDelegate         = UIApplication.sharedApplication().delegate as! AppDelegate
        let context             = appDelegate.managedObjectContext
        
        let fetch               = NSFetchRequest()
        let entityDescription   = NSEntityDescription.entityForName("Cities", inManagedObjectContext: context)
        fetch.entity            = entityDescription!
        //fetch.propertiesToFetch = ["make","model"]
        
        let predicate           = NSPredicate(format: "area_name Contains[cd] %@",areaName)
        fetch.predicate         = predicate
        
        do {
            let fetchedObjects  = try context.executeFetchRequest(fetch) as NSArray
            let uniqueCities    = fetchedObjects.valueForKeyPath("@distinctUnionOfObjects.city_name") as! NSArray
            
            let sortedCities : NSArray    = uniqueCities.sortedArrayUsingDescriptors([
                NSSortDescriptor(key: "", ascending: true)
                ])
            print("List of Cities :\(sortedCities)")
            return sortedCities.mutableCopy() as! NSMutableArray
        }
        catch let error {
            print("Error : \(error)")
        }
        
        return NSMutableArray()
    }
    
    /* Fetch Colors listing from DB */
    
    func fetchColorsFromDB () -> NSMutableArray {
        
        let appDelegate         = UIApplication.sharedApplication().delegate as! AppDelegate
        let context             = appDelegate.managedObjectContext
        
        let fetch               = NSFetchRequest()
        let entityDescription   = NSEntityDescription.entityForName("Color", inManagedObjectContext: context)
        fetch.entity            = entityDescription!
        
        do {
            let fetchedObjects  = try context.executeFetchRequest(fetch) as NSArray
            return fetchedObjects.mutableCopy() as! NSMutableArray
        }
        catch let error {
            print("Error : \(error)")
        }
        return NSMutableArray()
    }
}
