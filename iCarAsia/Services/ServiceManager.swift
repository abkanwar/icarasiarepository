//
//  ServiceManager.swift
//  iCarAsia
//
//  Created by Raman Kant on 8/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

import UIKit



class ServiceManager : NSObject , NSURLSessionDelegate {
    
    class var sharedInstance: ServiceManager {
        struct Static {
            static let instance: ServiceManager = ServiceManager()
        }
        return Static.instance
    }
    
    
    // MARK: - Login Services For QuickBlox & iCarAsia -
    
    /* Authenticate User On iCar Asia Server
     After Authentication login user to Quickblox Server
     if user exists then login (Other wise sign up as ne user)
     
     */
    
    func logIn( userName : String , password : String , loginVC : LoginVC)  {
        
        SVProgressHUD.showWithStatus("SA_STR_LOGGING_IN_AS".localized + userName, maskType: SVProgressHUDMaskType.Clear)
        let post: NSMutableDictionary       = ["user_name":userName,"password": password,"auth_type":"email"]
        let requestData : NSData            = NSKeyedArchiver.archivedDataWithRootObject(post)
        let urlString: String               = "http://ex-api.used246.icarasia.net/v2.0/id/id/authentication/dealer_login"
        let request: NSMutableURLRequest    = NSMutableURLRequest(URL: NSURL(string: urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
        request.HTTPMethod                  = "POST"
        request.setValue("6a005295c93b619124c51b33d0af0c88", forHTTPHeaderField: "app_key")
        request.setValue(post.valueForKey("user_name")as? String, forHTTPHeaderField: "user_name")
        request.setValue(post.valueForKey("password")as? String, forHTTPHeaderField: "password")
        request.setValue(post.valueForKey("auth_type")as? String, forHTTPHeaderField: "auth_type")
        request.setValue("iOS_MDA", forHTTPHeaderField: "platform")
        request.setValue("1.0", forHTTPHeaderField: "api_version")
        //request.setValue(post.valueForKey("auth_token")as? String, forHTTPHeaderField: "auth_token")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.setValue("\(UInt(requestData.length))", forHTTPHeaderField: "Content-Length")
        request.HTTPBody                    = requestData
        request.timeoutInterval             = 90.0
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            
            do{
                if error?.code == -1009{
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        SVProgressHUD.dismiss()
                        SWMessage.sharedInstance.dismissActiveNotification()
                        SWMessage.sharedInstance.showNotificationWithTitle("No internet connection", subtitle: nil, type: .Error) // (error?.localizedDescription)!
                    }
                    return
                }
                
                
                let resonseDictionary : NSMutableDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSMutableDictionary
                print(resonseDictionary)
                
                if ((resonseDictionary.valueForKey("message")?.isEqualToString("Unauthorized user")) != nil){
                    
                    dispatch_async(dispatch_get_main_queue()) { // [unowned self] in
                        
                        SVProgressHUD.dismiss()
                        let alert = UIAlertController(title: "Alert", message: resonseDictionary.valueForKey("message") as? String, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                        loginVC.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) { // [unowned self] in
                        
                        Utility.sharedInstance.saveToken(resonseDictionary.valueForKey("token") as! String)
                        self.loginOnQuickBlox( (post.valueForKey("user_name")as? String)!, password: (post.valueForKey("password")as? String)!)
                    }
                }
            }
            catch {
                print("json error: \(error)")
            }
        }
        task.resume()
        return
    }
    
    /* Logging User To Quickblox REST API and Chat. */
    
    func loginOnQuickBlox ( userName: String , password: String) {
        
        let loginUser = QBUUser()
        loginUser.login      = userName
        loginUser.password   = password + String("ab")
        
        ServicesManager.instance().logInWithUser(loginUser, completion:{ (success:Bool , errorMessage: String?) -> Void in
            
            
            
            guard success else {
                
                /* If User Don't Exist Signup As New User */
                self.signIn(userName, password: password)
                return
            }
            
            dispatch_async(dispatch_get_main_queue()) { // [unowned self] in
                
                SVProgressHUD.dismiss()
                /* Update User Login Status */
                Utility.sharedInstance.loginStatus()
                
                /* Update UI Root Controller To Tab Bar Controller */
                let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
                delegate.changeRootToMainTabBar()
            }
        })
    }
    
    /* SignUp User To Quickblox REST API and Chat. */
    
    func signIn( userName: String , password: String) {
        
        let loginUser = QBUUser()
        loginUser.login      = userName
        loginUser.password   = password + String("ab")
        
        QBRequest.signUp(loginUser, successBlock: {(response, user) in
            
            // Sign up was successfully //
            print("\(user?.ID)")
            dispatch_async(dispatch_get_main_queue()) { // [unowned self] in
                
                SVProgressHUD.dismiss()
                Utility.sharedInstance.loginStatus()
                let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
                delegate.changeRootToMainTabBar()
            }
            }, errorBlock: {(response: QBResponse) in
                // Handle error here //
                print("\(response.description)")
                SVProgressHUD.showErrorWithStatus("An error encounterd while login please try again")
        })
    }
    
    /* Get All App Data from DealerAppData we service and Save in DB */
    
    func getAppData () {
        
        SVProgressHUD.showWithStatus("SA_STR_LOADING".localized, maskType: SVProgressHUDMaskType.Clear)
        let urlString: String               = "http://ex-api.used246.icarasia.net/v2.0/id/id/dealerappdata"
        let request: NSMutableURLRequest    = NSMutableURLRequest(URL: NSURL(string: urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
        request.HTTPMethod                  = "GET"
        request.setValue(Utility.sharedInstance.token(), forHTTPHeaderField: "token")
        request.setValue("iOS_MDA", forHTTPHeaderField: "platform")
        request.setValue("1.0", forHTTPHeaderField: "api_version")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.timeoutInterval             = 90.0
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            
            do{
                let responseDictionary : NSMutableDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSMutableDictionary
                
                DBHelper.sharedInstance.saveAllAppDataInDB(responseDictionary)
                Utility.sharedInstance.statusDB()
                dispatch_async(dispatch_get_main_queue()) { // [unowned self] in
                    SVProgressHUD.dismiss()
                }
            }
            catch {
                print("json error: \(error)")
            }
        }
        task.resume()
        
    }
    
    /* Authentcate User after Expiring Token */
    
    func authentcateUserWith( userName: String , password : String , completionClosure: ( success: Bool , error : NSError)-> ()) {
        
        
        SVProgressHUD.showWithStatus("SA_STR_LOGGING_IN_AS".localized + userName, maskType: SVProgressHUDMaskType.Clear)
        let post: NSMutableDictionary       = ["user_name":userName,"password": password,"auth_type":"email"]
        let requestData : NSData            = NSKeyedArchiver.archivedDataWithRootObject(post)
        let urlString: String               = "http://ex-api.used246.icarasia.net/v2.0/id/id/authentication/dealer_login"
        let request: NSMutableURLRequest    = NSMutableURLRequest(URL: NSURL(string: urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
        request.HTTPMethod                  = "POST"
        request.setValue("6a005295c93b619124c51b33d0af0c88", forHTTPHeaderField: "app_key")
        request.setValue(post.valueForKey("user_name")as? String, forHTTPHeaderField: "user_name")
        request.setValue(post.valueForKey("password")as? String, forHTTPHeaderField: "password")
        request.setValue(post.valueForKey("auth_type")as? String, forHTTPHeaderField: "auth_type")
        request.setValue("iOS_MDA", forHTTPHeaderField: "platform")
        request.setValue("1.0", forHTTPHeaderField: "api_version")
        //request.setValue(post.valueForKey("auth_token")as? String, forHTTPHeaderField: "auth_token")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.setValue("\(UInt(requestData.length))", forHTTPHeaderField: "Content-Length")
        request.HTTPBody                    = requestData
        request.timeoutInterval             = 90.0
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            
            
            do{
                if (error != nil) {
                    
                    if error?.code == -1009{
                        dispatch_async(dispatch_get_main_queue()) {
                            SVProgressHUD.dismiss()
                            SWMessage.sharedInstance.dismissActiveNotification()
                            SWMessage.sharedInstance.showNotificationWithTitle("No internet connection", subtitle: nil, type: .Error) // (error?.localizedDescription)!
                            completionClosure( success: false , error: error!)
                            
                            return
                        }
                        return
                    }else{
                        dispatch_async(dispatch_get_main_queue()) {
                            SVProgressHUD.dismiss()
                        }
                        return
                    }
                }
                
                let resonseDictionary : NSMutableDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSMutableDictionary
                print(resonseDictionary)
                Utility.sharedInstance.saveToken(resonseDictionary.valueForKey("token") as! String)
                completionClosure( success: true , error: error!)
                
                return
            }
            catch {
                print("json error: \(error)")
            }
        }
        task.resume()
    }
    
    
    func createNewListing ( dictDetails : NSMutableDictionary , completionClosure: ( reponseDict: NSMutableDictionary , success: Bool , error : NSError)-> ()) {
        
        SVProgressHUD.showWithStatus("SA_STR_LOADING".localized, maskType: SVProgressHUDMaskType.Clear)
        
        
        let dictDetailsA: NSMutableDictionary       = ["icar_vcode":"IDVKIA_2002AABK" , "seller_id": "41de9f34-8b78-e411-94b6-06e4185eac05" , "seller_profile_id":"45de9f34-8b78-e411-94b6-06e4185eac05" , "type":"Recon" , "color":"Merah" , "description":"MS   Sedan Pertamax 5st 4dr Automatic 6cyl 2493cc FWD" , "state":"Banten" , "area":"Cilegon" , "city":"Jombang" , "price":"99999999999" , "title":"MS   Sedan Pertamax 5st 4dr Automatic 6cyl 2493cc FWD" , "assemble_type":"CBU" , "mileage":"22" , "mileage_range":"20k-25k km" , "unique_identifier":"2002" , "registered_year":"2016"]
        
        let requestData : NSData            = NSKeyedArchiver.archivedDataWithRootObject(dictDetailsA)
        let urlString: String               = "http://ex-api.used246.icarasia.net/v2.0/id/id/listing"
        let request: NSMutableURLRequest    = NSMutableURLRequest(URL: NSURL(string: urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
        request.HTTPMethod                  = "POST"
        //request.HTTPBody                    = requestData
        request.setValue("a5efada2-6ef6-41aa-85a4-cea45ce44d11", forHTTPHeaderField: "token")
        request.setValue("iOS_MDA", forHTTPHeaderField: "platform")
        request.setValue("1.0", forHTTPHeaderField: "api_version")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.setValue("\(UInt(requestData.length))", forHTTPHeaderField: "Content-Length")
        request.timeoutInterval             = 90.0

        
        //request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(dictDetailsA, options: .PrettyPrinted)

        var bodyData = ""
        for (key,value) in dictDetailsA{
            //if (value == nil){ continue }
            let scapedKey = key.stringByAddingPercentEncodingWithAllowedCharacters(
                .URLHostAllowedCharacterSet())!
            let scapedValue = value.stringByAddingPercentEncodingWithAllowedCharacters(
                .URLHostAllowedCharacterSet())!
            bodyData += "\(scapedKey)=\(scapedValue)&"
        }
        
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            
            do{
                let responseDictionary : NSMutableDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSMutableDictionary
                
                let _error = NSError(domain: "", code: 1, userInfo: nil)
                completionClosure( reponseDict: responseDictionary, success: true , error: _error)
                
                dispatch_async(dispatch_get_main_queue()) {
                    SVProgressHUD.dismiss()
                }
                
            }
            catch {
                print("json error: \(error)")
            }
        }
        task.resume()
    }
}