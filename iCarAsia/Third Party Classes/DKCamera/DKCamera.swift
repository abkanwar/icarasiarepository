//
//  DKCamera.swift
//  DKCameraDemo
//
//  Created by ZhangAo on 15/8/30.
//  Copyright (c) 2015年 ZhangAo. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion
import Photos


public class DKCameraPassthroughView: UIView {
    public override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let hitTestingView = super.hitTest(point, withEvent: event)
        return hitTestingView == self ? nil : hitTestingView
    }
}

public class DKCamera: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource{
    
    @IBOutlet weak var captureButton:   UIButton!
    @IBOutlet weak var galleryButton:   UIButton!
    @IBOutlet weak var cancelButton:    UIButton!
    @IBOutlet weak var nextButton:      UIButton!
    
    @IBOutlet weak var flashOnButton:   UIButton!
    @IBOutlet weak var flashOffButton:  UIButton!
    @IBOutlet weak var flashAutoButton: UIButton!
    
    @IBOutlet weak var headerCountLabel: UILabel!
    
                   var viewGesture =   UIView()
    
    @IBOutlet weak var collectionViewImages: UICollectionView!
    
    //var assets = [DKAsset]()
    
    public class func checkCameraPermission(handler: (granted: Bool) -> Void) {
        func hasCameraPermission() -> Bool {
            return AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) == .Authorized
        }
        
        func needsToRequestCameraPermission() -> Bool {
            return AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) == .NotDetermined
        }
        
        hasCameraPermission() ? handler(granted: true) : (needsToRequestCameraPermission() ?
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { granted in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    hasCameraPermission() ? handler(granted: true) : handler(granted: false)
                })
            }) : handler(granted: false))
    }
    
    public var cameraOverlayView: UIView? {
        didSet {
            if let cameraOverlayView = cameraOverlayView {
                self.view.addSubview(cameraOverlayView)
            }
        }
    }
    
    // The flash Model will to be remembered to next use. //
    public var flashMode:AVCaptureFlashMode! {
        didSet {
            self.updateFlashMode()
            self.updateFlashModeToUserDefautls(self.flashMode)
        }
    }
    
    public class func isAvailable() -> Bool {
        return UIImagePickerController.isSourceTypeAvailable(.Camera)
    }
    
    // Determines whether or not the rotation is enabled. //
    public var allowsRotate             = false
    public let captureSession           = AVCaptureSession()
    @IBOutlet public var previewLayer: AVCaptureVideoPreviewLayer!
    private var beginZoomScale: CGFloat = 1.0
    private var zoomScale: CGFloat      = 1.0
    
    public var currentDevice:           AVCaptureDevice?
    public var captureDeviceFront:      AVCaptureDevice?
    public var captureDeviceBack:       AVCaptureDevice?
    private weak var stillImageOutput:  AVCaptureStillImageOutput?
    
    //public var contentView = UIView()
    
    public var originalOrientation:     UIDeviceOrientation!
    public var currentOrientation:      UIDeviceOrientation!
    public let motionManager =          CMMotionManager()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupDevices()
        self.beginSession()
        self.setupMotionManager()
        
        captureButton.addTarget(self, action: #selector(DKCamera.takePicture), forControlEvents: .TouchUpInside)
        
        self.viewGesture.backgroundColor = UIColor.clearColor()
        self.viewGesture.frame           = CGRectMake(0, 60, CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame)/4*3 )
        self.self.view.addSubview(viewGesture)
        
        self.viewGesture.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(DKCamera.handleZoom(_:))))
        self.viewGesture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DKCamera.handleFocus(_:))))
        
        //let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongGesture))
        //self.collectionViewImages.addGestureRecognizer(longPressGesture)
        
        self.updateImagesCountLabelAndNextButton()
    }
    
    public override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.hidden = true
        
        if !self.captureSession.running {
            self.captureSession.startRunning()
        }
        
        if !self.motionManager.accelerometerActive {
            self.motionManager.startAccelerometerUpdatesToQueue(NSOperationQueue.currentQueue()!, withHandler: { accelerometerData, error in
                if error == nil {
                    let currentOrientation = accelerometerData!.acceleration.toDeviceOrientation() ?? self.currentOrientation
                    if self.originalOrientation == nil {
                        self.initialOriginalOrientationForOrientation()
                        self.currentOrientation = self.originalOrientation
                    }
                    if let currentOrientation = currentOrientation where self.currentOrientation != currentOrientation {
                        self.currentOrientation = currentOrientation
                        self.updateContentLayoutForCurrentOrientation()
                    }
                } else {
                    print("error while update accelerometer: \(error!.localizedDescription)", terminator: "")
                }
            })
        }
        
        if self.navigationController!.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
            self.navigationController!.interactivePopGestureRecognizer!.enabled  = false
        }
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.originalOrientation == nil {
            self.previewLayer.frame = CGRectMake(0, 60, CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame)/4*3 )
            self.viewGesture.frame  = CGRectMake(0, 60, CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame)/4*3 )
        }
    }
    
    public override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.captureSession.stopRunning()
        self.motionManager.stopAccelerometerUpdates()
    }
    
    public override func viewWillDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.navigationController!.respondsToSelector(Selector("interactivePopGestureRecognizer")) {
            self.navigationController!.interactivePopGestureRecognizer!.enabled  = true
        }
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated. //
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    /*
    func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        
        switch(gesture.state) {
            
        case UIGestureRecognizerState.Began:
            guard let selectedIndexPath = self.collectionViewImages.indexPathForItemAtPoint(gesture.locationInView(self.collectionViewImages)) else {
                break
            }
            if #available(iOS 9.0, *) {
                if selectedIndexPath.row < self.assets.count  {
                    self.collectionViewImages.beginInteractiveMovementForItemAtIndexPath(selectedIndexPath)
                }
            } else {
                // Fallback on earlier versions //
            }
        case UIGestureRecognizerState.Changed:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.updateInteractiveMovementTargetPosition(gesture.locationInView(gesture.view!))
            } else {
                // Fallback on earlier versions //
            }
        case UIGestureRecognizerState.Ended:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.endInteractiveMovement()
            } else {
                // Fallback on earlier versions //
            }
        default:
            if #available(iOS 9.0, *) {
                self.collectionViewImages.cancelInteractiveMovement()
            } else {
                // Fallback on earlier versions //
            }
        }
    }*/
    
    @IBAction func galleryAction(sender: AnyObject) {
        
        let pickerController                    = DKImagePickerController()
        pickerController.assetType              = .AllPhotos
        pickerController.allowsLandscape        = false
        pickerController.allowMultipleTypes     = true
        pickerController.sourceType             = .Photo
        pickerController.singleSelect           = false
        pickerController.maxSelectableCount     = 21
        pickerController.defaultSelectedAssets  = AdDetailStorage.sharedInstance.assets
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            AdDetailStorage.sharedInstance.assets = assets
            self.updateImagesCountLabelAndNextButton()
            self.collectionViewImages.reloadData()
        }
        if UI_USER_INTERFACE_IDIOM() == .Pad {
            pickerController.modalPresentationStyle = .FormSheet
        }
        self.presentViewController(pickerController, animated: true) {
        }
        
    }
    
    func updateImagesCountLabelAndNextButton () {
        
        if AdDetailStorage.sharedInstance.assets.count == 0 {
            headerCountLabel.text = "Select Images"
        }
        else{
            
            if AdDetailStorage.sharedInstance.assets.count == 1 {
                headerCountLabel.text = "\(AdDetailStorage.sharedInstance.assets.count) Image"
            }else{
                headerCountLabel.text = "\(AdDetailStorage.sharedInstance.assets.count) Images"
            }
        }
        AdDetailStorage.sharedInstance.assets.count >= 5 ? ( nextButton.hidden = false ) : (nextButton.hidden = true)
    }
    
    @IBAction func nextButtonAction(sender: AnyObject) {
        
        if self.belowMinimumImageLimit() {
            return
        }
        
        let previewVC    = self.storyboard?.instantiateViewControllerWithIdentifier("PreviewVC") as! PreviewVC
        //previewVC.assets = AdDetailStorage.sharedInstance.assets
        self.navigationController?.pushViewController(previewVC, animated: true)
    }
    
    @IBAction func flashOnAction(sender: AnyObject) {
        
        self.flashMode      = .On
        
        self.changeStates()
        let button          = sender as! UIButton
        button.selected     = true
        
    }
    
    @IBAction func flashAuto(sender: AnyObject) {
       
        self.flashMode      = .Auto
        
        self.changeStates()
        let button          = sender as! UIButton
        button.selected     = true
    }
    
    @IBAction func flashOffAction(sender: AnyObject) {
        
        self.flashMode      = .Off
        
        self.changeStates()
        let button          = sender as! UIButton
        button.selected     = true
    }
    
    func changeStates (){
        
        flashOnButton.selected      = false
        flashOffButton.selected     = false
        flashAutoButton.selected    = false

    }
    
    func enableButton ( value : Bool ) {
        
        self.flashOnButton.userInteractionEnabled   = value
        self.flashAutoButton.userInteractionEnabled = value
        self.flashOffButton.userInteractionEnabled  = value
    }
    
    func belowMinimumImageLimit () -> Bool
    {
        if AdDetailStorage.sharedInstance.assets.count < 5 {
            SWMessage.sharedInstance.dismissActiveNotification()
            SWMessage.sharedInstance.showNotificationWithTitle("You must add atleat 5 images per listing.", subtitle: nil, type: .Error)
            return true
        }
        return false
    }
    
    func exceedImageLimit () -> Bool
    {
        if AdDetailStorage.sharedInstance.assets.count == 21 {
            SWMessage.sharedInstance.dismissActiveNotification()
            SWMessage.sharedInstance.showNotificationWithTitle("You can add a max of 21 images per listing.", subtitle: nil, type: .Error)
            return true
        }
        return false
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        AdDetailStorage.sharedInstance.assets.removeAll()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    public override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    public func setupDevices() {
        let devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo) as! [AVCaptureDevice]
        
        for device in devices {
            if device.position == .Back {
                self.captureDeviceBack = device
            }
            if device.position == .Front {
                self.captureDeviceFront = device
            }
        }
        self.currentDevice = self.captureDeviceBack ?? self.captureDeviceFront
    }
    
    public func takePicture() {
        
        if self.exceedImageLimit() {
            return
        }
        let authStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        if authStatus == .Denied {
            return
        }
        if let stillImageOutput = self.stillImageOutput {
            dispatch_async(dispatch_get_global_queue(0, 0), {
                let connection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo)
                if connection == nil {
                    return
                }
                connection.videoOrientation         = self.currentOrientation.toAVCaptureVideoOrientation()
                connection.videoScaleAndCropFactor  = self.zoomScale
                
                stillImageOutput.captureStillImageAsynchronouslyFromConnection(connection, completionHandler: { (imageDataSampleBuffer, error: NSError?) -> Void in
                    
                    if error == nil {
                        let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                        
                        
                        let takenImage      = UIImage(data: imageData)
                        let outputRect      = self.previewLayer.metadataOutputRectOfInterestForRect(self.previewLayer.bounds)
                        let takenCGImage    = takenImage!.CGImage
                        let width           = CGFloat(CGImageGetWidth(takenCGImage))
                        let height          = CGFloat(CGImageGetHeight(takenCGImage))
                        let cropRect        = CGRectMake(outputRect.origin.x * width, outputRect.origin.y * height, outputRect.size.width * width, outputRect.size.height * height)
                        
                        let cropCGImage     = CGImageCreateWithImageInRect(takenCGImage, cropRect)
                        let cropTakenImage  = UIImage(CGImage: cropCGImage!, scale: 1, orientation: takenImage!.imageOrientation)
                        
                        var newImageIdentifier: String!
                        PHPhotoLibrary.sharedPhotoLibrary().performChanges( { () in
                            let assetRequest    = PHAssetChangeRequest.creationRequestForAssetFromImage(cropTakenImage)
                            newImageIdentifier  = assetRequest.placeholderForCreatedAsset!.localIdentifier
                            }, completionHandler: { (success, error) in
                                dispatch_async(dispatch_get_main_queue(), {
                                    if success {
                                        if let newAsset = PHAsset.fetchAssetsWithLocalIdentifiers([newImageIdentifier], options: nil).firstObject as? PHAsset {
                                            AdDetailStorage.sharedInstance.assets.append((DKAsset(originalAsset: newAsset)))
                                            self.updateImagesCountLabelAndNextButton()
                                            self.collectionViewImages.reloadData()
                                        }
                                    }
                                })
                        })
                    }
                    else {
                        print("error while capturing still image: \(error!.localizedDescription)", terminator: "")
                    }
                })
            })
        }
    }
    
    // MARK: - Handles Zoom -
    
    public func handleZoom(gesture: UIPinchGestureRecognizer) {
        if gesture.state == .Began {
            self.beginZoomScale = self.zoomScale
        } else if gesture.state == .Changed {
            self.zoomScale = min(4.0, max(1.0, self.beginZoomScale * gesture.scale))
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.025)
            self.previewLayer.setAffineTransform(CGAffineTransformMakeScale(self.zoomScale, self.zoomScale))
            CATransaction.commit()
        }
    }
    
    // MARK: - Handles Focus -
    
    public func handleFocus(gesture: UITapGestureRecognizer) {
        if let currentDevice = self.currentDevice where currentDevice.focusPointOfInterestSupported {
            let touchPoint = gesture.locationInView(self.view)
            self.focusAtTouchPoint(touchPoint)
        }
    }
    
    // MARK: - Handles Switch Camera -
    
    internal func switchCamera() {
        self.currentDevice = self.currentDevice == self.captureDeviceBack ?
            self.captureDeviceFront : self.captureDeviceBack
        
        self.setupCurrentDevice()
    }
    
    // MARK: - Handles Flash -
    
    public func flashModeFromUserDefaults() -> AVCaptureFlashMode {
        let rawValue = NSUserDefaults.standardUserDefaults().integerForKey("DKCamera.flashMode")
        return AVCaptureFlashMode(rawValue: rawValue)!
    }
    
    public func updateFlashModeToUserDefautls(flashMode: AVCaptureFlashMode) {
        NSUserDefaults.standardUserDefaults().setInteger(flashMode.rawValue, forKey: "DKCamera.flashMode")
    }
    
    // MARK: - Capture Session
    
    public func beginSession() {
        self.captureSession.sessionPreset = AVCaptureSessionPresetPhoto
        
        self.setupCurrentDevice()
        
        let stillImageOutput            = AVCaptureStillImageOutput()
        if self.captureSession.canAddOutput(stillImageOutput) {
            self.captureSession.addOutput(stillImageOutput)
            self.stillImageOutput       = stillImageOutput
        }
        
        self.previewLayer               = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.previewLayer.videoGravity  = AVLayerVideoGravityResizeAspectFill
        self.previewLayer.frame         = CGRectMake(0, 60, CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame)/4*3 )

        
        let rootLayer                   = self.view.layer
        rootLayer.masksToBounds         = true
        rootLayer.insertSublayer(self.previewLayer, atIndex: 0)
    }
    
    public func setupCurrentDevice() {
       
        if let currentDevice = self.currentDevice {
            
            if currentDevice.flashAvailable {
                
                self.enableButton(true)
                self.changeStates()
                self.flashMode = self.flashModeFromUserDefaults()
                switch flashModeFromUserDefaults() {
                case .On:   flashOnButton.selected      = true
                    break
                case .Auto: flashAutoButton.selected    = true
                    break
                case .Off:  flashOnButton.selected      = true
                    break
                //default:
                //    break
                }
                
            } else {
                self.enableButton(false)
            }
            
            for oldInput in self.captureSession.inputs as! [AVCaptureInput] {
                self.captureSession.removeInput(oldInput)
            }
            
            let frontInput = try? AVCaptureDeviceInput(device: self.currentDevice)
            if self.captureSession.canAddInput(frontInput) {
                self.captureSession.addInput(frontInput)
            }
            
            try! currentDevice.lockForConfiguration()
            if currentDevice.isFocusModeSupported(.ContinuousAutoFocus) {
                currentDevice.focusMode = .ContinuousAutoFocus
            }
            
            if currentDevice.isExposureModeSupported(.ContinuousAutoExposure) {
                currentDevice.exposureMode = .ContinuousAutoExposure
            }
            
            currentDevice.unlockForConfiguration()
        }
    }
    
    public func updateFlashMode() {
        if let currentDevice = self.currentDevice
            where currentDevice.flashAvailable && currentDevice.isFlashModeSupported(self.flashMode) {
            try! currentDevice.lockForConfiguration()
            currentDevice.flashMode = self.flashMode
            currentDevice.unlockForConfiguration()
        }
    }
    
    public func focusAtTouchPoint(touchPoint: CGPoint) {
        
        func showFocusViewAtPoint(touchPoint: CGPoint) {
            struct FocusView {
                static let focusView: UIView = {
                    let focusView = UIView()
                    let diameter: CGFloat = 100
                    focusView.bounds.size = CGSize(width: diameter, height: diameter)
                    focusView.layer.borderWidth = 2
                    focusView.layer.cornerRadius = diameter / 2
                    focusView.layer.borderColor = UIColor.whiteColor().CGColor
                    
                    return focusView
                }()
            }
            FocusView.focusView.transform = CGAffineTransformIdentity
            FocusView.focusView.center = touchPoint
            self.view.addSubview(FocusView.focusView)
            UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.1,
                                       options: .CurveEaseInOut, animations: { () -> Void in
                                        FocusView.focusView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6)
            }) { (Bool) -> Void in
                FocusView.focusView.removeFromSuperview()
            }
        }
        if self.currentDevice == nil || self.currentDevice?.flashAvailable == false {
            return
        }
        let focusPoint = self.previewLayer.captureDevicePointOfInterestForPoint(touchPoint)
        showFocusViewAtPoint(touchPoint)
        if let currentDevice = self.currentDevice {
            try! currentDevice.lockForConfiguration()
            currentDevice.focusPointOfInterest = focusPoint
            currentDevice.exposurePointOfInterest = focusPoint
            currentDevice.focusMode = .ContinuousAutoFocus
            if currentDevice.isExposureModeSupported(.ContinuousAutoExposure) {
                currentDevice.exposureMode = .ContinuousAutoExposure
            }
            currentDevice.unlockForConfiguration()
        }
    }
    
    // MARK: - Handles Orientation  -
    public override func shouldAutorotate() -> Bool {
        return false
    }
    
    public func setupMotionManager() {
        self.motionManager.accelerometerUpdateInterval = 0.5
        self.motionManager.gyroUpdateInterval = 0.5
    }
    
    public func initialOriginalOrientationForOrientation() {
        self.originalOrientation = UIApplication.sharedApplication().statusBarOrientation.toDeviceOrientation()
        if let connection = self.previewLayer.connection {
            connection.videoOrientation = self.originalOrientation.toAVCaptureVideoOrientation()
        }
    }
    
    public func updateContentLayoutForCurrentOrientation() {
        let newAngle = self.currentOrientation.toAngleRelativeToPortrait() - self.originalOrientation.toAngleRelativeToPortrait()
        
        if self.allowsRotate {
            var contentViewNewSize: CGSize!
            print("\(contentViewNewSize)")
            let width   = self.view.bounds.width
            let height  = self.view.bounds.height
            if UIDeviceOrientationIsLandscape(self.currentOrientation) {
                contentViewNewSize = CGSize(width: max(width, height), height: min(width, height))
            } else {
                contentViewNewSize = CGSize(width: min(width, height), height: max(width, height))
            }
            UIView.animateWithDuration(0.2) {
                //self.contentView.bounds.size      = contentViewNewSize
                //self.contentView.transform        = CGAffineTransformMakeRotation(newAngle)
            }
        } else {
            let rotateAffineTransform = CGAffineTransformRotate(CGAffineTransformIdentity, newAngle)
            print("\(rotateAffineTransform)")
            UIView.animateWithDuration(0.2) {
                //self.flashButton.transform        = rotateAffineTransform
                //self.cameraSwitchButton.transform = rotateAffineTransform
            }
        }
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 21 // self.assets.count ?? 0
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        //let asset       = self.assets[indexPath.row]
        var cell:         UICollectionViewCell?
        var imageView:    UIImageView?
        
        //if asset.isVideo {
        //    cell        = collectionView.dequeueReusableCellWithReuseIdentifier("CellVideo", forIndexPath: indexPath)
        //    imageView   = cell?.contentView.viewWithTag(1) as? UIImageView
        //} else {
            cell        = collectionView.dequeueReusableCellWithReuseIdentifier("CellImage", forIndexPath: indexPath)
            imageView   = cell?.contentView.viewWithTag(1) as? UIImageView
        //}
        
        if let cell = cell, imageView = imageView
        {
            let layout  = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            let tag     = indexPath.row + 1
            cell.tag    = tag
            
            if indexPath.row < AdDetailStorage.sharedInstance.assets.count {
                
                let asset   = AdDetailStorage.sharedInstance.assets[indexPath.row]
                asset.fetchImageWithSize(layout.itemSize.toPixel(), completeBlock: { image, info in
                    if cell.tag == tag {
                        imageView.image = image
                    }
                })
            }else{
                imageView.image = UIImage( named:  "ImgThumbnail")
            }
        }
        return cell!
    }
    
     public func collectionView(collectionView: UICollectionView, moveItemAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
    }
    
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
    }
}

public extension UIInterfaceOrientation {
    
    func toDeviceOrientation() -> UIDeviceOrientation {
        switch self {
        case .Portrait:
            return .Portrait
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        case .LandscapeRight:
            return .LandscapeLeft
        case .LandscapeLeft:
            return .LandscapeRight
        default:
            return .Portrait
        }
    }
}

public extension UIDeviceOrientation {
    
    func toAVCaptureVideoOrientation() -> AVCaptureVideoOrientation {
        switch self {
        case .Portrait:
            return .Portrait
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        case .LandscapeRight:
            return .LandscapeLeft
        case .LandscapeLeft:
            return .LandscapeRight
        default:
            return .Portrait
        }
    }
    
    func toInterfaceOrientationMask() -> UIInterfaceOrientationMask {
        
        switch self {
        case .Portrait:
            return .Portrait
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        case .LandscapeRight:
            return .LandscapeLeft
        case .LandscapeLeft:
            return .LandscapeRight
        default:
            return .Portrait
        }
    }
    
    func toAngleRelativeToPortrait() -> CGFloat {
     
        switch self {
        case .Portrait:
            return 0
        case .PortraitUpsideDown:
            return CGFloat(M_PI)
        case .LandscapeRight:
            return CGFloat(-M_PI_2)
        case .LandscapeLeft:
            return CGFloat(M_PI_2)
        default:
            return 0
        }
    }
}

public extension CMAcceleration {
    
    func toDeviceOrientation() -> UIDeviceOrientation? {
        if self.x >= 0.75 {
            return .LandscapeRight
        } else if self.x <= -0.75 {
            return .LandscapeLeft
        } else if self.y <= -0.75 {
            return .Portrait
        } else if self.y >= 0.75 {
            return .PortraitUpsideDown
        } else {
            return nil
        }
    }
}

