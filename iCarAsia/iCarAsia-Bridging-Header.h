//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#ifndef iCarAsia_Bridging_Header_h
#define iCarAsia_Bridging_Header_h

@import UIKit;
@import Foundation;
@import SystemConfiguration;
@import MobileCoreServices;


#import <Google/SignIn.h>
#import <QuickBlox/QBSettings.h>
#import <Quickblox/Quickblox.h>
#import <QMServices.h>
#import <SVProgressHUD.h>

#import <Bolts/Bolts.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "QMChatViewController.h"
#import "QMChatContactRequestCell.h"
#import "QMChatNotificationCell.h"
#import "QMChatIncomingCell.h"
#import "QMChatOutgoingCell.h"
#import "QMCollectionViewFlowLayoutInvalidationContext.h"

#import "TTTAttributedLabel.h"

#import "QMMessageNotificationManager.h"
#import "MPGNotification.h"

#import "_CDMessage.h"
#import "UIImage+QM.h"
#import "UIColor+QM.h"
#import "UIImage+fixOrientation.h"

#import "NSString+EMOEmoji.h"

#endif
